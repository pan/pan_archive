# PAN Simulation

## Introduction

This is the simulation software package of the PAN project. It will be maintained by PAN simulation group. 

1. The only version which can be used for publications is this version
2. All updates by the developers are shared on https://gitlab.unige.ch/pan/pan_archive
3. Non developers who want to use the code should discuss this first within the simulation group
4. Either the code is then provided to them with instructions OR the simulations are produced for them by the developers


## Packages

The software has been developed with and is supported for:

1. GEANT : 4.10.7.2 (https://geant4.web.cern.ch/)
2. ROOT  : 6.24.02 (https://root.cern/)
3. VGM (https://github.com/vmc-project/vgm) 

Install them prior to your next step.

<div class="text-red mb-2">
  Note: Do not use older GEANT4 versions! Several old features are deprecated and thus act "buggy" when used with this framework.
</div>

## How to compile and run the G4 simulation

* cd Simulations
  - Go to the main 'Simulations' directory. There should be a 'build' directory

* cd build
  - Go into the 'build' directory. If it does not exist; create one.

* cmake ../
  - Inside the 'build' directory, you should run 'cmake ../'. This will automatically setup everything for you regarding Geant4, ROOT, scripts, etc. It will produce a Makefile.
    It will also create a 'bin' (for all binaries) and a 'data/G4out' (for simulation files) directory.

* make -j4
  - Compile. Debug flags are enabled. You should receive warnings/errors if necessary.
 
* make install
  - Install. Note, the binary will be installed in the 'Simulations/bin' (so outside the build directory).

* cd ../bin
  - Go into the 'bin' where you will find the 'PANSim' binary.

* ./PANSim gps.mac run.mac -c config.mac -o output.root
  - run it :)

