#include "G4RunManager.hh"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#endif

#include "G4UImanager.hh"
#include "G4UIExecutive.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "G4VisExecutive.hh"

#include "PANDetectorConstruction.hh"
#include "PANPhysicsList.hh"
#include "PANActionInitialization.hh"
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

#include "OptionsManager.hh"

void RunGuiVis(OptionsManager, char**);
void RunTerminalVis(OptionsManager, char**);
bool RunSimulation(G4UImanager*, OptionsManager);

int main(int argc,char** argv) {
    // process command parameters
    OptionsManager options_mgr;
    if (!options_mgr.parse(argc, argv)) {
        if (options_mgr.GetVersion()) {
            options_mgr.print_version();
        } else {
            options_mgr.print_help();
        }
	exit(EXIT_SUCCESS);
    }

    //read the configuration files
    PANGlobalConfig* fPANGlobalConfig = PANGlobalConfig::Instance();
    PANSDConfig*     fPANSDConfig     = PANSDConfig::Instance();

    G4UImanager* uiManager = G4UImanager::GetUIpointer();
    G4String execute = "/control/execute ";
    if (options_mgr.config_file != "") {
        uiManager->ApplyCommand(execute + options_mgr.config_file);
        fPANGlobalConfig->print_config();
        fPANSDConfig->print_config();
    }

    if(!fPANGlobalConfig->IsMiniPAN){
        G4cout << "No Detector Type Selected. Specify this in the config file" << G4endl;
        exit(EXIT_FAILURE);
    }

    fPANSDConfig->GenerateTreeKeys(false);

    //disable option for 'light' detctor description if no visualition
    if(!options_mgr.IsGuiVis()) fPANGlobalConfig->visual_light = false;

    //initialize detector, physics and action
    PANDetectorConstruction* fPANDetectorConstruction;
    PANPhysicsList*          fPANPhysicsList;
    PANActionInitialization* fPANActionInitialization;

    G4RunManager* runManager = new G4RunManager();
    G4cout << "#### WARNING: only sequential mode can be used ####" << G4endl;
    G4cout << "#### using sequential mode ####" << G4endl;
    // set mandatory initialization classes
    fPANDetectorConstruction = new PANDetectorConstruction();
    runManager->SetUserInitialization(fPANDetectorConstruction);

    fPANPhysicsList           = new PANPhysicsList(fPANGlobalConfig->phys_verbose);
    runManager->SetUserInitialization(fPANPhysicsList);

    fPANActionInitialization = new PANActionInitialization(options_mgr.output_file, options_mgr.IsNameFixed());
    runManager->SetUserInitialization(fPANActionInitialization);
    // initialize G4 kernel
    G4cout << "initialize G4 kernel" << G4endl;
    runManager->Initialize();

    G4cout << "#### G4 KERNEL INITIALIZED ####" << G4endl;

    if(options_mgr.IsGuiVis()) {
        RunGuiVis(options_mgr, argv);                                                         // gui visualization
    } else if(options_mgr.IsTerminalVis()) {                                                   
	RunTerminalVis(options_mgr, argv);                                                    //interactive terminal
    } else {
	if(!RunSimulation(uiManager, options_mgr)){return 0;}     //simulations
    }

    // job termination
    delete runManager;

    return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunGuiVis(OptionsManager OptMgr, char** argv){
  G4UIExecutive* ui      = new G4UIExecutive(1, argv);  //define UI session
  G4UImanager* UImanager = G4UImanager::GetUIpointer(); //get the pointer to the UI manager

  G4VisManager* visManager = new G4VisExecutive("warnings");        //initialize visualization
  visManager->Initialize();
  
  //UImanager->ApplyCommand("/control/verbose 2");
  //UImanager->ApplyCommand("/control/saveHistory");
  //UImanager->ApplyCommand("/run/verbose 2");
  //UImanager->ApplyCommand("/run/initialize");

  G4String command = "/control/execute " + OptMgr.vis_mac_file;
  UImanager->ApplyCommand(command);

  ui->SessionStart();

  delete ui;
  delete visManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunTerminalVis(OptionsManager OptMgr, char** argv){
  G4UIExecutive* ui      = new G4UIExecutive(1, argv);  //define UI session
  G4UImanager* UImanager = G4UImanager::GetUIpointer(); //get the pointer to the UI manager

  G4VisManager* visManager = new G4VisExecutive("warnings");        //initialize visualization
  visManager->Initialize();

  G4UIsession* session = new G4UIterminal(new G4UItcsh);  //create character terminal with command completion, history, etc.
 
  G4String command = "/control/execute " + OptMgr.vis_mac_file; 
  UImanager->ApplyCommand(command);
  session->SessionStart();
					
  delete session;
  delete visManager;
  delete ui;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

bool RunSimulation(G4UImanager* UImanager, OptionsManager OptMgr){
// start simulation
   G4cout<< "********************* Start Simulation *********************" << G4endl;
  
   G4cout << "==== using general particle source ====" << G4endl;
     
   G4String command = "/control/execute " + OptMgr.primary_file;
   UImanager->ApplyCommand(command);
   
   command = "/control/execute " + OptMgr.run_mac_file; 
   UImanager->ApplyCommand(command);

   G4cout<< "********************* End Simulation *********************" << G4endl;

   return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
