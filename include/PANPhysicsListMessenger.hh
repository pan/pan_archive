#ifndef PANPhysicsListMessenger_HH
#define PANPhysicsListMessenger_HH

#include "globals.hh"
#include "G4UImessenger.hh"

class PANPhysicsList;
class G4UIdirectory;
class G4UIcmdWithAString;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class PANPhysicsListMessenger: public G4UImessenger
{
  public:
  
    PANPhysicsListMessenger(PANPhysicsList* );
   ~PANPhysicsListMessenger();
    
    virtual void SetNewValue(G4UIcommand*, G4String);
    
  private:
  
    PANPhysicsList*         fPhysicsList;
    
    G4UIdirectory*             fPhysDir;        
    G4UIcmdWithAString*        fListCmd;
    
};

#endif