#ifndef PANConfigMessenger_HH
#define PANConfigMessenger_HH

#include "globals.hh"
#include "G4UImessenger.hh"
#include "PANGlobalConfig.hh"

class PANGlobalConfig;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class PANConfigMessenger: public G4UImessenger {
private:
    PANGlobalConfig* fGlobalConfig_;

public:
    PANConfigMessenger(PANGlobalConfig* theGlobalConfig);
    ~PANConfigMessenger();

    void SetNewValue(G4UIcommand* command, G4String newValue);

private:
    // MiniPAN command list
    G4UIcmdWithABool*          fGlobalConfigIsMiniPANCmd_;
    G4UIcmdWithABool*          fGlobalConfigUseMLICmd_;
    G4UIcmdWithABool*          fGlobalConfigUseSSDCmd_;
    G4UIcmdWithABool*          fGlobalConfigUseTPx3Cmd_;
    G4UIcmdWithABool*          fGlobalConfigUseTOFCmd_;
    G4UIcmdWithADouble*        fGlobalConfigBirksConstCmd_;

    // general command list
    G4UIdirectory*             fGlobalConfigDir_;
    G4UIcmdWithADoubleAndUnit* fGlobalConfigHitThrCmd_;
    G4UIcmdWithAString*        fGlobalConfigOutDirCmd_;
    G4UIcmdWithAnInteger*      fGlobalConfigEventVerCmd_;
    G4UIcmdWithABool*          fGlobalConfigPriOnlyCmd_;
    
    G4UIcmdWithABool*          fGlobalConfigIsInSpaceCmd_;
    G4UIcmdWithAnInteger*      fGlobalConfigNumOfModCmd_;
    G4UIcmdWithABool*          fGlobalConfigUseSPENVISdataCmd_;
    G4UIcmdWithAnInteger*      fGlobalConfigPhysVerCmd_;
    G4UIcmdWithABool*          fGlobalConfigSimOutMoreCmd_;
    G4UIcmdWithABool*          fGlobalConfigPhysMoreCmd_;
    G4UIcmdWithABool*          fGlobalConfigVisLightCmd_;
    G4UIcmdWithABool*          fGlobalConfigWriteGeoROOTCmd_;
    G4UIcmdWithABool*          fGlobalConfigUseBeCmd_;
    G4UIcmdWithABool*          fGlobalConfigWriteDoseMoreCmd_;
    G4UIcmdWithABool*          fGlobalConfigAddMagneticFieldCmd_;

    G4UIcmdWithADoubleAndUnit* fGlobalConfigEnergyMinCmd_;
    G4UIcmdWithADoubleAndUnit* fGlobalConfigEnergyMaxCmd_;

};

#endif
