#ifndef PANDetectorConstruction_HH
#define PANDetectorConstruction_HH

#include "G4VUserDetectorConstruction.hh"

// units
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4Cache.hh"
#include "G4MagneticField.hh"

// configure
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

// tools
#include <vector>
#include <map>

// instrument
#include "instr_MiniPAN_Jun2022.hh"

class G4Material;
class G4UniformMagField;
class PANTabulatedField3D;

class PANDetectorConstruction : public G4VUserDetectorConstruction {

public:
    PANDetectorConstruction();
    ~PANDetectorConstruction();

    G4VPhysicalVolume* Construct();
    void ConstructSDandField();
    G4String GetSD_Name(G4LogicalVolume* SD_Log) const {return instrumentSD.at(SD_Log);}
    std::map<G4LogicalVolume*,G4String> GetSD_Map() const {return instrumentSD;}

    MiniPAN_Jun2022_Instrument GetMiniPAN() const {return MiniPAN_Jun2022;}

private:
    //configuration
    PANGlobalConfig* fPANGlobalConfig;
    PANSDConfig*     fPANSDConfig;

    // Materials
    G4Material* AmbientEnv_;

    // Volumes
    G4LogicalVolume* WorldLog_;

    //construct detector logic
    void DefineMaterials_();
    void ConstructMiniPANDetector_();
    void ConstructREMECDetector_();
    void ConstructLunPANDetector_();

    //available instruments
    MiniPAN_Jun2022_Instrument MiniPAN_Jun2022;

    //map for SD
    std::map<G4LogicalVolume*,G4String> instrumentSD;

    //some physcial constants
    G4double Photon3Energy[3] = {2.48*eV, 2.93*eV, 3.10*eV};

};

#endif
