#ifndef PANHit_HH
#define PANHit_HH

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "tls.hh"
#include "G4ThreeVector.hh"
#include "G4StepStatus.hh"

class PANHit: public G4VHit {
public:
    PANHit();
    ~PANHit();

    inline void* operator new(size_t);
    inline void  operator delete(void* aHit);

public:
    // hit information
    G4int             TrackID;
    G4int             pdgID;
    G4int             parentID;
    G4String          VolName;
    G4int             CopyID;
    G4int             PixelID;
    G4int             StripID;
    G4String          ParticleName;
    G4double          GlobalTime;
    G4int             StepStatus;
    G4double          KinEnergy;
    G4String          ProcessName;
    G4double          EnergyDep;
    G4double          DeltaTime;
    G4ThreeVector     LocalPosition;
    G4ThreeVector     WorldPosition;
    G4ThreeVector     Momentum;
    G4double          MassVolume;


private:
    static G4ThreadLocal G4Allocator<PANHit>* PANHitAllocator_;

};

typedef G4THitsCollection<PANHit> PANHitsCollection;

inline void* PANHit::operator new(size_t) {
    if (PANHitAllocator_ == NULL)
        PANHitAllocator_ = new G4Allocator<PANHit>();
    return static_cast<void*>(PANHitAllocator_->MallocSingle());
}

inline void PANHit::operator delete(void* aHit) {
    PANHitAllocator_->FreeSingle(static_cast<PANHit*>(aHit));
}

#endif
