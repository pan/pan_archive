#ifndef PANSensitiveDetector_HH
#define PANSensitiveDetector_HH

#include "G4VSensitiveDetector.hh"
#include "PANHit.hh"

class PANSensitiveDetector: public G4VSensitiveDetector {
public:
    PANSensitiveDetector(G4String SDName, G4String HCName);
    ~PANSensitiveDetector();

    void Initialize(G4HCofThisEvent* hitCollection);

    //instruments
    G4bool ProcessMiniPAN_Jun2022(G4Step*, G4LogicalVolume*);

    G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);
    G4bool ProcessShieldingInfo(G4Step*, G4LogicalVolume*);
    G4bool ProcessAlCaseInfo(G4Step*, G4LogicalVolume*);
    G4bool ProcessMagnetInfo(G4Step*, G4LogicalVolume*);
    G4bool ProcessBerylliumInfo(G4Step*, G4LogicalVolume*);
    G4bool ProcessSiliconBoxInfo(G4Step*, G4LogicalVolume*);
    G4bool ProcessPixSensorInfo(G4Step*, G4LogicalVolume*);
    G4bool ProcessPixAsicInfo(G4Step*, G4LogicalVolume*);
    G4bool Process_MiniPAN_StripXY(G4Step*, G4LogicalVolume*);
    void EndOfEvent(G4HCofThisEvent* hitCollection);

private:
    PANHitsCollection * fPANHitsCollection_;

    G4LogicalVolume* fShieldingVolume;
    G4LogicalVolume* fAlCaseVolume;
    G4LogicalVolume* fMagnetVolume;
    G4LogicalVolume* fBerylliumVolume;
    G4LogicalVolume* fSiliconBoxVolume;
    G4LogicalVolume* f_PixelSensorVolume;
    G4LogicalVolume* f_PixelAsicVolume;
    G4LogicalVolume* fSiStripXVolume;
    std::vector<G4LogicalVolume*> fSiStripYVolume;
 
    void CheckHitStatus(PANHit*);

    PANSDConfig* fPANSDConfig;
    PANGlobalConfig* fPANGlobalConfig;

};

#endif
