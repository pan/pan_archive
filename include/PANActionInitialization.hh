#ifndef PANActionInitialization_HH
#define PANActionInitialization_HH

#include "G4RunManager.hh"
#include "G4VUserActionInitialization.hh"
#include "G4String.hh"
#include "globals.hh"
#include "G4AnalysisManager.hh"

#include "PANPrimaryGeneratorAction.hh"
#include "PANRunAction.hh"
#include "PANEventAction.hh"
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

// instrument
#include "instr_MiniPAN_Jun2022.hh"

#include <map>

class PANActionInitialization: public G4VUserActionInitialization {
private:
    PANPrimaryGeneratorAction* fPrimaryGeneratorAction_;

    G4String  output_file_;
    G4bool    fixed_filename_;

    PANSDConfig*     fPANSDConfig;
    PANGlobalConfig* fPANGlobalConfig;

    void Create_MiniPAN_Tree(G4AnalysisManager*) const;
    void Add_MiniPAN_PrimaryTree(G4AnalysisManager*) const;
    void Add_MiniPAN_Config(G4AnalysisManager*) const;

public:
    PANActionInitialization(G4String the_output_file = "output.root", G4bool fixed_name = false);
    ~PANActionInitialization();

    PANPrimaryGeneratorAction* GetPrimaryGeneratorAction();

    void Build() const;
    void BuildForMaster() const;

};

#endif
