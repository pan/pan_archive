#ifndef PANSDConfig_HH
#define PANSDConfig_HH

#include "globals.hh"
#include "PANGlobalConfig.hh"

class PANSDMessenger;

class PANSDConfig {
  
  private:
    PANSDMessenger*   fPANSDMessenger_;
    PANGlobalConfig*  fPANGlobalConfig;
    std::map<G4String,G4int> treeID_;
    std::map<G4String,G4int> histID_;

  protected:
    PANSDConfig();
    ~PANSDConfig();

  public:
    static PANSDConfig* Instance();
    static void Destroy();

    static PANSDConfig* fSDinstance_;

    void print_config();
    G4String IsSD(G4bool);

    void GenerateTreeKeys(G4bool);

    //MiniPAN
    G4bool AddSD_SiStripX;
    G4bool AddSD_SiStripY;

    G4bool AddSD_PixSensor;
    G4bool AddSD_PixAsic;


    std::map<G4String,G4int> GetTreeID() const {return treeID_;}
    std::map<G4String,G4int> GetHistID() const {return histID_;}
};

#endif
