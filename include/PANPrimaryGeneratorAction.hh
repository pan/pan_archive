#ifndef PANPrimaryGeneratorAction_HH
#define PANPrimaryGeneratorAction_HH

#include "G4VUserPrimaryGeneratorAction.hh"
#include "PANGlobalConfig.hh"


class G4GeneralParticleSource;
class G4ParticleGun;

class PANPrimaryGeneratorAction: public G4VUserPrimaryGeneratorAction {

private:
    G4ParticleTable*         fParticleTable_;

    G4GeneralParticleSource* fParticleSource_;
 
    PANGlobalConfig* fPANGlobalConfig;

public:
    PANPrimaryGeneratorAction();
    ~PANPrimaryGeneratorAction();

    void GeneratePrimaries(G4Event* anEvent);

};

#endif
