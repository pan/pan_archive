#ifndef PANEventAction_HH
#define PANEventAction_HH

#include "G4UserEventAction.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include <vector>

class PANEventAction: public G4UserEventAction {

  public:
    PANEventAction();
    ~PANEventAction();

    void BeginOfEventAction(const G4Event* anEvent);
    void EndOfEventAction(const G4Event* anEvent);

};



#endif
