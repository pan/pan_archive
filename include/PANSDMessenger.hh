#ifndef PANSDMessenger_HH
#define PANSDMessenger_HH

#include "globals.hh"
#include "G4UImessenger.hh"
#include "PANSDConfig.hh"

class G4UIdirectory;
class G4UIcmdWithABool;

class PANSDConfig;

/// Messenger class that defines commands for SensitiveDetector.

class PANSDMessenger: public G4UImessenger
{
  private:
    PANSDConfig* fSDConfig_;

  public:
    PANSDMessenger( PANSDConfig* );
    ~PANSDMessenger();

    void SetNewValue(G4UIcommand*, G4String); 

  private:
    G4UIdirectory* fSDConfigDir_;

    G4UIcmdWithABool* fSDConfig_SiStripXCmd_;
    G4UIcmdWithABool* fSDConfig_SiStripYCmd_;

    G4UIcmdWithABool* fSDConfig_PixSensorCmd_;
    G4UIcmdWithABool* fSDConfig_PixAsicCmd_;

};

#endif
