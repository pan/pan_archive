#ifndef PANTabulatedField3D_HH
#define PANTabulatedField3D_HH

#include "globals.hh"
#include "G4MagneticField.hh"
#include "G4ios.hh"

#include <fstream>
#include <vector>
#include <cmath>

class PANTabulatedField3D : public G4MagneticField {
  
  // Storage space for the table
  std::vector< std::vector< std::vector< double > > > xField;
  std::vector< std::vector< std::vector< double > > > yField;
  std::vector< std::vector< std::vector< double > > > zField;
  // The dimensions of the table
  int nX,nY,nZ;
  int StepSize;
  // The physical limits of the defined region
  double minx, maxx, miny, maxy, minz, maxz;
  // The physical extent of the defined region
  double dx, dy, dz;
  double fZoffset;
  bool invertX, invertY, invertZ;

public:
  PANTabulatedField3D(const char* filename, G4double);
  void  GetFieldValue( const  double Point[4], double *Bfield ) const;

};


#endif