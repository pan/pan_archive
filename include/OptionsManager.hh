#ifndef OPTIONSMANAGER_H
#define OPTIONSMANAGER_H

#include <G4ios.hh>
#include <G4String.hh>

#include <cstdlib>
#include <vector>
#include "Constants.hh"

class OptionsManager {
private:
    G4bool version_flag_;
    G4bool case_g(G4int, char**);   //-g
    G4bool case_i(G4int, char**);   //-i
    G4bool case_o(G4int, char**);   //-o
    G4bool case_c(G4int, char**);   //-c
    
public:
    string vis_mac_file;
    string primary_file;
    string run_mac_file;
    string output_file;             // -o
    string config_file;             // -c

private:
    G4bool   gui_flag;              // -g
    G4bool   interactive_flag;      // -i
    G4bool   gps_flag;
    G4bool   rpd_flag;
    G4bool   fixed_name;

public:
    OptionsManager();
    ~OptionsManager();

    G4bool parse(G4int argc_par, char** argv_par);
    void print_help();
    void print_version();
    void print_options();
    void init();
    G4bool GetVersion();
    G4bool IsGPS();
    G4bool IsGuiVis();
    G4bool IsTerminalVis();
    G4bool IsNameFixed();
    
};

#endif
