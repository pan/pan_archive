#ifndef detStripPCB_HH
#define detStripPCB_HH

// units
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4LogicalVolume.hh"
#include "G4Types.hh"

#include "detMagnet.hh"

class G4Material;

class detStripPCB{

public:
    detStripPCB();
    ~detStripPCB();

    G4LogicalVolume* ConstructStripPCB();

    const G4double PCB_Box_x = 119.9 * mm;
    const G4double PCB_Box_y = 142.5 * mm;
    const G4double PCB_Box_z = 1.5 * mm;
    const G4double PCB_Box_CutOut_x = 15 * mm;
    const G4double PCB_Box_CutOut_y = 22.5 * mm;
    const G4double PCB_HoleCenterOffset  = 60 * mm;

private:

    //material
    G4Material* FR4_;

    void DefineMaterials_();

    detMagnet Magnet;
 
};

#endif