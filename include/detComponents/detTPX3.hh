#ifndef detTPX3_HH
#define detTPX3_HH

// units
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4LogicalVolume.hh"
#include "G4Types.hh"

// configure
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

//sensitive detector
#include "G4SDManager.hh"
#include "PANSensitiveDetector.hh"

//tools
#include <map>

class G4Material;

class detTPX3{

public:
    detTPX3();
    ~detTPX3();

    G4LogicalVolume* ConstructTimePix3_();
    G4LogicalVolume* GetPixelPCBLog() const {return PixelPCBLog_;}
    G4LogicalVolume* GetPixelASICLog() const {return PixelASICLog_;}
    G4LogicalVolume* GetPixelSensorLog() const {return PixelSensorLog_;}

    PANSensitiveDetector* AddSD(G4LogicalVolume*,std::map<G4LogicalVolume*,G4String>&);

    G4int GetNoPixSensorPlacements() const {return fNoPixSensorPlacements;}
    G4int GetNoPixAsicPlacements() const {return fNoPixAsicPlacements;}

	
    const G4double Pixel_detector_width     = 14.08 * mm;
    const G4double Pixel_detector_thickness = 300 * um;
    const G4double Pixel_asic_thickness     = 800 * um;
    const G4double Pixel_PCB_Width          = 10.0*cm;
    const G4double Pixel_PCB_Thickness      = 1.4*mm;
    const G4double bump_bond_height         = 18 * um;
    const G4double Pixel_epsilon            = 0.005 * nm; 

    const G4int nTpx3Layers        = 2;



private:

	void DefineMaterials_();

	//configuration
    PANGlobalConfig* fPANGlobalConfig;
    PANSDConfig*     fPANSDConfig;

    //material
    G4Material* AmbientEnv_;
    G4Material* TPx3_FR4_;
    G4Material* G4_Si_;
    G4Material* SiO2_;

    G4LogicalVolume* PixelPCBLog_;
	G4LogicalVolume* PixelSensorLog_;
	G4LogicalVolume* PixelASICLog_;

    G4int fNoPixSensorPlacements;
    G4int fNoPixAsicPlacements;
    
};

#endif
