#ifndef detTPX4_HH
#define detTPX4_HH

// units
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4LogicalVolume.hh"
#include "G4Types.hh"

// configure
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

//sensitive detector
#include "G4SDManager.hh"
#include "PANSensitiveDetector.hh"

//tools
#include <map>

class G4Material;

class detTPX4{

public:
	detTPX4();
    //detTPX4(int,G4double);
    ~detTPX4();

    G4LogicalVolume* ConstructTimePix4_(bool=true);
    G4LogicalVolume* GetPixSensorLog() const {return PixelSensorLog_;}
    G4LogicalVolume* GetPixASICLog() const {return PixelASICLog_;}

    PANSensitiveDetector* AddSD(G4LogicalVolume*,std::map<G4LogicalVolume*,G4String>&);

    G4int GetNoPixSensorPlacements() const {return fNoPixSensorPlacements;}
    G4int GetNoPixAsicPlacements() const {return fNoPixAsicPlacements;}
	
	//pixel
	const G4double Pix_x     = 55 * um;   //55um / 4
	const G4double Pix_y     = 55 * um;    //55um * 32
	G4double Pix_z           = 300 * um;
	const G4int    nPix_x    = 448;
	const G4int    nPix_y    = 512;
	const G4double TimePix4_gap = 55 * um;

	//ASIC
	const G4double ASIC_outer_Ring_x = 52.12 * mm;
	const G4double ASIC_outer_Ring_y = 58.32 * mm;
	const G4double ASIC_outer_Ring_width = 511 * um;

	const G4double ASIC_inner_Gap_x = ASIC_outer_Ring_x - 2 * ASIC_outer_Ring_width;
	const G4double ASIC_inner_Gap_y = ASIC_outer_Ring_y - 2 * ASIC_outer_Ring_width;
	const G4double ASIC_inner_Gap_width_x = 218 * um;
	const G4double ASIC_inner_Gap_width_y = 228 * um;

	const G4double ASIC_inner_Ring_x = ASIC_inner_Gap_x - 2 * ASIC_inner_Gap_width_x;
	const G4double ASIC_inner_Ring_y = ASIC_inner_Gap_y - 2 * ASIC_inner_Gap_width_x;
	const G4double ASIC_inner_Ring_width = 248 * um;

	const G4double bond_ring_radius = 10 * um;

	const G4double Asic_x    = 55 * um;
	const G4double Asic_y    = 55 * um;
	const G4int    nAsic_x   = 448;
	const G4int    nAsic_y   = 512;	
	G4double PixASIC_z       = 100 * um;

	int sensorConfig; //[0] long pixel, [1] 55x55 pixels

	void SetSensorThickness(G4double s_thickness) {Pix_z = s_thickness;}
	void SetASICThickness(G4double a_thickness) {PixASIC_z = a_thickness;}
	void SetSensorMaterial(G4Material* s_material) {SensorMat_ = s_material;}
	void SetSensorConfig(G4int sConfig) {sensorConfig = sConfig;}
	void SetSD_prefix(G4String sd_prefix) {SD_prefix = sd_prefix;}

private:

	void DefineMaterials_();

	//configuration
    PANGlobalConfig* fPANGlobalConfig;
    PANSDConfig*     fPANSDConfig;

    //material
    G4Material* AmbientEnv_;
    G4Material* G4_Si_;
    G4Material* SensorMat_;

	G4LogicalVolume* PixelSensorLog_;
	G4LogicalVolume* PixelASICLog_;

    G4int fNoPixSensorPlacements;
    G4int fNoPixAsicPlacements;

    G4String SD_prefix;
    
};

#endif
