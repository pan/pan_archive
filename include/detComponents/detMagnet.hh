#ifndef detMagnet_HH
#define detMagnet_HH

// units
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4LogicalVolume.hh"
#include "G4Types.hh"

// configure
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

//sensitive detector
#include "G4SDManager.hh"
#include "PANSensitiveDetector.hh"

//tools
#include <utility>
#include <map>

class G4Material;

class detMagnet{

public:
    detMagnet();
    ~detMagnet();

    std::pair<G4LogicalVolume*,G4LogicalVolume*> ConstructMagnet_();
    G4LogicalVolume* GetMagnetLog() const {return divMagLog_;}

    G4LogicalVolume* ConstructAluCase_();
    G4LogicalVolume* GetAlCaseLog() const {return AlCaseLog_;}

    PANSensitiveDetector* AddSD(G4LogicalVolume*,std::map<G4LogicalVolume*,G4String>&);

    G4int GetNoMagnetPlacements() const {return fNoMagnetPlacements;}

    //--- Magnet ---
    const G4double magnet_length = 50 * mm;
    const G4double mag_inR       = 25 * mm;
    const G4double mag_outR      = mag_inR + 10 * mm;
    const G4int nMagComponents   = 16;

    //--- Alu Case ---
    const G4double Al_case_inR   = mag_outR;
    const G4double Al_case_outR  = Al_case_inR + 3 * mm;



private:

	void DefineMaterials_();

	//configuration
    PANGlobalConfig* fPANGlobalConfig;
    PANSDConfig*     fPANSDConfig;

    //material
    G4Material* AmbientEnv_;
    G4Material* Nd2Fe14B_;
    G4Material* G4_Al_;

    //logical volumes
    G4LogicalVolume* divMagLog_;
    G4LogicalVolume* AlCaseLog_;

    G4int fNoMagnetPlacements; 
    
};

#endif
