#ifndef detStripXY_HH
#define detStripXY_HH

// units
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4LogicalVolume.hh"
#include "G4Types.hh"

// configure
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

//sensitive detector
#include "G4SDManager.hh"
#include "PANSensitiveDetector.hh"

//tools
#include <string>
#include <vector>

class G4Material;

class detStripXY{

public:
    detStripXY();
    ~detStripXY();

    G4LogicalVolume* ConstructStripX();
    G4LogicalVolume* ConstructStripY();
    G4LogicalVolume* GetSiStripXVolume() const {return SiStripXLog_;}
    std::vector<G4LogicalVolume*> GetSiStripYVolume() const {return SiStripYLog_;}

    PANSensitiveDetector* AddSD(G4LogicalVolume*,std::map<G4LogicalVolume*,G4String>&);
    PANSensitiveDetector* AddSD(G4LogicalVolume*,std::map<G4LogicalVolume*,G4String>&,int);

    G4int GetNoStripXPlacements() const {return fNoStripXPlacements;}
    G4int GetNoStripYPlacements() const {return fNoStripYPlacements;}

    const G4int SiStripX_nStrips      = 2048;
    const G4int SiStripY_nStrips      = 128;
    const G4double SiStrip_thickness  = 150 * um;
    
private:

    //configuration
    PANGlobalConfig* fPANGlobalConfig;
    PANSDConfig*     fPANSDConfig;

    //material
    G4Material* AmbientEnv_;
    G4Material* G4_Si_;

    void DefineMaterials_();

    //volumes
    G4LogicalVolume* SiStripXLog_;
    std::vector<G4LogicalVolume*> SiStripYLog_;

    //Silicon Strip
    G4int fNoStripXPlacements;
    G4int fNoStripYPlacements;
    const G4double SiStripX_length      = 51.2 * mm;
    const G4double SiStripY_length_base = 51.2 * mm;

    const G4double SiStripY_z[3]      = {-73.5*mm, 6.0*mm, 73.5*mm}; // nominal (2021.Nov.26)

    const G4double SiStripX_pitch = 25 * um;
    const G4double SiStripY_pitch = 400 * um;
    const G4double SiStripX_width = 13 * um;
    const G4double SiStripY_width = 380 * um;

};

#endif