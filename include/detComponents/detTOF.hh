#ifndef detTOF_HH
#define detTOF_HH

// units
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4LogicalVolume.hh"
#include "G4Types.hh"

// configure
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

//sensitive detector
#include "G4SDManager.hh"
#include "PANSensitiveDetector.hh"

class G4Material;

class detTOF{

public:
    detTOF();
    ~detTOF();

    G4LogicalVolume* ConstructTOF();
    G4LogicalVolume* GetScintLog() const {return TOF_ScintLog_;}
    G4LogicalVolume* GetSiPMLog() const {return TOF_SiPM_Log_;}

    PANSensitiveDetector* AddSD(G4LogicalVolume*,std::map<G4LogicalVolume*,G4String>&);


private:

    //configuration
    PANGlobalConfig* fPANGlobalConfig;
    PANSDConfig*     fPANSDConfig;

    //material
    G4Material* AmbientEnv_;
    G4Material* FR4_;
    G4Material* G4_Si_;
    G4Material* SiliconResin;
    G4Material* vikuiti;
    G4Material* EJ_230_;

    void DefineMaterials_();

    //some physcial constants
    G4double Photon3Energy[3] = {2.48*eV, 2.93*eV, 3.10*eV};

    G4LogicalVolume* TOF_ScintLog_;
    G4LogicalVolume* TOF_SiPM_Log_;

    const G4double Scin_xy             = 65 * mm;
    const G4double Scin_thickness      = 6 * mm;
    const G4double SiPM_xy             = 6 * mm;
    const G4double SiPM_thickness      = 1 * mm;
    const G4double SiliconResin_height = 0.1 * mm;      //thickness of Silicon resin
    const G4double MPPC_height         = 0.00193 * mm;  //MPPC thickness (calculations from NDA. Hamamatsu states "several um" in e-mail)
    const G4double VikuitiThickness    = 0.02 *mm;
    const G4double AirGap              = 0.01 * mm;
    const G4double TOF_Scin_center[2]  = {-82.4 * mm, 82.4 * mm};
    const G4int TOF_nSiPM_arrays       = 4;
    const G4double TOF_SiPM_pos[4]     = {-7.5 * mm, 0, 7.5 * mm, 22.5 * mm};
    const G4double TOF_SiPM_offset     = 33 * mm;

    const G4double TOF_box_xy = Scin_xy + 2*SiPM_thickness;
    const G4double TOF_box_z  = Scin_thickness + 2*AirGap + 2*VikuitiThickness;
 
};

#endif