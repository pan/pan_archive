#ifndef PANRUN_HH
#define PANRUN_HH

#include "MiniPANRun_Jun2022.hh"

#include "G4Run.hh"
#include "G4Event.hh"
#include "globals.hh"
#include "G4AnalysisManager.hh"

#include "PANPrimaryGeneratorAction.hh"
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"
#include "PANActionInitialization.hh"
#include "PANPrimaryGeneratorAction.hh"

#include <vector>

class MiniPANRun_Jun2022;

class PANRun: public G4Run {
public:
    PANRun();
    ~PANRun();

    void RecordEvent(const G4Event* anEvent);
    void EndOfRun() const;
    void Merge(const G4Run* aRun);

    G4int GetTotalEvents()   const {return totalEvents_;}

private:

    PANPrimaryGeneratorAction* fPrimaryGeneratorAction_;

    MiniPANRun_Jun2022* MiniPAN_Jun2022;

    G4int totalEvents_;
    G4int totalElectronEvents_;
    G4int totalProtonEvents_;

    PANGlobalConfig*        fPANGlobalConfig;
    PANSDConfig*            fPANSDConfig;

    //booleans to write doses
    G4bool write_MiniPAN_stripX;
    G4bool write_MiniPAN_stripY;

    void WriteData_Primary(G4PrimaryVertex*, const PANDetectorConstruction*, const G4Event*, G4AnalysisManager*, std::vector<G4String>, std::vector<G4int>);
    void WriteData_FluxInside(PANHitsCollection*, G4AnalysisManager*, G4int);

    G4int GetTreeID(G4String) const;
    G4int GetHistID(G4String) const;
};

#endif
