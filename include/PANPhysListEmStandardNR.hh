#ifndef PANPhysListEmStandardNR_HH
#define PANPhysListEmStandardNR_HH

#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class PANPhysListEmStandardNR : public G4VPhysicsConstructor
{
public: 
  PANPhysListEmStandardNR(const G4String& name = "standardNR");
 ~PANPhysListEmStandardNR();

public: 
  // This method is dummy for physics
  virtual void ConstructParticle() {};
 
  // This method will be invoked in the Construct() method.
  // each physics process will be instantiated and
  // registered to the process manager of each particle type 
  virtual void ConstructProcess();
};

#endif
