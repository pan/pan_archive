#ifndef PANRunAction_HH
#define PANRunAction_HH

#include "globals.hh"
#include "G4UserRunAction.hh"
#include "PANSDConfig.hh"
#include "PANGlobalConfig.hh"
#include "PANRun.hh"

class PANRun;

class PANRunAction: public G4UserRunAction {
private:
    G4String output_file_;
    G4bool   fixed_output_;
    static G4int randomSeed_;
    static G4int runId_;

    PANSDConfig*     fPANSDConfig;
    PANGlobalConfig* fPANGlobalConfig;
    
    PANRun* fRun;

    G4int GetTreeID(G4String) const;
    
public:
    PANRunAction(G4String the_output_file_, G4bool fixed_name = false);
    ~PANRunAction();

    G4Run* GenerateRun();
    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run*);
    
};

#endif
