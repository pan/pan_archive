#ifndef MiniPANRUN_Jun2022_HH
#define MiniPANRUN_Jun2022_HH

#include "globals.hh"

#include "G4Run.hh"
#include "G4AnalysisManager.hh"

#include "PANHit.hh"
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"
#include "PANDetectorConstruction.hh"
#include "PANActionInitialization.hh"

#include <vector>

class MiniPANRun_Jun2022: public G4Run {
public:
    MiniPANRun_Jun2022();
    ~MiniPANRun_Jun2022();

    void RecordEvent(const G4Event*,const PANDetectorConstruction*);
    void Merge(const G4Run*);
    G4bool WritePrimary(G4int);

    void WritePrimaryDose(G4AnalysisManager*, const PANDetectorConstruction*, G4int&);
    void WriteData(G4PrimaryVertex*, G4AnalysisManager*, const PANDetectorConstruction*, G4int);
    void WriteData_StripX(G4PrimaryVertex*, PANHitsCollection*, G4AnalysisManager*, G4int, G4int);
    void WriteData_StripY(G4PrimaryVertex*, PANHitsCollection*, G4AnalysisManager*, G4int, G4int);
    void WriteData_PixSensor(G4PrimaryVertex*, PANHitsCollection*, G4AnalysisManager*, G4int, G4int);
    void WriteData_PixAsic(G4PrimaryVertex*, PANHitsCollection*, G4AnalysisManager*, G4int, G4int);
    void WriteData_FluxInside(PANHitsCollection* , G4AnalysisManager* , G4int);
    void AddConfig(G4AnalysisManager*);

    G4int GetTotalEvents() const {return totalEvents_;}
    G4int GetTotalElectronEvents() const {return totalElectronEvents_;}
    G4int GetTotalProtonEvents() const {return totalProtonEvents_;}


private:

    G4int totalEvents_;
    G4int totalElectronEvents_;
    G4int totalProtonEvents_;

    //total doses 
    G4double fDose_StripX[2][6];    //proton/electron & 6 layers
    G4double fDose_StripY[2][3];    //proton/electron & 3 layers

    G4double fDose_PixSensor[2][2]; //proton/electron & 2 layers
    G4double fDose_PixAsic[2][2]; //proton/electron & 2 layers

    PANGlobalConfig*        fPANGlobalConfig;
    PANSDConfig*            fPANSDConfig;

    //booleans to write doses
    G4bool write_MiniPAN_stripX;
    G4bool write_MiniPAN_stripY;
    G4bool write_MiniPAN_pixSensor;
    G4bool write_MiniPAN_pixAsic;

    std::vector<G4String> sPANSDHitCollection;
    std::vector<PANHitsCollection*> fPANHitsCollection;
    std::vector<G4int> numberOfHits;

    std::vector<G4String> sTotalNumberOfHits;
    std::vector<G4int>    TotalNumberOfHits;

    G4int GetTreeID(G4String) const;
    G4int GetHistID(G4String) const;
    void CheckHC_Status(PANHitsCollection*, G4int);
    G4double GetMaxEnergyDeposit(PANHitsCollection*, G4int, G4int);

};


#endif
