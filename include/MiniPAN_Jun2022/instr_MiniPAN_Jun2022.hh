#ifndef MiniPAN_Jun2022_HH
#define MiniPAN_Jun2022_HH

#include "G4VUserDetectorConstruction.hh"
#include "G4RunManager.hh"

// instrument components
#include "detTPX3.hh"
#include "detMagnet.hh"
#include "detTOF.hh"
#include "detStripPCB.hh"
#include "detStripXY.hh"

//units
#include "G4Cache.hh"
#include "G4MagneticField.hh"

// tools
#include <vector>

// configure
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

class MiniPAN_Jun2022_Instrument {

public:
    MiniPAN_Jun2022_Instrument();
    ~MiniPAN_Jun2022_Instrument();

    G4VPhysicalVolume* Construct();
    void ConstructSDandField(std::map<G4LogicalVolume*,G4String>&);
    
    detTPX3 GetdetTPX3() const {return TimePix3;}
    detMagnet GetdetMagnet() const {return Magnet;}
    detStripXY GetdetStripXY() const {return StripXY;}

    G4int GetnStripX(){return SiStripX_nLayers;}
    G4int GetnStripY(){return SiStripY_nLayers;}

private:

	void DefineMaterials_();
    G4bool materials_defined;

	G4LogicalVolume* ConstructMiniPAN_Jun2022_Detector_();
    G4bool ConstructMiniPAN_CenterAluFrame();
    G4bool ConstructMiniPAN_OuterAluFrame();
    G4bool ConstructMiniPAN_AluSupport();

    G4LogicalVolume* AluSupportLog_;
    G4LogicalVolume* OuterAluFrameLog_;
    G4LogicalVolume* CenterAluFrameLog_;

    //configuration
    PANGlobalConfig* fPANGlobalConfig;
    PANSDConfig*     fPANSDConfig;

    G4Cache<G4MagneticField*> fMagField;  //pointer to the thread-local fields

    //available general purpose components
    detTPX3 TimePix3;
    detMagnet Magnet;
    detTOF TOF;
    detStripPCB StripPCB;
    detStripXY StripXY;

    //materials
    G4Material* AmbientEnv_;
    G4Material* Polythene_Al_;
    G4Material* G4_Al_;
    G4Material* G4_Si_;

    //--- MiniPAN ---

    const G4double MiniPAN_DetectorBox_x = 200 * mm;
    const G4double MiniPAN_DetectorBox_y = 200 * mm;
    const G4double MiniPAN_DetectorBox_z = 250 * mm;

    //mechanical error
    const G4double moduleOffset_z = 1.4 * mm;

    //MLI
    const G4double MLI_XY       = 200 * mm;
    const G4double MLIThickness = 100 * um;
    const G4double MLIGap       = 50 * um;
    const G4int nMLI            = 33;

    //Magnet configuration
    const G4int nMagnets = 2;
    const G4double MiniPAN_magGap = 12 * mm;
    const G4double MiniPAN_MagPos_z[2] = {-(MiniPAN_magGap/2.+Magnet.magnet_length/2),(MiniPAN_magGap/2.+Magnet.magnet_length/2)};

    //PCB configuration
    const G4double nPCB_segments     = 3;
    const G4double PCB_Gap2Magnet[2] = {2.2 * mm, 0.3 * mm};
    const G4double PCB_Gap[6]        = {8.3 * mm, 0.1 * mm, 4.9 * mm, .1 * mm, 8.3 * mm, .1 * mm};

    //TimePix3
    const G4int nTpx3Layers        = 2;
    const G4double Tpx3Distance[2] = {-76 * mm, 76 * mm};

    //TOF
    const G4int nTOF = 2;
    const G4double TOF_Center[2] = {-82.4 * mm, 82.4 * mm};

    //StripXY
    const G4int SiStripX_nLayers  = 6;
    const G4int SiStripY_nLayers  = 3;


    //// Support ////
    //CutOut1 (gap above/below circle)
    const G4double CutOut1_box_x = 86 * mm;
    const G4double CutOut1_box_y = 23 * mm;
    const G4double CutOut1_thickness = 53 * mm; //as long as it is thicker than (52.5mm)
    const G4double CutOut1_edgeR = 5 * mm;
    const G4double CutOut1_Voidbox_x = 7.9 * mm;
    const G4double CutOut1_Voidbox_y = 9 * mm;
    const G4double CutOut1_BottomVoidbox_x = 60 * mm;
    const G4double CutOut1_BottomVoidbox_y = 9 * mm;
    const G4double CutOut1_VoidOffset_x = 13 * mm;
    const G4double CutOut1_CenterOffset_y = Magnet.Al_case_outR + 10*mm;
    const G4double CutOut1_SliceCenterWidth = 40 * mm;
    const G4double CutOut1_position_y = 29.5 * mm;

    //CutOut2
    const G4double CutOut2_box_x = 35 * mm;
    const G4double CutOut2_box_y = 115 * mm;
    const G4double CutOut2_box_z = 42 * mm;

    //CutOut3
    const G4double CutOut3_box_x = 151 * mm;
    const G4double CutOut3_box_y = 5 * mm;
    const G4double CutOut3_box_z = 36.5 * mm;
    const G4double CutOut3_Voidbox_x = 85 * mm;
    const G4double CutOut3_Voidbox_y = 5 * mm;
    const G4double CutOut3_Voidbox_z = 20 * mm;

    const G4double Support_box_x = 165 * mm;
    const G4double Support_box_y = 165 * mm;
    const G4double Support_box_z = 52.5 * mm;

    //// Outer Frame ////
    //CutOut1 (gap above/below circle)
    const G4double Frame_CutOut1_box_x = 14 * mm;
    const G4double Frame_CutOut1_box_y = 20 * mm;
    const G4double Frame_CutOut1_box_z = 12.9 * mm;
    
    const G4double Frame_CutOut2_box_x = 14 * mm;
    const G4double Frame_CutOut2_box_y = 40 * mm;
    const G4double Frame_CutOut2_box_z = 12.9 * mm;
    
    const G4double Frame_CoutOut01_offset_x  = 4 * mm;
    const G4double Frame_CoutOut12_offset_y[2] = {7.5 * mm, 15 * mm};

    const G4double Frame_CutOut3_box_x = 65 * mm;
    const G4double Frame_CutOut3_box_y = 65 * mm;
    const G4double Frame_CutOut3_box_z = 12.9 * mm;

    const G4double Frame_CutOut4_box_x = 120.4 * mm;
    const G4double Frame_CutOut4_box_y = 120.4 * mm;
    const G4double Frame_CutOut4_box_z = 3.1 * mm;

    const G4double Frame_CutOut5_box_x = 116 * mm;
    const G4double Frame_CutOut5_box_y = 100 * mm;
    const G4double Frame_CutOut5_box_z = 3 * mm;

    const G4double Frame_CutOut6_box_x = 80 * mm;
    const G4double Frame_CutOut6_box_y = 24.5 * mm;
    const G4double Frame_CutOut6_box_z = 13 * mm;

    const G4double Frame_CutOut7_box_x = 90.4 * mm;
    const G4double Frame_CutOut7_box_y = 143 * mm;
    const G4double Frame_CutOut7_box_z = Frame_CutOut4_box_z;

    const G4double Frame_box_x = 165 * mm;
    const G4double Frame_box_y = 165 * mm;
    const G4double Frame_box_z = 12.9 * mm;

    //// Center Frame ////
    const G4double CenterFrame_box_z = 9.5 * mm;

    const G4double CenterFrame_CutOut1_box_x = 14 * mm;
    const G4double CenterFrame_CutOut1_box_y = 50 * mm;
    const G4double CenterFrame_CutOut1_box_z = CenterFrame_box_z + 0.5 * mm;  //as long as it is more than CenterFrame_box_z
    const G4double CenterFrame_CutOut1_OffsetY[2] = {25 * mm, 15 * mm};
    const G4double CenterFrame_CutOut1_OffsetX    = 4 * mm;
    const G4int CenterFrame_nCutOut1 = 4;

    const G4double CenterFrame_CutOut2_box_x = 25 * mm;
    const G4double CenterFrame_CutOut2_box_y = 10 * mm;
    const G4double CenterFrame_CutOut2_box_z = CenterFrame_box_z + 0.5 * mm;  //as long as it is more than CenterFrame_box_z

    const G4double CenterFrame_CutOut7_box_x = 90.4 * mm;
    const G4double CenterFrame_CutOut7_box_y = 142.7 * mm;
    const G4double CenterFrame_CutOut7_box_z = StripPCB.PCB_Box_z;

    const G4double CenterFrame_CutOut8_box_x = 28.1 * mm;
    const G4double CenterFrame_CutOut8_box_y = 15 * mm;
    const G4double CenterFrame_CutOut8_box_z = 1.6 * mm;  //as long as it is more than CenterFrame_box_z
    const G4double CenterFrame_CutOut8_OffsetX = CenterFrame_CutOut2_box_x/2 - CenterFrame_CutOut8_box_x/2 - 2.5 * mm;

    const G4double CenterFrame_CutOut9_box_x = 23 * mm;
    const G4double CenterFrame_CutOut9_box_y = 11.5 * mm;
    const G4double CenterFrame_CutOut9_box_z = CenterFrame_box_z + 0.5 * mm;  //as long as it is more than CenterFrame_box_z
    const G4double CenterFrame_CutOut9_OffsetX = 30.8 * mm;
    const G4double CenterFrame_CutOut9_OffsetY = 31.2 * mm;

};

#endif