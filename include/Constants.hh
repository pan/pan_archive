#ifndef CONSTANTS_H
#define CONSTANTS_H

using namespace std;

const string SW_NAME = "PANsim";

const string SW_VERSION = "v2.1.0";

const string RELEASE_DATE = "2022 Jun 20";

#endif
