#ifndef PANGlobalConfig_HH
#define PANGlobalConfig_HH

#include "globals.hh"

class PANConfigMessenger;

class PANGlobalConfig {
private:
    PANConfigMessenger* fPANConfigMessenger_;    

public:
    PANGlobalConfig();
    ~PANGlobalConfig();

public:
    static PANGlobalConfig* Instance();

private:
    static PANGlobalConfig* fgInstance_;

public:
    // Detector Type
    G4double IsMiniPAN;

    // config parameter list
    G4double hit_threshold;  // lowest energy with unit
    G4double birks_constant;
    G4String output_directory;
    G4int    event_verbose;
    G4bool   primary_only;
    G4bool   spacelab;
    G4bool   IsInSpace;
    G4bool   UseSPENVISdata;
    G4int    SetTrajectory;
    G4int    phys_verbose;
    G4bool   simout_more;
    G4bool   phys_more;
    G4bool   visual_light;
    G4bool   write_geometry;
    G4bool   UseBe;
    G4bool   UseMLI;
    G4bool   UseSSD;
    G4bool   UseTPx3;
    G4bool   UseTOF;
    G4bool   WriteDoseMore;
    G4bool   WriteFluxInside;
    G4bool   AddMagneticField;
    G4double SpectrumEnergyMin;
    G4double SpectrumEnergyMax;


    //trajectory info
    G4double Range;
    G4double latitude;
    G4double longitude;

public:
    void print_config();
    void CheckDetectorFlag();

};

#endif
