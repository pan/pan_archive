#ifndef PANPhysicsList_HH
#define PANPhysicsList_HH

#include "G4VModularPhysicsList.hh"

class G4VPhysicsConstructor;
class StepMax;
class PANPhysicsListMessenger;

class PANPhysicsList: public G4VModularPhysicsList {
public:
    PANPhysicsList(G4int ver = 1);
    ~PANPhysicsList();

    void SetCuts();
    void ConstructParticle();
    
    void AddPhysicsList(const G4String& name);
    void ConstructProcess();
    
    void AddStepMax();       
    StepMax* GetStepMaxProcess() {return fStepMaxProcess;};

private:

  void AddIonGasModels();

  G4bool   fHelIsRegisted;
  G4bool   fBicIsRegisted;
  G4bool   fBiciIsRegisted;
    
  G4String                             fEmName;
  G4VPhysicsConstructor*               fEmPhysicsList;
  G4VPhysicsConstructor*               fDecPhysicsList;
  std::vector<G4VPhysicsConstructor*>  fHadronPhys;    
  StepMax*                             fStepMaxProcess;
    
  PANPhysicsListMessenger*  fMessenger;

  G4VPhysicsConstructor* fIonInelastic=NULL;
  G4VPhysicsConstructor* fIonElastic=NULL;
  G4VPhysicsConstructor* fHadronInelastic=NULL;
  G4VPhysicsConstructor* fRadioactiveDecay=NULL;
  G4VPhysicsConstructor* fGammaNuclear=NULL;
  G4VPhysicsConstructor* fHadronElastic=NULL;
  G4VPhysicsConstructor* fShielding=NULL;
};

#endif