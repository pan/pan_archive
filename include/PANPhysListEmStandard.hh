#ifndef PANPhysListEmStandard_HH
#define PANListEmStandard_HH

#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

class PANPhysListEmStandard : public G4VPhysicsConstructor
{
  public: 
    PANPhysListEmStandard(const G4String& name = "standard");
   ~PANPhysListEmStandard();

  public: 
    // This method is dummy for physics
    virtual void ConstructParticle() {};
 
    // This method will be invoked in the Construct() method.
    // each physics process will be instantiated and
    // registered to the process manager of each particle type 
    virtual void ConstructProcess();
};

#endif








