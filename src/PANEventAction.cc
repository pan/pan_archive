#include "PANEventAction.hh"
#include "PANGlobalConfig.hh"

PANEventAction::PANEventAction() 
: G4UserEventAction()
{
   G4RunManager::GetRunManager();
}

PANEventAction::~PANEventAction() {

}

void PANEventAction::BeginOfEventAction(const G4Event* anEvent) {
    PANGlobalConfig* fPANGlobalConfig = PANGlobalConfig::Instance();
    if (fPANGlobalConfig->event_verbose < 1) return;
    G4int nEvent = anEvent -> GetEventID();
    if((nEvent % 10000000 == 0) && (nEvent != 0)) {
        G4cout << "INFORMATION: " << nEvent << " event in progress..." << G4endl;
    }
}

void PANEventAction::EndOfEventAction(const G4Event* anEvent) {
    PANGlobalConfig* fPANGlobalConfig = PANGlobalConfig::Instance();
    if (fPANGlobalConfig->event_verbose < 1) return;
    G4int nmbEvents = (anEvent -> GetEventID()) + 1;
    if(nmbEvents % 10000 == 0)
    {
        G4cout << "INFORMATION: " << nmbEvents << " events processed." << G4endl;
    }
}
