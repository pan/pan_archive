#include "PANGlobalConfig.hh"
#include "PANConfigMessenger.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

PANGlobalConfig* PANGlobalConfig::fgInstance_ = NULL;

PANGlobalConfig::PANGlobalConfig() {
    if (fgInstance_ != NULL) {
        G4ExceptionDescription description;
        description << "      "
                    << "PANGlobalConfig already exists."
                    <<  "Cannot create another instance.";
        G4Exception("PANGlobalConfig::PANGlobalConfig()", "Config_F001",
                FatalException, description);
    }
    fPANConfigMessenger_ = new PANConfigMessenger(this);
    fgInstance_ = this;

    //Detector Type
    IsMiniPAN   = false;

    // initial value
    hit_threshold      = 0.1*eV;
    birks_constant     = 0.143;
    output_directory   = "";
    event_verbose      = 0;
    primary_only       = false;
    spacelab           = false;
    IsInSpace          = true;
    UseSPENVISdata     = false;
    SetTrajectory      = -1;
    phys_verbose       = 0;
    simout_more        = false;
    phys_more          = false;
    visual_light       = false;
    write_geometry     = false;
    UseBe              = false;
    UseMLI             = false;
    WriteDoseMore      = false;
    WriteFluxInside    = false;
    AddMagneticField   = false;

    SpectrumEnergyMin  = 0*eV;
    SpectrumEnergyMax  = 1e9*GeV;

    //intial trajectory
    Range       = -1;
    latitude    = -1;
    longitude   = -1;
}

PANGlobalConfig* PANGlobalConfig::Instance() {
    if (fgInstance_ == NULL) {
        fgInstance_ = new PANGlobalConfig();
    }
    return fgInstance_;
}

PANGlobalConfig::~PANGlobalConfig() {
    delete fPANConfigMessenger_;
}

void PANGlobalConfig::print_config() {
    G4cout << "=============================================================" << G4endl;
    G4cout << "======================= Detector Type =======================" << G4endl;
    G4cout << "=============================================================" << G4endl; 
    G4cout << " - IsMiniPAN            = " << IsMiniPAN << G4endl;
    G4cout << "=============================================================" << G4endl;
    G4cout << "======================= Configuration =======================" << G4endl;
    G4cout << "=============================================================" << G4endl;
    G4cout << " - hit_threshold        = " << hit_threshold / eV << " eV" << G4endl;
    G4cout << " - output_directory     = " << output_directory << G4endl;
    G4cout << " - event_verbose        = " << event_verbose << G4endl;
    G4cout << " - primary_only         = " << primary_only << G4endl;
    G4cout << " - birks_constant       = " << birks_constant << " mm/MeV" << G4endl;
    G4cout << " - spacelab             = " << spacelab << G4endl;
    G4cout << " - IsInSpace            = " << IsInSpace << G4endl;
    G4cout << " - UseSPENVISdata       = " << UseSPENVISdata << G4endl;
    G4cout << " - SetTrajectory        = " << SetTrajectory << G4endl;
    G4cout << " - phys_verbose         = " << phys_verbose << G4endl;
    G4cout << " - simout_more          = " << simout_more << G4endl;
    G4cout << " - phys_more            = " << phys_more << G4endl;
    G4cout << " - visual_light         = " << visual_light << G4endl;
    G4cout << " - write_geometry       = " << write_geometry << G4endl;
    G4cout << " - UseBe                = " << UseBe << G4endl;
    G4cout << " - UseMLI               = " << UseMLI << G4endl;
    G4cout << " - UseSSD               = " << UseSSD << G4endl;
    G4cout << " - UseTPx3              = " << UseTPx3 << G4endl;
    G4cout << " - UseTOF               = " << UseTOF << G4endl;
    G4cout << " - WriteDoseMore        = " << WriteDoseMore << G4endl;
    G4cout << " - WriteFluxInside      = " << WriteFluxInside << G4endl;
    G4cout << " - AddMagneticField     = " << AddMagneticField << G4endl;
    G4cout << " - SpectrumEnergyMin    = " << SpectrumEnergyMin / keV << " keV" << G4endl;
    G4cout << " - SpectrumEnergyMax    = " << SpectrumEnergyMax / keV << " keV" << G4endl;
    G4cout << "=============================================================" << G4endl;

    CheckDetectorFlag();
}

void PANGlobalConfig::CheckDetectorFlag(){
    int nDet = 0;
    if(IsMiniPAN) nDet++;

    if(nDet == 0){
        G4cout << "ERROR! No detectors selected. Check your configuration file" << G4endl;
        exit(EXIT_FAILURE);
    }

}