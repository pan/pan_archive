#include "PANSDMessenger.hh"
#include "PANSensitiveDetector.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PANSDMessenger::PANSDMessenger( PANSDConfig* theSDConfig )
{
  fSDConfig_ = theSDConfig;

  //set command
  fSDConfigDir_ = new G4UIdirectory("/PAN/SDConfig/set/");
  fSDConfigDir_->SetGuidance("UI commands for sensitive detectors");

  fSDConfig_SiStripXCmd_ = new G4UIcmdWithABool("/PAN/SDConfig/set/AddSD_SiStripX", this);
  fSDConfig_SiStripXCmd_->SetParameterName("AddSD_SiStripX", false);
  fSDConfig_SiStripXCmd_->SetGuidance("Add SD_SiStripX");

  fSDConfig_SiStripYCmd_ = new G4UIcmdWithABool("/PAN/SDConfig/set/AddSD_SiStripY", this);
  fSDConfig_SiStripYCmd_->SetParameterName("AddSD_SiStripY", false);
  fSDConfig_SiStripYCmd_->SetGuidance("Add SD_SiStripY");

  fSDConfig_PixSensorCmd_ = new G4UIcmdWithABool("/PAN/SDConfig/set/AddSD_PixSensor", this);
  fSDConfig_PixSensorCmd_->SetParameterName("AddSD_PixSensor", false);
  fSDConfig_PixSensorCmd_->SetGuidance("Add SD_PixSensor");

  fSDConfig_PixAsicCmd_ = new G4UIcmdWithABool("/PAN/SDConfig/set/AddSD_PixAsic", this);
  fSDConfig_PixAsicCmd_->SetParameterName("AddSD_PixAsic", false);
  fSDConfig_PixAsicCmd_->SetGuidance("Add SD_PixAsic");

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PANSDMessenger::~PANSDMessenger() 
{
  delete fSDConfig_PixSensorCmd_;
  delete fSDConfig_PixAsicCmd_;
  delete fSDConfig_SiStripXCmd_;
  delete fSDConfig_SiStripYCmd_;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PANSDMessenger::SetNewValue(G4UIcommand* command, G4String newValue) 
{
  if (command == fSDConfig_PixSensorCmd_) {
    fSDConfig_->AddSD_PixSensor = fSDConfig_PixSensorCmd_->GetNewBoolValue(newValue);
  } else if (command == fSDConfig_PixAsicCmd_) {
    fSDConfig_->AddSD_PixAsic = fSDConfig_PixAsicCmd_->GetNewBoolValue(newValue);
  } else if (command == fSDConfig_SiStripXCmd_) {
    fSDConfig_->AddSD_SiStripX = fSDConfig_SiStripXCmd_->GetNewBoolValue(newValue);
  } else if (command == fSDConfig_SiStripYCmd_) {
    fSDConfig_->AddSD_SiStripY = fSDConfig_SiStripYCmd_->GetNewBoolValue(newValue);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
