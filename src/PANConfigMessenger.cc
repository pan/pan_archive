#include "PANConfigMessenger.hh"
#include "PANGlobalConfig.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"
#include "G4SystemOfUnits.hh"

PANConfigMessenger::PANConfigMessenger(PANGlobalConfig* theGlobalConfig) {
    fGlobalConfig_ = theGlobalConfig;

    ///////////// MiniPAN commands ///////////// 
    fGlobalConfigIsMiniPANCmd_ = new G4UIcmdWithABool(
            "/PAN/GlobalConfig/set/IsMiniPAN", this);
    fGlobalConfigIsMiniPANCmd_->SetParameterName("IsMiniPAN", false);
    fGlobalConfigIsMiniPANCmd_->SetGuidance("Set IsMiniPAN");

    fGlobalConfigUseMLICmd_ = new G4UIcmdWithABool(
                         "/PAN/GlobalConfig/set/UseMLI", this);
    fGlobalConfigUseMLICmd_->SetParameterName("UseMLI", false);
    fGlobalConfigUseMLICmd_->SetGuidance("Set UseMLI");

    fGlobalConfigUseSSDCmd_ = new G4UIcmdWithABool(
                         "/PAN/GlobalConfig/set/UseSSD", this);
    fGlobalConfigUseSSDCmd_->SetParameterName("UseSSD", false);
    fGlobalConfigUseSSDCmd_->SetGuidance("Set UseSSD");

    fGlobalConfigUseTPx3Cmd_ = new G4UIcmdWithABool(
                         "/PAN/GlobalConfig/set/UseTPx3", this);
    fGlobalConfigUseTPx3Cmd_->SetParameterName("UseTPx3", false);
    fGlobalConfigUseTPx3Cmd_->SetGuidance("Set UseTPx3");

    fGlobalConfigUseTOFCmd_ = new G4UIcmdWithABool(
                         "/PAN/GlobalConfig/set/UseTOF", this);
    fGlobalConfigUseTOFCmd_->SetParameterName("UseTOF", false);
    fGlobalConfigUseTOFCmd_->SetGuidance("Set UseTOF");

    fGlobalConfigBirksConstCmd_ = new G4UIcmdWithADouble(
                        "/PAN/GlobalConfig/set/BirksConstant", this);
    fGlobalConfigBirksConstCmd_->SetParameterName("BirksConstant", false);
    fGlobalConfigBirksConstCmd_->SetGuidance("Set Birks Constant");

    ///////////// general commands /////////////
    fGlobalConfigDir_ = new G4UIdirectory("/PAN/GlobalConfig/set/");
    fGlobalConfigDir_->SetGuidance("GlobalConfig Control.");

    fGlobalConfigHitThrCmd_ = new G4UIcmdWithADoubleAndUnit(
            "/PAN/GlobalConfig/set/hit_threshold",this);
    fGlobalConfigHitThrCmd_->SetParameterName("hit_threshold",false);
    fGlobalConfigHitThrCmd_->SetGuidance("Set hit threshold");
    fGlobalConfigHitThrCmd_->SetUnitCategory("Energy");

    fGlobalConfigOutDirCmd_ = new G4UIcmdWithAString(
            "/PAN/GlobalConfig/set/output_directory", this);
    fGlobalConfigOutDirCmd_->SetParameterName("output_directory", false);
    fGlobalConfigOutDirCmd_->SetGuidance("Set output directory");

    fGlobalConfigEventVerCmd_ = new G4UIcmdWithAnInteger(
            "/PAN/GlobalConfig/set/event_verbose", this);
    fGlobalConfigEventVerCmd_->SetParameterName("event_verbose", false);
    fGlobalConfigEventVerCmd_->SetGuidance("Set event verbose");

    fGlobalConfigPriOnlyCmd_ = new G4UIcmdWithABool(
            "/PAN/GlobalConfig/set/primary_only", this);
    fGlobalConfigPriOnlyCmd_->SetParameterName("primary_only", false);
    fGlobalConfigPriOnlyCmd_->SetGuidance("Set primary only");

    fGlobalConfigIsInSpaceCmd_ = new G4UIcmdWithABool(
                               "/PAN/GlobalConfig/set/IsInSpace", this);
    fGlobalConfigIsInSpaceCmd_->SetParameterName("IsInSpace", false);
    fGlobalConfigIsInSpaceCmd_->SetGuidance("Set IsInSpace");

    fGlobalConfigUseSPENVISdataCmd_ = new G4UIcmdWithABool(
		                 "/PAN/GlobalConfig/set/UseSPENVISdata", this);
    fGlobalConfigUseSPENVISdataCmd_->SetParameterName("UseSPENVISdata", false);
    fGlobalConfigUseSPENVISdataCmd_->SetGuidance("Set UseSPENVISdata");

    fGlobalConfigPhysVerCmd_ = new G4UIcmdWithAnInteger(
            "/PAN/GlobalConfig/set/phys_verbose", this);
    fGlobalConfigPhysVerCmd_->SetParameterName("phys_verbose", false);
    fGlobalConfigPhysVerCmd_->SetGuidance("Set phys_verbose");

    fGlobalConfigSimOutMoreCmd_ = new G4UIcmdWithABool(
            "/PAN/GlobalConfig/set/simout_more", this);
    fGlobalConfigSimOutMoreCmd_->SetParameterName("simout_more", false);
    fGlobalConfigSimOutMoreCmd_->SetGuidance("Set simout_more");

    fGlobalConfigPhysMoreCmd_ = new G4UIcmdWithABool(
	    "/PAN/GlobalConfig/set/phys_more", this);
    fGlobalConfigPhysMoreCmd_->SetParameterName("phys_more", false);
    fGlobalConfigPhysMoreCmd_->SetGuidance("Set phys_more");

    fGlobalConfigVisLightCmd_ = new G4UIcmdWithABool(
            "/PAN/GlobalConfig/set/visual_light", this);
    fGlobalConfigVisLightCmd_->SetParameterName("visual_light", false);
    fGlobalConfigVisLightCmd_->SetGuidance("Set visual_light");

    fGlobalConfigWriteGeoROOTCmd_ = new G4UIcmdWithABool(
            "/PAN/GlobalConfig/set/write_geometry", this);
    fGlobalConfigWriteGeoROOTCmd_->SetParameterName("write_geometry", false);
    fGlobalConfigWriteGeoROOTCmd_->SetGuidance("Set write_geometry");

    fGlobalConfigUseBeCmd_ = new G4UIcmdWithABool(
                         "/PAN/GlobalConfig/set/UseBe", this);
    fGlobalConfigUseBeCmd_->SetParameterName("UseBe", false);
    fGlobalConfigUseBeCmd_->SetGuidance("Set UseBe");

    fGlobalConfigWriteDoseMoreCmd_ = new G4UIcmdWithABool(
                         "/PAN/GlobalConfig/set/WriteDoseMore", this);
    fGlobalConfigWriteDoseMoreCmd_->SetParameterName("WriteDoseMore", false);
    fGlobalConfigWriteDoseMoreCmd_->SetGuidance("Set WriteDoseMore");

    fGlobalConfigAddMagneticFieldCmd_= new G4UIcmdWithABool(
                         "/PAN/GlobalConfig/set/AddMagneticField", this);
    fGlobalConfigAddMagneticFieldCmd_->SetParameterName("AddMagneticField", false);
    fGlobalConfigAddMagneticFieldCmd_->SetGuidance("Set AddMagneticField");

    fGlobalConfigEnergyMinCmd_ = new G4UIcmdWithADoubleAndUnit(
                        "/PAN/GlobalConfig/set/SpectrumEnergyMin", this);
    fGlobalConfigEnergyMinCmd_->SetParameterName("SpectrumEnergyMin", false);
    fGlobalConfigEnergyMinCmd_->SetGuidance("Set EnergyMin");
    fGlobalConfigEnergyMinCmd_->SetDefaultUnit("keV");
    fGlobalConfigEnergyMinCmd_->SetUnitCategory ("Energy"); 

    fGlobalConfigEnergyMaxCmd_ = new G4UIcmdWithADoubleAndUnit(
                        "/PAN/GlobalConfig/set/SpectrumEnergyMax", this);
    fGlobalConfigEnergyMaxCmd_->SetParameterName("SpectrumEnergyMax", false);
    fGlobalConfigEnergyMaxCmd_->SetGuidance("Set EnergyMax");
    fGlobalConfigEnergyMaxCmd_->SetDefaultUnit("keV");
    fGlobalConfigEnergyMaxCmd_->SetUnitCategory ("Energy"); 

}

PANConfigMessenger::~PANConfigMessenger() {
    delete fGlobalConfigIsMiniPANCmd_;
    delete fGlobalConfigDir_;
    delete fGlobalConfigHitThrCmd_;
    delete fGlobalConfigOutDirCmd_;
    delete fGlobalConfigEventVerCmd_;
    delete fGlobalConfigPriOnlyCmd_;
    delete fGlobalConfigIsInSpaceCmd_;
    delete fGlobalConfigNumOfModCmd_;
    delete fGlobalConfigUseSPENVISdataCmd_;
    delete fGlobalConfigPhysVerCmd_;
    delete fGlobalConfigSimOutMoreCmd_;
    delete fGlobalConfigPhysMoreCmd_;
    delete fGlobalConfigVisLightCmd_;
    delete fGlobalConfigWriteGeoROOTCmd_;
    delete fGlobalConfigUseBeCmd_;
    delete fGlobalConfigUseMLICmd_;
    delete fGlobalConfigUseSSDCmd_;
    delete fGlobalConfigUseTPx3Cmd_;
    delete fGlobalConfigUseTOFCmd_;
    delete fGlobalConfigWriteDoseMoreCmd_;
    delete fGlobalConfigAddMagneticFieldCmd_;
    delete fGlobalConfigBirksConstCmd_;
    delete fGlobalConfigEnergyMinCmd_;
    delete fGlobalConfigEnergyMaxCmd_;
}

void PANConfigMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {

    //general commands
    if (command == fGlobalConfigHitThrCmd_) {
        fGlobalConfig_->hit_threshold = fGlobalConfigHitThrCmd_->GetNewDoubleValue(newValue);
    } else if (command == fGlobalConfigOutDirCmd_) {
        fGlobalConfig_->output_directory = newValue;
    } else if (command == fGlobalConfigEventVerCmd_) {
        fGlobalConfig_->event_verbose = fGlobalConfigEventVerCmd_->GetNewIntValue(newValue);
    } else if (command == fGlobalConfigPriOnlyCmd_) {
        fGlobalConfig_->primary_only = fGlobalConfigPriOnlyCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigIsInSpaceCmd_) {
	    fGlobalConfig_->IsInSpace = fGlobalConfigIsInSpaceCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigUseSPENVISdataCmd_) {
	    fGlobalConfig_->UseSPENVISdata = fGlobalConfigUseSPENVISdataCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigPhysVerCmd_) {
        fGlobalConfig_->phys_verbose = fGlobalConfigPhysVerCmd_->GetNewIntValue(newValue);
    } else if (command == fGlobalConfigSimOutMoreCmd_) {
        fGlobalConfig_->simout_more = fGlobalConfigSimOutMoreCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigPhysMoreCmd_) {
        fGlobalConfig_->phys_more = fGlobalConfigPhysMoreCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigVisLightCmd_) {
        fGlobalConfig_->visual_light = fGlobalConfigVisLightCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigWriteGeoROOTCmd_) {
        fGlobalConfig_->write_geometry = fGlobalConfigWriteGeoROOTCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigUseBeCmd_) {
        fGlobalConfig_->UseBe = fGlobalConfigUseBeCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigWriteDoseMoreCmd_) {
        fGlobalConfig_->WriteDoseMore = fGlobalConfigWriteDoseMoreCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigAddMagneticFieldCmd_) {
        fGlobalConfig_->AddMagneticField = fGlobalConfigAddMagneticFieldCmd_->GetNewBoolValue(newValue);
    } if (command == fGlobalConfigIsMiniPANCmd_){
        fGlobalConfig_->IsMiniPAN = fGlobalConfigIsMiniPANCmd_->GetNewBoolValue(newValue);    
    } else if (command == fGlobalConfigUseMLICmd_) {
        fGlobalConfig_->UseMLI = fGlobalConfigUseMLICmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigUseSSDCmd_) {
        fGlobalConfig_->UseSSD = fGlobalConfigUseSSDCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigUseTPx3Cmd_) {
        fGlobalConfig_->UseTPx3 = fGlobalConfigUseTPx3Cmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigUseTOFCmd_) {
        fGlobalConfig_->UseTOF = fGlobalConfigUseTOFCmd_->GetNewBoolValue(newValue);
    } else if (command == fGlobalConfigBirksConstCmd_){
        fGlobalConfig_->birks_constant = fGlobalConfigBirksConstCmd_->GetNewDoubleValue(newValue);
    } else if (command == fGlobalConfigEnergyMinCmd_){
        fGlobalConfig_->SpectrumEnergyMin = fGlobalConfigEnergyMinCmd_->GetNewDoubleValue(newValue);
    } else if (command == fGlobalConfigEnergyMaxCmd_){
        fGlobalConfig_->SpectrumEnergyMax = fGlobalConfigEnergyMaxCmd_->GetNewDoubleValue(newValue);
    }

}
