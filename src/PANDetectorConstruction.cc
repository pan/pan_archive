#include "PANDetectorConstruction.hh"

//material
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"
#include "G4NistManager.hh"

//geometry
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Ellipsoid.hh"
#include "G4Trap.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Sphere.hh"
#include "G4Para.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4MultiUnion.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include <cstdio>
#include <cmath>

//sensitive detector
#include "G4SDManager.hh"
#include "PANSensitiveDetector.hh"

//magnetic field
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4UniformMagField.hh"
#include "PANTabulatedField3D.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//global function
#include "globals.hh"

//VGM
#include "Geant4GM/volumes/Factory.h"
#include "RootGM/volumes/Factory.h"
#include "TGeoManager.h"

PANDetectorConstruction::PANDetectorConstruction() {
    WorldLog_        = NULL;
        
    fPANGlobalConfig = PANGlobalConfig::Instance();
    fPANSDConfig     = PANSDConfig::Instance();
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANDetectorConstruction::~PANDetectorConstruction() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4VPhysicalVolume* PANDetectorConstruction::Construct() {
    //-----------------------------------------------------------------------------------------------------------
    //--------Define the detector geometry
    //-----------------------------------------------------------------------------------------------------------

    DefineMaterials_();

    //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    // Construct WorldPhys
    G4double world_hx = (fPANGlobalConfig->spacelab ? 12*m : 500*mm);
    G4double world_hy = (fPANGlobalConfig->spacelab ? 30*m : 500*mm);
    G4double world_hz = (fPANGlobalConfig->spacelab ? 12*m : 500*mm);
    G4Box* WorldBox = new G4Box("WorldBox", world_hx, world_hy, world_hz);
    WorldLog_ = new G4LogicalVolume(WorldBox, AmbientEnv_, "WorldLog_");

    G4VPhysicalVolume* WorldPhys;
    if (!fPANGlobalConfig->primary_only) {
        // Construct Detector
        G4String detName = "";
        if(fPANGlobalConfig->IsMiniPAN){
          WorldPhys = MiniPAN_Jun2022.Construct(); detName = "MiniPAN";
        }

    }

    //set visualization of World
    G4VisAttributes* VisAtt_WorldLog = new G4VisAttributes(true, G4Colour(1.0, 0.0, 0.0, 1.0));
    VisAtt_WorldLog->SetForceWireframe(!fPANGlobalConfig->visual_light);
    VisAtt_WorldLog->SetVisibility(!fPANGlobalConfig->visual_light);
    WorldLog_->SetVisAttributes(VisAtt_WorldLog);

    // ---------------------------------------------------------------------------
    // Export geometry in Root and save it in a file
  
    // Import Geant4 geometry to VGM
    Geant4GM::Factory g4Factory;
    g4Factory.SetDebug(1);
    g4Factory.Import(WorldPhys);
  
    // Export VGM geometry to Root
    RootGM::Factory rtFactory;
    rtFactory.SetDebug(1);
    g4Factory.Export(&rtFactory);
    gGeoManager->CloseGeometry();
    gGeoManager->Export("DetectorGeometry.root");
    
    //---------------------------------------------------------------------------



    return WorldPhys;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void PANDetectorConstruction::ConstructSDandField() {

    //create SD for MiniPAN
    if (!fPANGlobalConfig->primary_only && fPANGlobalConfig->IsMiniPAN) {
      MiniPAN_Jun2022.ConstructSDandField(instrumentSD);
    }

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void PANDetectorConstruction::DefineMaterials_() {

    G4NistManager* man = G4NistManager::Instance();

    if(fPANGlobalConfig->IsInSpace) {AmbientEnv_ = man->FindOrBuildMaterial("G4_Galactic");}  // for space
    else {AmbientEnv_ = man->FindOrBuildMaterial("G4_AIR");}                                    // for ground

}