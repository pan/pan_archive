#include "detStripPCB.hh"

//material
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"

//geometry
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"


detStripPCB::detStripPCB() {

  DefineMaterials_();

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

detStripPCB::~detStripPCB() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void detStripPCB::DefineMaterials_() {
    //-----------------------------------------------------------------------------------------------------------
    //--------Define Material
    //-----------------------------------------------------------------------------------------------------------
    FR4_ = NULL;

    G4double density;                         // matter density
    G4double fractionmass;                    // compound proportion
    G4int ncomponents;                        // component number
    G4int natoms;                             // atom proportion

    //////////////////////////////////

    G4NistManager* man = G4NistManager::Instance();

    G4Element* elC  = man->FindOrBuildElement("C");
    G4Element* elH  = man->FindOrBuildElement("H");
    G4Element* elN  = man->FindOrBuildElement("N");
    G4Element* elO  = man->FindOrBuildElement("O");
    G4Element* elSi = man->FindOrBuildElement("Si");

    //FR4 for PCB
    //Silicon Oxide
    G4Material* SiliconOxide = new G4Material("SiliconOxide", density = 2.65*g/cm3, ncomponents = 2);
    SiliconOxide->AddElement(elSi, natoms=1);
    SiliconOxide->AddElement(elO,  natoms=2);
    //Diglycidyl Ether of Bisphenol A (First compound of epoxy resin Epotek 301-1)
    G4Material* Epoxy_1 = new G4Material("Epoxy_1", density = 1.16*g/cm3, ncomponents = 3);
    Epoxy_1->AddElement(elC, natoms=19);
    Epoxy_1->AddElement(elH, natoms=20);
    Epoxy_1->AddElement(elO, natoms=4);
    //1,4-Butanediol Diglycidyl Ether (Second compound of epoxy resin Epotek 301-1)
    G4Material* Epoxy_2 = new G4Material("Epoxy_2", density = 1.10*g/cm3, ncomponents = 3);
    Epoxy_2->AddElement(elC, natoms=10);
    Epoxy_2->AddElement(elH, natoms=18);
    Epoxy_2->AddElement(elO, natoms=4);
    //1,6-Hexanediamine 2,2,4-trimetyl (Third compound of epoxy resin Epotek 301-1)
    G4Material* Epoxy_3 = new G4Material("Epoxy_3", density = 1.16*g/cm3, ncomponents = 3);
    Epoxy_3->AddElement(elC, natoms=9);
    Epoxy_3->AddElement(elH, natoms=22);
    Epoxy_3->AddElement(elN, natoms=2);
    //Epoxy resin Epotek 301-1
    G4Material* Epoxy_Resin = new G4Material("Epoxy_Resin", density = 1.21*g/cm3, ncomponents = 3);
    Epoxy_Resin->AddMaterial(Epoxy_1, fractionmass=56*perCent);
    Epoxy_Resin->AddMaterial(Epoxy_2, fractionmass=24*perCent);
    Epoxy_Resin->AddMaterial(Epoxy_3, fractionmass=20*perCent);
    //FR4 PCB material
    FR4_ = new G4Material("FR4", density = 1.8*g/cm3, ncomponents=2);
    FR4_->AddMaterial(SiliconOxide, fractionmass=60*perCent);
    FR4_->AddMaterial(Epoxy_Resin,  fractionmass=40*perCent);

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4LogicalVolume* detStripPCB::ConstructStripPCB(){
  //PCB
  G4Box* PCB_Box   = new G4Box("PCB_Box_thin",PCB_Box_x/2,PCB_Box_y/2,PCB_Box_z/2);
  G4Box* PCB_Box_CutOut = new G4Box("PCB_Box_CutOut",PCB_Box_CutOut_x,PCB_Box_CutOut_y,PCB_Box_z);
  G4Tubs* PCB_CenterHole = new G4Tubs("PCB_CenterHole",0,Magnet.mag_inR,PCB_Box_z,0.,2.* M_PI * rad);

  G4VSolid* PCB_noHole = new G4SubtractionSolid("PCB_Box-PCB_CenterHole", PCB_Box, PCB_CenterHole,NULL, G4ThreeVector(0,PCB_HoleCenterOffset-PCB_Box_y/2.,0));
    
  G4VSolid* PCB_CutOut1 = new G4SubtractionSolid("PCB_noHole-PCB_Box_CutOut1", PCB_noHole, PCB_Box_CutOut,NULL, G4ThreeVector(PCB_Box_x/2,PCB_Box_y/2,0));
  G4VSolid* PCB_CutOut2 = new G4SubtractionSolid("PCB_CutOut1-PCB_Box_CutOut2", PCB_CutOut1, PCB_Box_CutOut,NULL, G4ThreeVector(-PCB_Box_x/2,PCB_Box_y/2,0));

  G4LogicalVolume* PCB_StripLog = new G4LogicalVolume(PCB_CutOut2, FR4_, "PCB_Strip");

  G4VisAttributes* VisAtt_PCB_Strip = new G4VisAttributes(true, G4Color(0.66, 0.7, 1.0, 0.7));
  VisAtt_PCB_Strip->SetForceSolid(true);
  PCB_StripLog->SetVisAttributes(VisAtt_PCB_Strip);

  return PCB_StripLog;
}
