#include "detTPX3.hh"

//material
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"

//geometry
#include "G4Box.hh"
#include "G4PVPlacement.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"


detTPX3::detTPX3() {

  PixelSensorLog_ = NULL;
  PixelASICLog_	  = NULL;

  fPANGlobalConfig = PANGlobalConfig::Instance();
  fPANSDConfig     = PANSDConfig::Instance();

  fNoPixSensorPlacements = 0;
  fNoPixAsicPlacements   = 0;

  DefineMaterials_();

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

detTPX3::~detTPX3() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void detTPX3::DefineMaterials_() {
    //-----------------------------------------------------------------------------------------------------------
    //--------Define Material
    //-----------------------------------------------------------------------------------------------------------
    G4_Si_ = NULL;
    SiO2_  = NULL;
    TPx3_FR4_ = NULL;

    G4NistManager* man = G4NistManager::Instance();

    if(fPANGlobalConfig->IsInSpace) {AmbientEnv_ = man->FindOrBuildMaterial("G4_Galactic");}  // for space
    else {AmbientEnv_ = man->FindOrBuildMaterial("G4_AIR");}                                    // for ground

    //elements
    G4Element* elC  = man->FindOrBuildElement("C");
    G4Element* elH  = man->FindOrBuildElement("H");
    G4Element* elO  = man->FindOrBuildElement("O");
    G4Element* elSi = man->FindOrBuildElement("Si");

    //G4_Si
    G4_Si_ = man->FindOrBuildMaterial("G4_Si");

    //TPx3 FR4
    // SiO2 (from http://www.physi.uni-heidelberg.de/~adler/TRD/TRDunterlagen/RadiatonLength/tgc2.html)
    SiO2_ = new G4Material("Pixel_quartz", 2.200*g/cm3, 2);
    SiO2_->AddElement(elSi, 1);
    SiO2_->AddElement(elO , 2);

    //Epoxy (for FR4)
    G4Material* TPx3_epoxy_ = new G4Material("TPx3_Epoxy" , 1.2*g/cm3, 2);
    TPx3_epoxy_->AddElement(elH, 2);
    TPx3_epoxy_->AddElement(elC, 2);

    TPx3_FR4_ = new G4Material("TPx3_FR4"  , 1.86*g/cm3, 2);
    TPx3_FR4_->AddMaterial(SiO2_,  0.528);
    TPx3_FR4_->AddMaterial(TPx3_epoxy_, 0.472);

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4LogicalVolume* detTPX3::ConstructTimePix3_(){

  //Box
  G4Box* TimePix3Box = new G4Box("TimePix3Box", Pixel_PCB_Width/2., Pixel_PCB_Width/2., (Pixel_PCB_Thickness+Pixel_asic_thickness+Pixel_detector_thickness+2*bump_bond_height+2*Pixel_epsilon)/2.);
  G4LogicalVolume* TimePix3Log_ = new G4LogicalVolume(TimePix3Box, AmbientEnv_, "TimePix3Log_");
  G4VisAttributes* VisAtt_TimePix3Log = new G4VisAttributes(true, G4Colour(0.7, 0.0, .5, 1.0));
  VisAtt_TimePix3Log->SetForceWireframe(!fPANGlobalConfig->visual_light || true);
  VisAtt_TimePix3Log->SetVisibility(!fPANGlobalConfig->visual_light || true);
  TimePix3Log_->SetVisAttributes(VisAtt_TimePix3Log);

  //PCB
  G4Box* PixelPCBBox = new G4Box("PixelPCBBox", Pixel_PCB_Width/2., Pixel_PCB_Width/2., Pixel_PCB_Thickness/2.);
  PixelPCBLog_ = new G4LogicalVolume(PixelPCBBox, TPx3_FR4_, "PixelPCBLog");
  G4VisAttributes* VisAtt_PixelPCBLog = new G4VisAttributes(true, G4Color(0.0, 0.7, 0.0));
  PixelPCBLog_->SetVisAttributes(VisAtt_PixelPCBLog);

  //ASIC
  G4Box* PixelASICBox = new G4Box("PixelASICBox", Pixel_detector_width/2., Pixel_detector_width/2., Pixel_asic_thickness/2.);
  PixelASICLog_ = new G4LogicalVolume(PixelASICBox, G4_Si_, "PixelASICLog");
  G4VisAttributes* VisAtt_PixelASICLog = new G4VisAttributes(true, G4Color(0.7, 0.7, 0.7));
  PixelASICLog_->SetVisAttributes(VisAtt_PixelASICLog);

  //Sensor
  G4Box* PixelSensorBox = new G4Box("PixelSensorBox", Pixel_detector_width/2., Pixel_detector_width/2., Pixel_detector_thickness/2.);
  PixelSensorLog_ = new G4LogicalVolume(PixelSensorBox, G4_Si_, "PixelSensorLog");
  G4VisAttributes* VisAtt_PixelSensorLog = new G4VisAttributes(true, G4Color(0.0, 0.0, 0.8));
  PixelSensorLog_->SetVisAttributes(VisAtt_PixelSensorLog);

  //
  G4double pixel_pcb_z    = -(Pixel_PCB_Thickness+Pixel_asic_thickness+Pixel_detector_thickness)/2.+(Pixel_detector_thickness + bump_bond_height + Pixel_asic_thickness + Pixel_epsilon + Pixel_PCB_Thickness / 2);
  G4double pixel_asic_z   = -(Pixel_PCB_Thickness+Pixel_asic_thickness+Pixel_detector_thickness)/2.+(Pixel_detector_thickness + bump_bond_height + Pixel_asic_thickness/2 + Pixel_epsilon );
  G4double pixel_sensor_z = -(Pixel_PCB_Thickness+Pixel_asic_thickness+Pixel_detector_thickness)/2.+(Pixel_detector_thickness/2);

  new G4PVPlacement(NULL, G4ThreeVector(0, 0, pixel_pcb_z), PixelPCBLog_, "TPx3_PCB", TimePix3Log_, false, 0);

  new G4PVPlacement(NULL, G4ThreeVector(Pixel_detector_width/2. + 50.0*um, Pixel_detector_width/2. + 50.0*um, pixel_asic_z), PixelASICLog_, "Pixel_ASIC1", TimePix3Log_, false, 0);
  new G4PVPlacement(NULL, G4ThreeVector(Pixel_detector_width/2. + 50.0*um, Pixel_detector_width/2. + 50.0*um, pixel_sensor_z), PixelSensorLog_, "Pixel_Sensor1", TimePix3Log_, false, 0);
        
  new G4PVPlacement(NULL, G4ThreeVector(Pixel_detector_width/2. + 50.0*um, - Pixel_detector_width/2. - 50.0*um, pixel_asic_z), PixelASICLog_, "Pixel_ASIC2", TimePix3Log_, false, 0);
  new G4PVPlacement(NULL, G4ThreeVector(Pixel_detector_width/2. + 50.0*um, - Pixel_detector_width/2. - 50.0*um, pixel_sensor_z), PixelSensorLog_, "Pixel_Sensor2", TimePix3Log_, false, 0);
        
  new G4PVPlacement(NULL, G4ThreeVector(- Pixel_detector_width/2. - 50.0*um, Pixel_detector_width/2. + 50.0*um, pixel_asic_z), PixelASICLog_, "Pixel_ASIC3", TimePix3Log_, false, 0);
  new G4PVPlacement(NULL, G4ThreeVector(- Pixel_detector_width/2. - 50.0*um, Pixel_detector_width/2. + 50.0*um, pixel_sensor_z), PixelSensorLog_, "Pixel_Sensor3", TimePix3Log_, false, 0);
        
  new G4PVPlacement(NULL, G4ThreeVector(- Pixel_detector_width/2. - 50.0*um, - Pixel_detector_width/2. - 50.0*um, pixel_asic_z), PixelASICLog_, "Pixel_ASIC4", TimePix3Log_, false, 0);
  new G4PVPlacement(NULL, G4ThreeVector(- Pixel_detector_width/2. - 50.0*um, - Pixel_detector_width/2. - 50.0*um, pixel_sensor_z), PixelSensorLog_, "Pixel_Sensor4", TimePix3Log_, false, 0);

  fNoPixSensorPlacements = 4;
  fNoPixAsicPlacements   = 4;

  return TimePix3Log_;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANSensitiveDetector* detTPX3::AddSD(G4LogicalVolume* detSD, std::map<G4LogicalVolume*,G4String>& instrumentSD){
	G4String strSD     = "PANSD_" + detSD->GetName();
	G4String strHitCol = "PANHitsCollection_" + detSD->GetName();

	instrumentSD[detSD] = strHitCol;

	PANSensitiveDetector* PAN_SD = new PANSensitiveDetector(strSD,strHitCol);
  G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);

  return PAN_SD;
}
