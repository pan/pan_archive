#include "detTOF.hh"

//material
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"

//geometry
#include "G4Box.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"


detTOF::detTOF() {

  TOF_ScintLog_ = NULL;
  TOF_SiPM_Log_ = NULL;

  fPANGlobalConfig = PANGlobalConfig::Instance();
  fPANSDConfig     = PANSDConfig::Instance();

  DefineMaterials_();

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

detTOF::~detTOF() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void detTOF::DefineMaterials_() {
  //-----------------------------------------------------------------------------------------------------------
  //--------Define Material
  //-----------------------------------------------------------------------------------------------------------
  AmbientEnv_ = NULL;
  FR4_        = NULL;
  G4_Si_      = NULL;
  SiliconResin = NULL;
  vikuiti      = NULL;
  EJ_230_      = NULL;

  G4NistManager* man = G4NistManager::Instance();

  G4double density;                         // matter density
  G4double fractionmass;                    // compound proportion
  G4String name;                            // material name
  G4int ncomponents;                        // component number
  G4int natoms;                             // atom proportion

  if(fPANGlobalConfig->IsInSpace) {AmbientEnv_ = man->FindOrBuildMaterial("G4_Galactic");}  // for space
  else {AmbientEnv_ = man->FindOrBuildMaterial("G4_AIR");}    

  //elements
  G4Element* elAl = man->FindOrBuildElement("Al");
  G4Element* elB  = man->FindOrBuildElement("B");
  G4Element* elC  = man->FindOrBuildElement("C");
  G4Element* elH  = man->FindOrBuildElement("H");
  G4Element* elK  = man->FindOrBuildElement("K");
  G4Element* elN  = man->FindOrBuildElement("N");
  G4Element* elNa = man->FindOrBuildElement("Na");
  G4Element* elO  = man->FindOrBuildElement("O");
  G4Element* elSi = man->FindOrBuildElement("Si");

  //G4_Si
  G4_Si_ = man->FindOrBuildMaterial("G4_Si");


  //Glass (the window of the PM)
  SiliconResin = new G4Material("SiliconResin", 1.03*g/cm3, ncomponents=6);
  SiliconResin->AddElement(elB, fractionmass=0.040064);
  SiliconResin->AddElement(elO, fractionmass=0.539562);
  SiliconResin->AddElement(elNa,fractionmass=0.028191);/* Borosilicate glass http://en.wikipedia.org/wiki/Borosilicate_glass */
  SiliconResin->AddElement(elAl,fractionmass=0.011644);
  SiliconResin->AddElement(elSi,fractionmass=0.377220);
  SiliconResin->AddElement(elK, fractionmass=0.003321);

  //FR4 for PCB
  //Silicon Oxide
  G4Material* SiliconOxide = new G4Material("SiliconOxide", density = 2.65*g/cm3, ncomponents = 2);
  SiliconOxide->AddElement(elSi, natoms=1);
  SiliconOxide->AddElement(elO,  natoms=2);
  //Diglycidyl Ether of Bisphenol A (First compound of epoxy resin Epotek 301-1)
  G4Material* Epoxy_1 = new G4Material("Epoxy_1", density = 1.16*g/cm3, ncomponents = 3);
  Epoxy_1->AddElement(elC, natoms=19);
  Epoxy_1->AddElement(elH, natoms=20);
  Epoxy_1->AddElement(elO, natoms=4);
  //1,4-Butanediol Diglycidyl Ether (Second compound of epoxy resin Epotek 301-1)
  G4Material* Epoxy_2 = new G4Material("Epoxy_2", density = 1.10*g/cm3, ncomponents = 3);
  Epoxy_2->AddElement(elC, natoms=10);
  Epoxy_2->AddElement(elH, natoms=18);
  Epoxy_2->AddElement(elO, natoms=4);
  //1,6-Hexanediamine 2,2,4-trimetyl (Third compound of epoxy resin Epotek 301-1)
  G4Material* Epoxy_3 = new G4Material("Epoxy_3", density = 1.16*g/cm3, ncomponents = 3);
  Epoxy_3->AddElement(elC, natoms=9);
  Epoxy_3->AddElement(elH, natoms=22);
  Epoxy_3->AddElement(elN, natoms=2);
  //Epoxy resin Epotek 301-1
  G4Material* Epoxy_Resin = new G4Material("Epoxy_Resin", density = 1.21*g/cm3, ncomponents = 3);
  Epoxy_Resin->AddMaterial(Epoxy_1, fractionmass=56*perCent);
  Epoxy_Resin->AddMaterial(Epoxy_2, fractionmass=24*perCent);
  Epoxy_Resin->AddMaterial(Epoxy_3, fractionmass=20*perCent);
  //FR4 PCB material
  FR4_ = new G4Material("FR4", density = 1.8*g/cm3, ncomponents=2);
  FR4_->AddMaterial(SiliconOxide, fractionmass=60*perCent);
  FR4_->AddMaterial(Epoxy_Resin,  fractionmass=40*perCent);

  //Vikuiti
  vikuiti = new G4Material(name = "vikuiti", density=1.32*g/cm3, ncomponents = 3);
  vikuiti->AddElement(elC, natoms = 20);
  vikuiti->AddElement(elH, natoms = 12);
  vikuiti->AddElement(elO, natoms = 3);
  
  // EJ_230_
  EJ_230_ = new G4Material(name = "EJ_230", density = 1.023*g/cm3, ncomponents = 2);
  EJ_230_->AddElement(elH, fractionmass = 0.08400);  // 5.15E22 atoms per cm3
  EJ_230_->AddElement(elC, fractionmass = 0.91600);  // 4.68E22 atoms per cm3
  EJ_230_->GetIonisation()->SetBirksConstant(fPANGlobalConfig->birks_constant*mm/MeV);

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4LogicalVolume* detTOF::ConstructTOF(){

  //Box
  G4Box* TOFBox = new G4Box("DetectorBox",TOF_box_xy/2,TOF_box_xy/2,TOF_box_z/2 );
  G4LogicalVolume* TOFLog_ = new G4LogicalVolume(TOFBox, AmbientEnv_, "TOFLog_");
  G4VisAttributes* VisAtt_TOFLog = new G4VisAttributes(true, G4Colour(0.7, 0.0, .5, 1.0));
  VisAtt_TOFLog->SetForceWireframe(!fPANGlobalConfig->visual_light);
  VisAtt_TOFLog->SetVisibility(!fPANGlobalConfig->visual_light);
  TOFLog_->SetVisAttributes(VisAtt_TOFLog);

  //Vikuiti
  G4Box * VikuitiBox = new G4Box("VikuitiBox", Scin_xy/2, Scin_xy/2, VikuitiThickness/2);
  G4LogicalVolume* VikuitiLog_ = new G4LogicalVolume(VikuitiBox, vikuiti, "VikuitiLog");
  G4VisAttributes* VisAtt_VikuitiLog = new G4VisAttributes(true, G4Color(0.66, 0.0, 1.0, 0.2));
  VisAtt_VikuitiLog->SetForceSolid(true);
  VikuitiLog_->SetVisAttributes(VisAtt_VikuitiLog);

  //AirGapBox
  G4Box * AirGapBox = new G4Box("AirGapBox", Scin_xy/2, Scin_xy/2, AirGap/2);
  G4LogicalVolume* AirGapLog_ = new G4LogicalVolume(AirGapBox, AmbientEnv_, "AirGapLog");
  AirGapLog_->SetVisAttributes(VisAtt_VikuitiLog);
       
  //Scintillator
  G4Box * ScinBox = new G4Box("ScinBox", Scin_xy/2, Scin_xy/2, Scin_thickness/2);
  TOF_ScintLog_ = new G4LogicalVolume(ScinBox, EJ_230_, "TOF_ScintLog");
  G4VisAttributes * VisAtt_ScintLog = new G4VisAttributes(true, G4Color(0.3, 0.3, 0.3, 0.9));
  TOF_ScintLog_->SetVisAttributes(VisAtt_ScintLog);

  //SiPM
  G4Box* SiPM_Box = new G4Box("SiPM_Box", SiPM_xy/2 , SiPM_xy/2 , SiPM_thickness/2.);
  G4LogicalVolume* SiPMLog_ = new G4LogicalVolume(SiPM_Box, AmbientEnv_, "SiPMLog_");

  G4Box* SiPM_channel_Box   = new G4Box("SiPM_pixel", SiPM_xy/2., SiPM_xy/2., MPPC_height/2.);              //Silicon
  TOF_SiPM_Log_ = new G4LogicalVolume(SiPM_channel_Box, G4_Si_, "TOF_SiPM_Log");

  G4Box* SiliconResin_mppc  = new G4Box("SiliconResin", SiPM_xy/2., SiPM_xy/2., SiliconResin_height/2.);    //glass layer
  G4LogicalVolume* SiliconResin_log = new G4LogicalVolume(SiliconResin_mppc, SiliconResin, "SiliconResin",0,0,0);

  G4Box* SiPM_PCB_Box       = new G4Box("SiPM_PCB",SiPM_xy/2., SiPM_xy/2.,(SiPM_thickness-SiliconResin_height-MPPC_height)/2);
  G4LogicalVolume* SiPM_PCB_log = new G4LogicalVolume(SiPM_PCB_Box, FR4_, "SiPM_PCB_Log",0,0,0);

  //placement inside SiPMLog_
  new G4PVPlacement(NULL, G4ThreeVector(0,0,SiPM_thickness/2.-SiliconResin_height/2.), SiliconResin_log, "SiliconResin", SiPMLog_, false, 0);
  new G4PVPlacement(NULL, G4ThreeVector(0,0,SiPM_thickness/2.-SiliconResin_height-MPPC_height/2.), TOF_SiPM_Log_, "SiliconResin", SiPMLog_, false, 0);
  new G4PVPlacement(NULL, G4ThreeVector(0,0,-SiliconResin_height/2.-MPPC_height/2.), SiPM_PCB_log, "SiliconResin", SiPMLog_, false, 0);

  //placement inside TOFLog_
  new G4PVPlacement(NULL, G4ThreeVector(0,0,0), TOF_ScintLog_, "Scintillator", TOFLog_, false, 0);

  for(int iLayer=0;iLayer<2;iLayer++){
    new G4PVPlacement(NULL, G4ThreeVector(0, 0, (iLayer == 0 ? 1 : -1 )*(Scin_thickness/2+AirGap/2)), AirGapLog_, "AirGap", TOFLog_, false, iLayer);
    new G4PVPlacement(NULL, G4ThreeVector(0, 0, (iLayer == 0 ? 1 : -1 )*(Scin_thickness/2+AirGap+VikuitiThickness/2)), VikuitiLog_, "Vikuiti", TOFLog_, false, iLayer);
  }

  G4RotationMatrix * rotax = new G4RotationMatrix();
  rotax->rotateX(90*deg);
  G4RotationMatrix * rotay = new G4RotationMatrix();
  rotay->rotateY(90*deg);

  for(int iSiPM=0;iSiPM<TOF_nSiPM_arrays;iSiPM++){
    new G4PVPlacement(rotax, G4ThreeVector(TOF_SiPM_pos[iSiPM], -TOF_SiPM_offset, 0), SiPMLog_, "TOF_SiPM", TOFLog_, false, iSiPM);
    new G4PVPlacement(rotay, G4ThreeVector(TOF_SiPM_offset, TOF_SiPM_pos[iSiPM], 0), SiPMLog_, "TOF_SiPM", TOFLog_, false, iSiPM + 1*TOF_nSiPM_arrays);
    new G4PVPlacement(rotax, G4ThreeVector(-TOF_SiPM_pos[iSiPM], TOF_SiPM_offset, 0), SiPMLog_, "TOF_SiPM", TOFLog_, false, iSiPM + 2*TOF_nSiPM_arrays);
    new G4PVPlacement(rotay, G4ThreeVector(-TOF_SiPM_offset, -TOF_SiPM_pos[iSiPM], 0), SiPMLog_, "TOF_SiPM", TOFLog_, false, iSiPM + 3*TOF_nSiPM_arrays);
  }
  
  return TOFLog_;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANSensitiveDetector* detTOF::AddSD(G4LogicalVolume* detSD, std::map<G4LogicalVolume*,G4String>& instrumentSD){
  G4String strSD     = "PANSD_" + detSD->GetName();
  G4String strHitCol = "PANHitsCollection_" + detSD->GetName();

  instrumentSD[detSD] = strHitCol;

  PANSensitiveDetector* PAN_SD = new PANSensitiveDetector(strSD,strHitCol);
  G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);

  return PAN_SD;
}