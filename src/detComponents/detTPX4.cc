#include "detTPX4.hh"

//material
#include "G4Material.hh"
#include "G4NistManager.hh"

//geometry
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

detTPX4::detTPX4(){

	PixelSensorLog_		= NULL;
	PixelASICLog_		  = NULL;

  fPANGlobalConfig = PANGlobalConfig::Instance();
  fPANSDConfig     = PANSDConfig::Instance();

  DefineMaterials_();

  //defaults
  SetSensorThickness(300*um);
  SetASICThickness(100*um);
  SetSensorConfig(0);
  SetSensorMaterial(G4_Si_);
  SetSD_prefix("");

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

detTPX4::~detTPX4() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void detTPX4::DefineMaterials_() {
    //-----------------------------------------------------------------------------------------------------------
    //--------Define Material
    //-----------------------------------------------------------------------------------------------------------

    G4_Si_ = NULL;

    G4NistManager* man = G4NistManager::Instance();

    if(fPANGlobalConfig->IsInSpace) {AmbientEnv_ = man->FindOrBuildMaterial("G4_Galactic");}  // for space
    else {AmbientEnv_ = man->FindOrBuildMaterial("G4_AIR");}                                    // for ground

    //G4_Si
    G4_Si_ = man->FindOrBuildMaterial("G4_Si");

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4LogicalVolume* detTPX4::ConstructTimePix4_(bool IsQuad){

  G4Box* TimePix4Box = new G4Box("PixelSensorBox", (IsQuad?1:0.5) * ASIC_outer_Ring_x/2., (IsQuad?1:0.5) * ASIC_outer_Ring_y/2., (Pix_z+PixASIC_z)/2.);
  G4LogicalVolume* TimePix4Log_ = new G4LogicalVolume(TimePix4Box, AmbientEnv_, "TimePix4Log_");
  G4VisAttributes* VisAtt_TimePix4Log = new G4VisAttributes(true, G4Colour(0.7, 0.0, .5, 1.0));
  VisAtt_TimePix4Log->SetForceWireframe(!fPANGlobalConfig->visual_light);
  VisAtt_TimePix4Log->SetVisibility(!fPANGlobalConfig->visual_light);
  TimePix4Log_->SetVisAttributes(VisAtt_TimePix4Log);

  //generate quad
  G4double TimePix_x = (IsQuad?2:1) * (sensorConfig==0 ? nPix_x*4. : nPix_x) * (sensorConfig==0 ? Pix_x/4. : Pix_x) + (IsQuad?1:0) * TimePix4_gap;
  G4double TimePix_y = (IsQuad?2:1) * (sensorConfig==0 ? nPix_y/32. : nPix_y) * (sensorConfig==0 ? Pix_y*32. : Pix_y) + (IsQuad?1:0) * TimePix4_gap;

  if(!fPANGlobalConfig->visual_light){
    //create single pixel     
    G4Box* PixelSensorBox = new G4Box("PixelSensorBox", (sensorConfig==0 ? Pix_x/4. : Pix_x)/2., (sensorConfig==0 ? Pix_y*32. : Pix_y)/2., Pix_z/2.);
    PixelSensorLog_ = new G4LogicalVolume(PixelSensorBox, SensorMat_, "PixelSensorLog_");
    G4VisAttributes* VisAtt_PixelSensorLog = new G4VisAttributes(true, G4Color(0.0, 0.0, 0.8));
    PixelSensorLog_->SetVisAttributes(VisAtt_PixelSensorLog);

    //create ASIC
    G4Box* OuterASICBox1 = new G4Box("OuterASICBox1", (IsQuad?1:0.5) * ASIC_outer_Ring_x/2., (IsQuad?1:0.5) * ASIC_outer_Ring_y/2., PixASIC_z/2.);
    G4Box* OuterASICBox2 = new G4Box("OuterASICBox2", (IsQuad?1:0.5) * (ASIC_outer_Ring_x/2.-ASIC_outer_Ring_width), (IsQuad?1:0.5) * (ASIC_outer_Ring_y/2.-ASIC_outer_Ring_width), PixASIC_z);
    G4VSolid* OuterASICRing = new G4SubtractionSolid("OuterASICBox1-OuterASICBox2", OuterASICBox1, OuterASICBox2,NULL, G4ThreeVector(0,0,0));
   
    G4Box* InnerASICBox1 = new G4Box("InnerASICBox1", (IsQuad?1:0.5) * ASIC_inner_Ring_x/2., (IsQuad?1:0.5) * ASIC_inner_Ring_y/2., PixASIC_z/2.);
    G4Box* InnerASICBox2 = new G4Box("InnerASICBox2", (IsQuad?1:0.5) * (ASIC_inner_Ring_x/2.-ASIC_inner_Ring_width), (IsQuad?1:0.5) * (ASIC_inner_Ring_y/2.-ASIC_inner_Ring_width), PixASIC_z);
    G4VSolid* InnerASICRing = new G4SubtractionSolid("InnerASICBox1-InnerASICBox2", InnerASICBox1, InnerASICBox2,NULL, G4ThreeVector(0,0,0));

    G4VSolid* UnionASIC = new G4UnionSolid("OuterASIC+InnerASIC",OuterASICRing,InnerASICRing,NULL,G4ThreeVector(0,0,0));

    PixelASICLog_ = new G4LogicalVolume(UnionASIC, G4_Si_, "PixelASICLog_");
    G4VisAttributes* VisAtt_PixelASICLog = new G4VisAttributes(true, G4Color(0.7, 0.7, 0.7));
    PixelASICLog_->SetVisAttributes(VisAtt_PixelASICLog);

    G4VSolid* ASICbond = new G4Tubs("ASICbond", 0,bond_ring_radius,PixASIC_z/2., 0,2.* M_PI);
    G4LogicalVolume* ASICbondLog_ = new G4LogicalVolume(ASICbond,G4_Si_,"ASICbondLog_");

    new G4PVPlacement(NULL, G4ThreeVector(0,0, -(Pix_z+PixASIC_z)/2. + PixASIC_z/2), PixelASICLog_, "PixelAsic", TimePix4Log_, false, 0);

    //placement of each pixel
    int PixID = 0;
    for(G4int xPix=0; xPix<(IsQuad?2:1)*(sensorConfig==0 ? nPix_x*4. : nPix_x); xPix++){
      for(G4int yPix=0; yPix<(IsQuad?2:1)*(sensorConfig==0 ? nPix_y/32. : nPix_y); yPix++){
        new G4PVPlacement(NULL, G4ThreeVector((sensorConfig==0 ? Pix_x/4. : Pix_x)/2+xPix*(sensorConfig==0 ? Pix_x/4. : Pix_x) - TimePix_x/2 + (xPix < (sensorConfig==0 ? nPix_x*4. : nPix_x) ? 0 : TimePix4_gap), (sensorConfig==0 ? Pix_y*32. : Pix_y)/2+yPix*(sensorConfig==0 ? Pix_y*32. : Pix_y) - TimePix_y/2 + (yPix < (sensorConfig==0 ? nPix_y/32. : nPix_y) ? 0 : TimePix4_gap), -(Pix_z+PixASIC_z)/2. + PixASIC_z +Pix_z/2), PixelSensorLog_, "Pixel", TimePix4Log_, false, PixID);
        PixID++;
        fNoPixSensorPlacements++;
      }
    }

    //placement of each ASIC bond
    int AsicID = 0;
    for(G4int xAsic=0; xAsic<(IsQuad?2:1)*nAsic_x; xAsic++){
      for(G4int yAsic=0; yAsic<(IsQuad?2:1)*nAsic_y; yAsic++){
        new G4PVPlacement(NULL, G4ThreeVector(Asic_x/2 + xAsic*Asic_x - TimePix_x/2 + (xAsic < nAsic_x ? 0 : TimePix4_gap), Asic_y/2 + yAsic*Asic_y - TimePix_y/2 + (yAsic < nAsic_y ? 0 : TimePix4_gap), -(Pix_z+PixASIC_z)/2. + PixASIC_z/2), ASICbondLog_, "AsicBond", TimePix4Log_, false, AsicID);
        AsicID++;
        fNoPixAsicPlacements++;
      }
    }

  } else{

    //'pixel' sheet
    G4Box* PixelSensorBox = new G4Box("PixelSensorBox", TimePix_x/2., TimePix_y/2., Pix_z/2.);
    PixelSensorLog_ = new G4LogicalVolume(PixelSensorBox, SensorMat_, "PixelSensorLog_");
    G4VisAttributes* VisAtt_PixelSensorLog = new G4VisAttributes(true, G4Color(0.0, 0.0, 0.8));
    PixelSensorLog_->SetVisAttributes(VisAtt_PixelSensorLog);

    new G4PVPlacement(NULL, G4ThreeVector(0,0,PixASIC_z/2), PixelSensorLog_, "TimePixLight", TimePix4Log_, false, 0);
    fNoPixSensorPlacements++;

    //Pixel ASIC
    G4Box* PixelASICBox = new G4Box("PixelASICBox", TimePix_x/2., TimePix_y/2., PixASIC_z/2.);
    PixelASICLog_ = new G4LogicalVolume(PixelASICBox, G4_Si_, "PixelASICLog_");
    G4VisAttributes* VisAtt_PixelASICLog = new G4VisAttributes(true, G4Color(0.7, 0.7, 0.7));
    PixelASICLog_->SetVisAttributes(VisAtt_PixelASICLog);

    new G4PVPlacement(NULL, G4ThreeVector(0, 0, -Pix_z/2), PixelASICLog_, "PixelAsic", TimePix4Log_, false, 0);
    fNoPixAsicPlacements++;

  }

  return TimePix4Log_;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANSensitiveDetector* detTPX4::AddSD(G4LogicalVolume* detSD, std::map<G4LogicalVolume*,G4String>& instrumentSD){
	G4String strSD     = SD_prefix + "PANSD_" + detSD->GetName();
	G4String strHitCol = SD_prefix + "PANHitsCollection_" + detSD->GetName();

	instrumentSD[detSD] = strHitCol;

	PANSensitiveDetector* PAN_SD = new PANSensitiveDetector(strSD,strHitCol);
  G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);

  return PAN_SD;
}