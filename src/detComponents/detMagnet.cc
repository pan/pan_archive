#include "detMagnet.hh"

//material
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"

//geometry
#include "G4Tubs.hh"
#include "G4PVReplica.hh"
#include "G4PVPlacement.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

detMagnet::detMagnet() {

  divMagLog_ = NULL;
  AlCaseLog_ = NULL;

  fPANGlobalConfig = PANGlobalConfig::Instance();
  fPANSDConfig     = PANSDConfig::Instance();

  fNoMagnetPlacements = 0;

  DefineMaterials_();
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

detMagnet::~detMagnet() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void detMagnet::DefineMaterials_() {
    //-----------------------------------------------------------------------------------------------------------
    //--------Define Material
    //-----------------------------------------------------------------------------------------------------------
    AmbientEnv_ = NULL;
    Nd2Fe14B_   = NULL;
    G4_Al_      = NULL;

    G4NistManager* man = G4NistManager::Instance();

    if(fPANGlobalConfig->IsInSpace) {AmbientEnv_ = man->FindOrBuildMaterial("G4_Galactic");}  // for space
    else {AmbientEnv_ = man->FindOrBuildMaterial("G4_AIR");}                                    // for ground

    //elements
    G4Element* elB  = man->FindOrBuildElement("B");
    G4Element* elFe = man->FindOrBuildElement("Fe");
    G4Element* elNd = man->FindOrBuildElement("Nd");

    //NdFeB_
    Nd2Fe14B_ = new G4Material("Neodymium", 7.5*g/cm3, 3);
    Nd2Fe14B_->AddElement(elNd,2);
    Nd2Fe14B_->AddElement(elFe,14);
    Nd2Fe14B_->AddElement(elB,1);

    // G4_Al
    G4_Al_ = man->FindOrBuildMaterial("G4_Al");

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

std::pair<G4LogicalVolume*,G4LogicalVolume*> detMagnet::ConstructMagnet_() {

    //logic
    G4VSolid* MagTube = new G4Tubs("magnet_tube",mag_inR,mag_outR,magnet_length/2.,0.,2.* M_PI * rad);
    G4LogicalVolume* MagnetLog_ = new G4LogicalVolume(MagTube, AmbientEnv_, "MagnetLog");
    G4VisAttributes* VisAtt_MagnetLog = new G4VisAttributes(true, G4Colour(1.0, 0.0, 1.0, 1.0));
    VisAtt_MagnetLog->SetForceWireframe(!fPANGlobalConfig->visual_light);
    VisAtt_MagnetLog->SetVisibility(!fPANGlobalConfig->visual_light);
    MagnetLog_->SetVisAttributes(VisAtt_MagnetLog);

    //magnets
    G4double dPhi_comp = 2.* M_PI * rad / nMagComponents;
    G4VSolid* div_mag = new G4Tubs("div_tube", mag_inR,mag_outR,magnet_length/2., -dPhi_comp/2,dPhi_comp);
    divMagLog_ = new G4LogicalVolume(div_mag,Nd2Fe14B_,"divMagLog_");
    G4VPhysicalVolume* div_tube_phys  = new G4PVReplica("div_tube_phys", divMagLog_, MagnetLog_, kPhi, nMagComponents, dPhi_comp); 

    G4VisAttributes* VisAtt_MagCompLog = new G4VisAttributes(true, G4Color(0.4, 0.8, 0.5, 1.0));
    divMagLog_->SetVisAttributes(VisAtt_MagCompLog);

    fNoMagnetPlacements = nMagComponents;

    std::pair<G4LogicalVolume*,G4LogicalVolume*> volPair;
    volPair.first  = MagnetLog_; //mother
    volPair.second = divMagLog_; //daughter
    
    return volPair;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

//Alu casing
G4LogicalVolume* detMagnet::ConstructAluCase_() {

    G4VSolid* AlCase = new G4Tubs("magnet_tube",Al_case_inR,Al_case_outR,magnet_length/2.,0.,2.* M_PI * rad);
    AlCaseLog_ = new G4LogicalVolume(AlCase, G4_Al_, "AlCaseLog_");
    G4VisAttributes* VisAtt_AlCaseLog = new G4VisAttributes(true, G4Color(0.5, 0.5, 0.5));
    AlCaseLog_->SetVisAttributes(VisAtt_AlCaseLog);

    return AlCaseLog_;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANSensitiveDetector* detMagnet::AddSD(G4LogicalVolume* detSD, std::map<G4LogicalVolume*,G4String>& instrumentSD){
	G4String strSD     = "PANSD_" + detSD->GetName();
	G4String strHitCol = "PANHitsCollection_" + detSD->GetName();

	instrumentSD[detSD] = strHitCol;

	PANSensitiveDetector* PAN_SD = new PANSensitiveDetector(strSD,strHitCol);
  G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);

  return PAN_SD;
}
