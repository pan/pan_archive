#include "detStripXY.hh"

//material
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"

//geometry
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4PVPlacement.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

detStripXY::detStripXY() {


  fPANGlobalConfig = PANGlobalConfig::Instance();
  fPANSDConfig     = PANSDConfig::Instance();

  SiStripXLog_ = NULL;
  if(fPANGlobalConfig->visual_light){SiStripYLog_.assign(1,NULL);}
  else{SiStripYLog_.assign(SiStripY_nStrips,NULL);}

  fNoStripXPlacements    = 0;
  fNoStripYPlacements    = 0;

  DefineMaterials_();

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

detStripXY::~detStripXY() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void detStripXY::DefineMaterials_() {
  //-----------------------------------------------------------------------------------------------------------
  //--------Define Material
  //-----------------------------------------------------------------------------------------------------------

  AmbientEnv_ = NULL;
  G4_Si_      = NULL;

  G4NistManager* man = G4NistManager::Instance();

  if(fPANGlobalConfig->IsInSpace) {AmbientEnv_ = man->FindOrBuildMaterial("G4_Galactic");}  // for space
  else {AmbientEnv_ = man->FindOrBuildMaterial("G4_AIR");}    

  //G4_Si
  G4_Si_ = man->FindOrBuildMaterial("G4_Si");
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4LogicalVolume* detStripXY::ConstructStripX(){

  //Detector Volume
  G4Box* StripXBox = new G4Box("DetectorBox",(SiStripX_nStrips*SiStripX_pitch)/2., SiStripX_length/2., SiStrip_thickness/2.);
  G4LogicalVolume* DetectorLog_ = new G4LogicalVolume(StripXBox, AmbientEnv_, "StripXBoxLog_");
  G4VisAttributes* VisAtt_StripXLog = new G4VisAttributes(true, G4Colour(0.2, 0.2, 1., 0.5));
  VisAtt_StripXLog->SetForceWireframe(!fPANGlobalConfig->visual_light);
  VisAtt_StripXLog->SetVisibility(!fPANGlobalConfig->visual_light);
  DetectorLog_->SetVisAttributes(VisAtt_StripXLog);

  G4Box* SiStripXBox;
  if(fPANGlobalConfig->visual_light) {SiStripXBox = new G4Box("SiStripX", (SiStripX_nStrips*SiStripX_pitch)/2., SiStripX_length/2., SiStrip_thickness/2.);}
  else {SiStripXBox = new G4Box("SiStripX", SiStripX_pitch/2., SiStripX_length/2., SiStrip_thickness/2.);}

  SiStripXLog_ = new G4LogicalVolume(SiStripXBox, G4_Si_, "SiStripXLog");

  G4VisAttributes* VisAtt_SiStripXLog = new G4VisAttributes(true, G4Color(0.5, 0.5, 0.5));// Grey
  SiStripXLog_->SetVisAttributes(VisAtt_SiStripXLog);

  if(fPANGlobalConfig->visual_light){
    new G4PVPlacement(NULL, G4ThreeVector(0,0,0), SiStripXLog_, "SiStripX", DetectorLog_, false, 0);
    fNoStripXPlacements++;
  } else {
    for(int iStrip =0;iStrip<SiStripX_nStrips;iStrip++){
      G4double x_pos = -SiStripX_nStrips/2.*SiStripX_pitch + iStrip*SiStripX_pitch + SiStripX_pitch/2;
      new G4PVPlacement(NULL, G4ThreeVector(x_pos,0,0), SiStripXLog_, "SiStripX", DetectorLog_, false, iStrip);
      fNoStripXPlacements++;
    }
  }

  return DetectorLog_;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4LogicalVolume* detStripXY::ConstructStripY(){

  //Detector Volume
  G4Tubs* StripYTub = new G4Tubs("SiStripY",0,SiStripY_length_base/2.,SiStrip_thickness/2.,0.,2.* M_PI * rad);
  G4LogicalVolume* DetectorLog_ = new G4LogicalVolume(StripYTub, AmbientEnv_, "StripYTubLog_");
  G4VisAttributes* VisAtt_StripYLog = new G4VisAttributes(true, G4Colour(0.2, 0.2, 1., 0.5));
  VisAtt_StripYLog->SetForceWireframe(!fPANGlobalConfig->visual_light);
  VisAtt_StripYLog->SetVisibility(!fPANGlobalConfig->visual_light);
  DetectorLog_->SetVisAttributes(VisAtt_StripYLog);

  //Si Strip Y
  G4VisAttributes* VisAtt_SiStripYLog = new G4VisAttributes(true, G4Color(0.5, 0.5, 0.5));// Grey

  if(fPANGlobalConfig->visual_light){
    G4Tubs* SiStripCyl = new G4Tubs("SiStripY",0,SiStripY_length_base/2.,SiStrip_thickness/2.,0.,2.* M_PI * rad);
    SiStripYLog_[0] = new G4LogicalVolume(SiStripCyl, G4_Si_, "SiStripYLog");
    SiStripYLog_[0]->SetVisAttributes(VisAtt_SiStripYLog);
    new G4PVPlacement(NULL, G4ThreeVector(0, 0, 0), SiStripYLog_[0], "SiStripY", DetectorLog_, false, 0);
    fNoStripYPlacements++;
  } else {

    for (G4int istrip=0; istrip<SiStripY_nStrips; istrip++){
      G4double SiStripY_xcur = -(SiStripY_pitch * SiStripY_nStrips/2. - SiStripY_pitch/2 ) + SiStripY_pitch * istrip;
            
      G4double SiStripY_half_length = sqrt(SiStripY_length_base/2. * SiStripY_length_base/2. - SiStripY_xcur * SiStripY_xcur);

      G4Box* SiStripYBox = new G4Box("SiStripY", SiStripY_half_length, SiStripY_pitch/2., SiStrip_thickness/2.);
      SiStripYLog_[istrip] = new G4LogicalVolume(SiStripYBox, G4_Si_, "SiStripYLog");
      SiStripYLog_[istrip]->SetVisAttributes(VisAtt_SiStripYLog);

      new G4PVPlacement(NULL, G4ThreeVector(0, -(SiStripY_pitch * SiStripY_nStrips/2. - SiStripY_pitch/2 ) + SiStripY_pitch * istrip, 0), SiStripYLog_[istrip], "SiStripY", DetectorLog_, false, istrip);
      fNoStripYPlacements++;
    }
  }

  return DetectorLog_;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANSensitiveDetector* detStripXY::AddSD(G4LogicalVolume* detSD, std::map<G4LogicalVolume*,G4String>& instrumentSD){
  G4String strSD     = "PANSD_" + detSD->GetName();
  G4String strHitCol = "PANHitsCollection_" + detSD->GetName();

  instrumentSD[detSD] = strHitCol;

  PANSensitiveDetector* PAN_SD = new PANSensitiveDetector(strSD,strHitCol);
  G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);

  return PAN_SD;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANSensitiveDetector* detStripXY::AddSD(G4LogicalVolume* detSD, std::map<G4LogicalVolume*,G4String>& instrumentSD, int iStrip){
  G4String strSD     = "PANSD_" + std::to_string(iStrip) + "_" + detSD->GetName();
  G4String strHitCol = "PANHitsCollection_" + std::to_string(iStrip) + "_" + detSD->GetName();

  instrumentSD[detSD] = strHitCol;

  PANSensitiveDetector* PAN_SD = new PANSensitiveDetector(strSD,strHitCol);
  G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);

  return PAN_SD;
}