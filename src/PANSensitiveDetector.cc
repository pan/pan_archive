#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4VProcess.hh"
#include "G4StepPoint.hh"
#include "G4SystemOfUnits.hh"
#include "G4Version.hh"
#include "G4IonTable.hh"

#include "PANDetectorConstruction.hh"
#include "PANSensitiveDetector.hh"
#include "PANGlobalConfig.hh"
#include "PANSDConfig.hh"

PANSensitiveDetector::PANSensitiveDetector(G4String SDName, G4String HCName): G4VSensitiveDetector(SDName) {
    collectionName.insert(HCName);
    
    fPANHitsCollection_  = NULL;

    fShieldingVolume   = NULL;
    fAlCaseVolume      = NULL;
    fMagnetVolume      = NULL;
    fBerylliumVolume   = NULL;
    fSiliconBoxVolume  = NULL;

    f_PixelSensorVolume             = NULL;
    f_PixelAsicVolume               = NULL;
    
    fPANSDConfig     = PANSDConfig::Instance();
    fPANGlobalConfig = PANGlobalConfig::Instance();

    fSiStripXVolume    = NULL;
    fSiStripYVolume.clear();
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

PANSensitiveDetector::~PANSensitiveDetector() {
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void PANSensitiveDetector::Initialize(G4HCofThisEvent* hitCollection) {
    // at the start of an event
    fPANHitsCollection_ = new PANHitsCollection(SensitiveDetectorName, collectionName[0]);
    G4int HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    hitCollection->AddHitsCollection(HCID, fPANHitsCollection_);
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory* ) {

    G4LogicalVolume* volume = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetLogicalVolume();

    //assign volume
    if(fPANGlobalConfig->IsMiniPAN){return ProcessMiniPAN_Jun2022(aStep,volume);}

    return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessMiniPAN_Jun2022(G4Step* aStep, G4LogicalVolume* volume){

  const PANDetectorConstruction* detectorConstruction = static_cast<const PANDetectorConstruction*>
                (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

  if(!fSiStripXVolume){fSiStripXVolume = detectorConstruction->GetMiniPAN().GetdetStripXY().GetSiStripXVolume();}
  if(fSiStripYVolume.size() == 0){fSiStripYVolume = detectorConstruction->GetMiniPAN().GetdetStripXY().GetSiStripYVolume();}
  if (!f_PixelSensorVolume) {f_PixelSensorVolume = detectorConstruction->GetMiniPAN().GetdetTPX3().GetPixelSensorLog();}
  if (!f_PixelAsicVolume) {f_PixelAsicVolume = detectorConstruction->GetMiniPAN().GetdetTPX3().GetPixelASICLog();}

  //Process StripXY  
  if(fPANSDConfig->AddSD_SiStripX && (volume == fSiStripXVolume))
    {if(!Process_MiniPAN_StripXY(aStep,volume)){return false;}}

  if(fPANSDConfig->AddSD_SiStripY){
    if(std::find(fSiStripYVolume.begin(), fSiStripYVolume.end(), volume) != fSiStripYVolume.end()){
      if(!Process_MiniPAN_StripXY(aStep,volume)){return false;}
    }
  }

  //Process TimePix3
  if(fPANSDConfig->AddSD_PixSensor && (volume == f_PixelSensorVolume)){
    if(!ProcessPixSensorInfo(aStep,volume)){return false;}
  }

  if(fPANSDConfig->AddSD_PixAsic && (volume == f_PixelAsicVolume))
    {if(!ProcessPixAsicInfo(aStep,volume)){return false;}}

  //Process TOF

  return true;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessShieldingInfo(G4Step* aStep, G4LogicalVolume* thisVolume){

  if (!(aStep->GetTotalEnergyDeposit() > 0.)) { return false; }

  G4StepPoint*      preStepPoint  = aStep->GetPreStepPoint();
  G4TouchableHandle preTouchable  = preStepPoint->GetTouchableHandle();

  G4int             copyID        = preTouchable->GetCopyNumber(0);
  G4double          energyDep     = aStep->GetTotalEnergyDeposit();

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  aHit->CopyID         = copyID;
  aHit->EnergyDep      = energyDep;
  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessAlCaseInfo(G4Step* aStep, G4LogicalVolume* thisVolume){

  if (!(aStep->GetTotalEnergyDeposit() > 0.)) { return false; }

  G4StepPoint*      preStepPoint  = aStep->GetPreStepPoint();
  G4TouchableHandle preTouchable  = preStepPoint->GetTouchableHandle();

  G4int             copyID        = preTouchable->GetCopyNumber(0);
  G4double          energyDep     = aStep->GetTotalEnergyDeposit();

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  aHit->CopyID         = copyID;
  aHit->EnergyDep      = energyDep;
  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessMagnetInfo(G4Step* aStep, G4LogicalVolume* thisVolume){

  if (!(aStep->GetTotalEnergyDeposit() > 0.)) { return false; }

  G4StepPoint*      preStepPoint  = aStep->GetPreStepPoint();
  G4TouchableHandle preTouchable  = preStepPoint->GetTouchableHandle();

  G4int             copyID        = preTouchable->GetCopyNumber(1);
  G4double          energyDep     = aStep->GetTotalEnergyDeposit();

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  aHit->CopyID         = copyID;
  aHit->EnergyDep      = energyDep;
  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessBerylliumInfo(G4Step* aStep, G4LogicalVolume* thisVolume){

  if (!(aStep->GetTotalEnergyDeposit() > 0.)) { return false; }

  G4StepPoint*      preStepPoint  = aStep->GetPreStepPoint();
  G4TouchableHandle preTouchable  = preStepPoint->GetTouchableHandle();

  G4int             copyID        = preTouchable->GetCopyNumber(0);
  G4double          energyDep     = aStep->GetTotalEnergyDeposit();

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  aHit->CopyID         = copyID;
  aHit->EnergyDep      = energyDep;
  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessSiliconBoxInfo(G4Step* aStep, G4LogicalVolume* thisVolume){

  if (!(aStep->GetTotalEnergyDeposit() > 0.)) { return false; }

  G4StepPoint*      preStepPoint  = aStep->GetPreStepPoint();
  G4TouchableHandle preTouchable  = preStepPoint->GetTouchableHandle();

  G4int             copyID        = preTouchable->GetCopyNumber(0);
  G4double          energyDep     = aStep->GetTotalEnergyDeposit();

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  aHit->CopyID         = copyID;
  aHit->EnergyDep      = energyDep;
  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessPixSensorInfo(G4Step* aStep, G4LogicalVolume* thisVolume){

  // track info
  G4Track* theTrack     = aStep->GetTrack();
  G4int    trackID      = theTrack->GetTrackID();
  G4String particleName = theTrack->GetDefinition()->GetParticleName();
  G4int    pdgID        = theTrack->GetDefinition()->GetPDGEncoding();
  G4int    parentID     = theTrack->GetParentID();

  // point info
  G4StepPoint*      preStepPoint      = aStep->GetPreStepPoint();
  G4StepPoint*      postStepPoint     = aStep->GetPostStepPoint();
  G4TouchableHandle preTouchable      = preStepPoint->GetTouchableHandle();
  G4TouchableHandle postTouchable     = postStepPoint->GetTouchableHandle();
  G4int             pixelID           = preTouchable->GetCopyNumber(0);
  G4int             copyID            = preTouchable->GetCopyNumber(1);
  G4String          volName           = preTouchable->GetVolume()->GetName();
  G4ThreeVector     WorldPosition     = preStepPoint->GetPosition();
  G4double          globalTime        = preStepPoint->GetGlobalTime();
  G4double          preKinEnergy      = preStepPoint->GetKineticEnergy();
  G4bool            isEntering        = (preStepPoint->GetStepStatus() == fGeomBoundary);
  G4bool            isLeaving         = (postStepPoint->GetStepStatus() == fGeomBoundary);
  G4String          processName       = postStepPoint->GetProcessDefinedStep()->GetProcessName();
  G4ThreeVector     LocalPosition     = preTouchable->GetHistory()->GetTopTransform().TransformPoint(WorldPosition);
  G4ThreeVector     ParticleMomentum  = theTrack->GetMomentum();

  G4double          energyDep         = aStep->GetTotalEnergyDeposit();
  G4double          deltaTime         = aStep->GetDeltaTime(); 

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  // store hit information
  aHit->TrackID        = trackID;
  aHit->pdgID          = pdgID;
  aHit->parentID       = parentID;
  aHit->VolName        = volName; 
  aHit->CopyID         = copyID;
  aHit->PixelID        = pixelID;
  aHit->ParticleName   = particleName;
  aHit->GlobalTime     = globalTime;
  aHit->StepStatus     = (isLeaving ? 2 : (isEntering ? 1 : 0));
  aHit->KinEnergy      = preKinEnergy;
  aHit->ProcessName    = processName;
  aHit->EnergyDep      = energyDep;
  aHit->DeltaTime      = deltaTime;
  aHit->LocalPosition  = LocalPosition;
  aHit->WorldPosition  = WorldPosition;
  aHit->Momentum       = ParticleMomentum;

  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::ProcessPixAsicInfo(G4Step* aStep, G4LogicalVolume* thisVolume){

  // track info
  G4Track* theTrack     = aStep->GetTrack();
  G4int    trackID      = theTrack->GetTrackID();
  G4String particleName = theTrack->GetDefinition()->GetParticleName();
  G4int    pdgID        = theTrack->GetDefinition()->GetPDGEncoding();
  G4int    parentID     = theTrack->GetParentID();

  // point info
  G4StepPoint*      preStepPoint      = aStep->GetPreStepPoint();
  G4StepPoint*      postStepPoint     = aStep->GetPostStepPoint();
  G4TouchableHandle preTouchable      = preStepPoint->GetTouchableHandle();
  G4TouchableHandle postTouchable     = postStepPoint->GetTouchableHandle();
  G4int             copyID            = preTouchable->GetCopyNumber(1);
  G4String          volName           = preTouchable->GetVolume()->GetName();
  G4ThreeVector     WorldPosition     = preStepPoint->GetPosition();
  G4double          globalTime        = preStepPoint->GetGlobalTime();
  G4double          preKinEnergy      = preStepPoint->GetKineticEnergy();
  G4bool            isEntering        = (preStepPoint->GetStepStatus() == fGeomBoundary);
  G4bool            isLeaving         = (postStepPoint->GetStepStatus() == fGeomBoundary);
  G4String          processName       = postStepPoint->GetProcessDefinedStep()->GetProcessName();
  G4ThreeVector     LocalPosition     = preTouchable->GetHistory()->GetTopTransform().TransformPoint(WorldPosition);
  G4ThreeVector     ParticleMomentum  = theTrack->GetMomentum();

  G4double          energyDep         = aStep->GetTotalEnergyDeposit();
  G4double          deltaTime         = aStep->GetDeltaTime(); 

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  // store hit information
  aHit->TrackID        = trackID;
  aHit->pdgID          = pdgID;
  aHit->parentID       = parentID;
  aHit->VolName        = volName;
  aHit->CopyID         = copyID;
  aHit->ParticleName   = particleName;
  aHit->GlobalTime     = globalTime;
  aHit->StepStatus     = (isLeaving ? 2 : (isEntering ? 1 : 0));
  aHit->KinEnergy      = preKinEnergy;
  aHit->ProcessName    = processName;
  aHit->EnergyDep      = energyDep;
  aHit->DeltaTime      = deltaTime;
  aHit->LocalPosition  = LocalPosition;
  aHit->WorldPosition  = WorldPosition;
  aHit->Momentum       = ParticleMomentum;

  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool PANSensitiveDetector::Process_MiniPAN_StripXY(G4Step* aStep, G4LogicalVolume* thisVolume){

  // track info
  G4Track* theTrack     = aStep->GetTrack();
  G4int    trackID      = theTrack->GetTrackID();
  G4String particleName = theTrack->GetDefinition()->GetParticleName();
  G4int    pdgID        = theTrack->GetDefinition()->GetPDGEncoding();
  G4int    parentID     = theTrack->GetParentID();

  // point info
  G4StepPoint*      preStepPoint      = aStep->GetPreStepPoint();
  G4StepPoint*      postStepPoint     = aStep->GetPostStepPoint();
  G4TouchableHandle preTouchable      = preStepPoint->GetTouchableHandle();
  G4TouchableHandle postTouchable     = postStepPoint->GetTouchableHandle();
  G4int             stripID           = preTouchable->GetCopyNumber(0);
  G4int             copyID            = preTouchable->GetCopyNumber(1);
  G4String          volName           = preTouchable->GetVolume()->GetName();
  G4ThreeVector     WorldPosition     = preStepPoint->GetPosition();
  G4double          globalTime        = preStepPoint->GetGlobalTime();
  G4double          preKinEnergy      = preStepPoint->GetKineticEnergy();
  G4bool            isEntering        = (preStepPoint->GetStepStatus() == fGeomBoundary);
  G4bool            isLeaving         = (postStepPoint->GetStepStatus() == fGeomBoundary);
  G4String          processName       = postStepPoint->GetProcessDefinedStep()->GetProcessName();
  G4ThreeVector     LocalPosition     = preTouchable->GetHistory()->GetTopTransform().TransformPoint(WorldPosition);
  G4ThreeVector     ParticleMomentum  = theTrack->GetMomentum();

  G4double          energyDep         = aStep->GetTotalEnergyDeposit();
  G4double          deltaTime         = aStep->GetDeltaTime(); 

  // generate a hit
  PANHit* aHit = new PANHit();
  CheckHitStatus(aHit);

  // store hit information
  aHit->TrackID        = trackID;
  aHit->pdgID          = pdgID;
  aHit->parentID       = parentID;
  aHit->VolName        = volName;
  aHit->CopyID         = copyID;
  aHit->StripID        = stripID;
  aHit->ParticleName   = particleName;
  aHit->GlobalTime     = globalTime;
  aHit->StepStatus     = (isLeaving ? 2 : (isEntering ? 1 : 0));
  aHit->KinEnergy      = preKinEnergy;
  aHit->ProcessName    = processName;
  aHit->EnergyDep      = energyDep;
  aHit->DeltaTime      = deltaTime;
  aHit->LocalPosition  = LocalPosition;
  aHit->WorldPosition  = WorldPosition;
  aHit->Momentum       = ParticleMomentum;

  aHit->MassVolume     = thisVolume->GetMass();

  fPANHitsCollection_->insert(aHit);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void PANSensitiveDetector::EndOfEvent(G4HCofThisEvent* ) {
    // at the end of an event
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void PANSensitiveDetector::CheckHitStatus(PANHit* aHit){
  if (aHit == NULL) {
    G4ExceptionDescription msg;
    msg << "Cannot access hit " << G4endl;
    G4Exception("PANSensitiveDetector::ProcessHits()", "MyCode0003", FatalException, msg);
  }
}

