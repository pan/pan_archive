#include "G4GeneralParticleSource.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4ParticleTable.hh"
#include "G4ThreeVector.hh"
#include "G4Alpha.hh"
#include "G4Proton.hh"
#include "G4Gamma.hh"
#include "G4Neutron.hh"
#include "G4Electron.hh"

#include "PANPrimaryGeneratorAction.hh"

#include "globals.hh"
#include "Randomize.hh"

PANPrimaryGeneratorAction::PANPrimaryGeneratorAction() {

    fParticleTable_     = G4ParticleTable::GetParticleTable();

    fParticleSource_ = new G4GeneralParticleSource();

    fPANGlobalConfig = PANGlobalConfig::Instance();

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

PANPrimaryGeneratorAction::~PANPrimaryGeneratorAction() {
    delete fParticleSource_;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
    fParticleSource_->GeneratePrimaryVertex(anEvent);    
}
