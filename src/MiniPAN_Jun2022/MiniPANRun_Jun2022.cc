#include "G4SDManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4RunManager.hh"
#include "G4AnalysisManager.hh"

#include "PANDetectorConstruction.hh"
#include "MiniPANRun_Jun2022.hh"

#include "TString.h"

MiniPANRun_Jun2022::MiniPANRun_Jun2022() {
    totalEvents_         = 0;
    totalElectronEvents_ = 0;
    totalProtonEvents_   = 0;

    fPANGlobalConfig = PANGlobalConfig::Instance();
    fPANSDConfig     = PANSDConfig::Instance();

    for(int iPart=0;iPart<2;iPart++){//particle type
      for(int ii=0;ii<6;ii++){
        fDose_StripX[iPart][ii] = 0;
      }
      for(int ii=0;ii<3;ii++){
        fDose_StripY[iPart][ii] = 0;
      }

      for(int ii=0;ii<2;ii++){
        fDose_PixSensor[iPart][ii] = 0;
        fDose_PixAsic[iPart][ii]   = 0;
      }
    }

    //default writes
    write_MiniPAN_stripX   = false;
    write_MiniPAN_stripY   = false;

    write_MiniPAN_pixSensor     = false;
    write_MiniPAN_pixAsic       = false;

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

MiniPANRun_Jun2022::~MiniPANRun_Jun2022() {

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::Merge(const G4Run* aRun){

	const MiniPANRun_Jun2022* localRun = static_cast<const MiniPANRun_Jun2022*>(aRun);

	totalEvents_             += localRun->totalEvents_;
  totalElectronEvents_     += localRun->totalElectronEvents_;
  totalProtonEvents_       += localRun->totalProtonEvents_;

  for(int iPart=0;iPart<2;iPart++){//particle type
    for(int ii=0;ii<6;ii++){
      fDose_StripX[iPart][ii] += localRun->fDose_StripX[iPart][ii];
    }
    
    for(int ii=0;ii<3;ii++){
      fDose_StripY[iPart][ii] += localRun->fDose_StripY[iPart][ii];
    }

    for(int ii=0;ii<2;ii++){
      fDose_PixSensor[iPart][ii] = localRun->fDose_PixSensor[iPart][ii];
      fDose_PixAsic[iPart][ii]   = localRun->fDose_PixAsic[iPart][ii];
    }

  }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

G4bool MiniPANRun_Jun2022::WritePrimary(G4int iPart){

  if(fPANSDConfig->AddSD_SiStripX){
    if((fDose_StripX[iPart][0] > 0) || (fDose_StripX[iPart][1] > 0) || (fDose_StripX[iPart][2] > 0) || 
       (fDose_StripX[iPart][3] > 0) || (fDose_StripX[iPart][4] > 0) || (fDose_StripX[iPart][5] > 0)) {return true;}
  	}

  if(fPANSDConfig->AddSD_SiStripY){
    if((fDose_StripY[iPart][0] > 0) || (fDose_StripY[iPart][1] > 0) || (fDose_StripY[iPart][2] > 0)) {return true;}
  }

  if(fPANSDConfig->AddSD_PixSensor){
    if((fDose_PixSensor[iPart][0] > 0) || (fDose_PixSensor[iPart][1] > 0)) {return true;}
    }

  if(fPANSDConfig->AddSD_PixAsic){
    if((fDose_PixAsic[iPart][0] > 0) || (fDose_PixAsic[iPart][1] > 0)) {return true;}
  }

  return false;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::RecordEvent(const G4Event* anEvent,const PANDetectorConstruction* detectorConstruction){

  //calculate number of SD types
  G4int nSD_type = 0;
  if(fPANSDConfig->AddSD_SiStripX || fPANGlobalConfig->WriteFluxInside){nSD_type++;}
  if(fPANSDConfig->AddSD_SiStripY || fPANGlobalConfig->WriteFluxInside){nSD_type++;}
  if(fPANSDConfig->AddSD_PixSensor){nSD_type++;}
  if(fPANSDConfig->AddSD_PixAsic){nSD_type++;}
  if(nSD_type < 1) {return;}

  // calculate and record value
  sPANSDHitCollection.clear();
  fPANHitsCollection.clear();
  numberOfHits.clear();

  sTotalNumberOfHits.clear();
  TotalNumberOfHits.clear();

  if(fPANSDConfig->AddSD_SiStripX || fPANGlobalConfig->WriteFluxInside){
    G4int collectionID = G4SDManager::GetSDMpointer()->GetCollectionID(detectorConstruction->GetSD_Name(detectorConstruction->GetMiniPAN().GetdetStripXY().GetSiStripXVolume()));
    sPANSDHitCollection.push_back("StripX");
    fPANHitsCollection.push_back(static_cast<PANHitsCollection*>(anEvent->GetHCofThisEvent()->GetHC(collectionID)));
    G4int i_element = fPANHitsCollection.size()-1;

    CheckHC_Status(fPANHitsCollection[i_element], collectionID);

    numberOfHits.push_back(fPANHitsCollection[i_element]->entries());
    G4double max_energy_dep = GetMaxEnergyDeposit(fPANHitsCollection[i_element], numberOfHits[i_element],9);
    if (max_energy_dep >= 0) {write_MiniPAN_stripX = true;}
    else {write_MiniPAN_stripX = false;}

    //count total hits
    std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), "StripX");
    if(it == sTotalNumberOfHits.end()){ //entry not found...add new entry
      sTotalNumberOfHits.push_back("StripX");
      TotalNumberOfHits.push_back(fPANHitsCollection[i_element]->entries());
    } else {
      G4int j_element = std::distance(sTotalNumberOfHits.begin(), it);
      TotalNumberOfHits[j_element] += fPANHitsCollection[i_element]->entries();
    }
  }

  if(fPANSDConfig->AddSD_SiStripY || fPANGlobalConfig->WriteFluxInside){
    for(size_t iStripY=0;iStripY<detectorConstruction->GetMiniPAN().GetdetStripXY().GetSiStripYVolume().size();iStripY++){
      G4int collectionID = G4SDManager::GetSDMpointer()->GetCollectionID(detectorConstruction->GetSD_Name(detectorConstruction->GetMiniPAN().GetdetStripXY().GetSiStripYVolume()[iStripY]));
      G4String theStripYname = "StripY_" + std::to_string(iStripY);
      sPANSDHitCollection.push_back(theStripYname);
      fPANHitsCollection.push_back(static_cast<PANHitsCollection*>(anEvent->GetHCofThisEvent()->GetHC(collectionID)));
      G4int i_element = fPANHitsCollection.size()-1;

      CheckHC_Status(fPANHitsCollection[i_element], collectionID);

      numberOfHits.push_back(fPANHitsCollection[i_element]->entries());
      G4double max_energy_dep = GetMaxEnergyDeposit(fPANHitsCollection[i_element], numberOfHits[i_element],3);
      if (max_energy_dep >= 0) {write_MiniPAN_stripY = true;}
      else {write_MiniPAN_stripY = false;}

      //count total hits
      std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), theStripYname);
      if(it == sTotalNumberOfHits.end()){ //entry not found...add new entry
        sTotalNumberOfHits.push_back(theStripYname);
        TotalNumberOfHits.push_back(fPANHitsCollection[i_element]->entries());
      } else {
        G4int j_element = std::distance(sTotalNumberOfHits.begin(), it);
        TotalNumberOfHits[j_element] += fPANHitsCollection[i_element]->entries();
      }
    }
  }

  if(fPANSDConfig->AddSD_PixSensor){
    G4int collectionID = G4SDManager::GetSDMpointer()->GetCollectionID(detectorConstruction->GetSD_Name(detectorConstruction->GetMiniPAN().GetdetTPX3().GetPixelSensorLog()));
    sPANSDHitCollection.push_back("pixSensor");
    fPANHitsCollection.push_back(static_cast<PANHitsCollection*>(anEvent->GetHCofThisEvent()->GetHC(collectionID)));
    G4int i_element = fPANHitsCollection.size()-1;

    CheckHC_Status(fPANHitsCollection[i_element], collectionID);

    numberOfHits.push_back(fPANHitsCollection[i_element]->entries());
    G4double max_energy_dep = GetMaxEnergyDeposit(fPANHitsCollection[i_element], numberOfHits[i_element],2);
    if (max_energy_dep >= 0) {write_MiniPAN_pixSensor = true;}
    else {write_MiniPAN_pixSensor = false;}

    //count total hits
    std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), "pixSensor");
    if(it == sTotalNumberOfHits.end()){ //entry not found...add new entry
      sTotalNumberOfHits.push_back("pixSensor");
      TotalNumberOfHits.push_back(fPANHitsCollection[i_element]->entries());
    } else {
      G4int j_element = std::distance(sTotalNumberOfHits.begin(), it);
      TotalNumberOfHits[j_element] += fPANHitsCollection[i_element]->entries();
    }
  }

  if(fPANSDConfig->AddSD_PixAsic){
    G4int collectionID = G4SDManager::GetSDMpointer()->GetCollectionID(detectorConstruction->GetSD_Name(detectorConstruction->GetMiniPAN().GetdetTPX3().GetPixelASICLog()));
    sPANSDHitCollection.push_back("pixAsic");
    fPANHitsCollection.push_back(static_cast<PANHitsCollection*>(anEvent->GetHCofThisEvent()->GetHC(collectionID)));
    G4int i_element = fPANHitsCollection.size()-1;

    CheckHC_Status(fPANHitsCollection[i_element], collectionID);

    numberOfHits.push_back(fPANHitsCollection[i_element]->entries());
    if(numberOfHits[i_element] > 0) { write_MiniPAN_pixAsic = true;}
    else{ write_MiniPAN_pixAsic = false;}

    //count total hits
    std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), "pixAsic");
    if(it == sTotalNumberOfHits.end()){ //entry not found...add new entry
      sTotalNumberOfHits.push_back("pixAsic");
      TotalNumberOfHits.push_back(fPANHitsCollection[i_element]->entries());
    } else {
      G4int j_element = std::distance(sTotalNumberOfHits.begin(), it);
      TotalNumberOfHits[j_element] += fPANHitsCollection[i_element]->entries();
    }
  }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::WritePrimaryDose(G4AnalysisManager* fAnalysisManager, const PANDetectorConstruction* detectorConstruction, G4int& addTupleCol){

    if(fPANSDConfig->AddSD_SiStripX){
      std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), "StripX");
      G4int i_element = std::distance(sTotalNumberOfHits.begin(), it);
      fAnalysisManager->FillNtupleIColumn(GetTreeID("Primary"), 8  + addTupleCol, TotalNumberOfHits[i_element] );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 9  + addTupleCol, fDose_StripX[0][0] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 10 + addTupleCol, fDose_StripX[0][1] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 11 + addTupleCol, fDose_StripX[0][2] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 12 + addTupleCol, fDose_StripX[0][3] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 13 + addTupleCol, fDose_StripX[0][4] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 14 + addTupleCol, fDose_StripX[0][5] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 15 + addTupleCol, fDose_StripX[1][0] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 16 + addTupleCol, fDose_StripX[1][1] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 17 + addTupleCol, fDose_StripX[1][2] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 18 + addTupleCol, fDose_StripX[1][3] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 19 + addTupleCol, fDose_StripX[1][4] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 20 + addTupleCol, fDose_StripX[1][5] / (1e-6*gray)   ); //electron
      addTupleCol += 13;
    }

    if(fPANSDConfig->AddSD_SiStripY){
      G4int totalNumberOfHits = 0;
      for(size_t iStripY=0;iStripY<detectorConstruction->GetMiniPAN().GetdetStripXY().GetSiStripYVolume().size();iStripY++){
        G4String theStripYname = "StripY_" + std::to_string(iStripY);
        std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), theStripYname);
        G4int i_element = std::distance(sTotalNumberOfHits.begin(), it);
        totalNumberOfHits += TotalNumberOfHits[i_element];
      }

      fAnalysisManager->FillNtupleIColumn(GetTreeID("Primary"), 8  + addTupleCol, totalNumberOfHits );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 9  + addTupleCol, fDose_StripY[0][0] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 10 + addTupleCol, fDose_StripY[0][1] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 11 + addTupleCol, fDose_StripY[0][2] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 12 + addTupleCol, fDose_StripY[1][0] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 13 + addTupleCol, fDose_StripY[1][1] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 14 + addTupleCol, fDose_StripY[1][2] / (1e-6*gray)   ); //electron
      addTupleCol += 7;
    }

    if(fPANSDConfig->AddSD_PixSensor){
      std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), "pixSensor");
      G4int i_element = std::distance(sTotalNumberOfHits.begin(), it);
      fAnalysisManager->FillNtupleIColumn(GetTreeID("Primary"), 8  + addTupleCol, TotalNumberOfHits[i_element] );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 9  + addTupleCol, fDose_PixSensor[0][0] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 10 + addTupleCol, fDose_PixSensor[0][1] / (1e-6*gray)   ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 11 + addTupleCol, fDose_PixSensor[1][0] / (1e-6*gray)   ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 12 + addTupleCol, fDose_PixSensor[1][1] / (1e-6*gray)   ); //electron
      addTupleCol += 5;
    }

    if(fPANSDConfig->AddSD_PixAsic){
      std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), "pixAsic");
      G4int i_element = std::distance(sTotalNumberOfHits.begin(), it);
      fAnalysisManager->FillNtupleIColumn(GetTreeID("Primary"), 8  + addTupleCol, TotalNumberOfHits[i_element]  );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 9  + addTupleCol, fDose_PixAsic[0][0] / (1e-6*gray)      ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 10 + addTupleCol, fDose_PixAsic[0][1] / (1e-6*gray)      ); //proton
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 11 + addTupleCol, fDose_PixAsic[1][0] / (1e-6*gray)      ); //electron
      fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 12 + addTupleCol, fDose_PixAsic[1][1] / (1e-6*gray)      ); //electron
      addTupleCol += 5;
    }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::WriteData(G4PrimaryVertex* primaryVertex, G4AnalysisManager* fAnalysisManager, const PANDetectorConstruction* detectorConstruction, G4int theEventID){

    //write hits in StripX
    if(fPANSDConfig->AddSD_SiStripX && write_MiniPAN_stripX){
      std::vector<G4String>::iterator it = std::find(sPANSDHitCollection.begin(), sPANSDHitCollection.end(), "StripX");
      G4int i_element = std::distance(sPANSDHitCollection.begin(), it);
      WriteData_StripX(primaryVertex,fPANHitsCollection[i_element], fAnalysisManager, numberOfHits[i_element], theEventID);  
    } 

    //write Flux in StripX
    if(fPANGlobalConfig->WriteFluxInside){
      std::vector<G4String>::iterator it = std::find(sPANSDHitCollection.begin(), sPANSDHitCollection.end(), "StripX");
      G4int i_element = std::distance(sPANSDHitCollection.begin(), it);
      WriteData_FluxInside(fPANHitsCollection[i_element], fAnalysisManager, numberOfHits[i_element]);
    }

    //write hits in StripY
    if(fPANSDConfig->AddSD_SiStripY &&  write_MiniPAN_stripY){
      for(size_t iStripY=0;iStripY<detectorConstruction->GetMiniPAN().GetdetStripXY().GetSiStripYVolume().size();iStripY++){
        G4String theStripYname = "StripY_" + std::to_string(iStripY);
        std::vector<G4String>::iterator it = std::find(sTotalNumberOfHits.begin(), sTotalNumberOfHits.end(), theStripYname);
        G4int i_element = std::distance(sTotalNumberOfHits.begin(), it);
        WriteData_StripY(primaryVertex,fPANHitsCollection[i_element], fAnalysisManager, numberOfHits[i_element], theEventID);
      }
    }

    //write hits in TimePix Sensor
    if(fPANSDConfig->AddSD_PixSensor && write_MiniPAN_pixSensor){
      std::vector<G4String>::iterator it = std::find(sPANSDHitCollection.begin(), sPANSDHitCollection.end(), "pixSensor");
      G4int i_element = std::distance(sPANSDHitCollection.begin(), it);
      WriteData_PixSensor(primaryVertex,fPANHitsCollection[i_element], fAnalysisManager, numberOfHits[i_element], theEventID);  
    } 

    //write hits in TimePix Asic
    if(fPANSDConfig->AddSD_PixAsic &&  write_MiniPAN_pixAsic){
      std::vector<G4String>::iterator it = std::find(sPANSDHitCollection.begin(), sPANSDHitCollection.end(), "pixAsic");
      G4int i_element = std::distance(sPANSDHitCollection.begin(), it);
      WriteData_PixAsic(primaryVertex,fPANHitsCollection[i_element], fAnalysisManager, numberOfHits[i_element], theEventID);
    }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::WriteData_StripX(G4PrimaryVertex* primaryVertex, PANHitsCollection* i_PANHitsCollection, G4AnalysisManager* fAnalysisManager, G4int i_numberOfHits, G4int theEventID){
  
  for(int iPart=0;iPart<2;iPart++){//particle type
    for(int ii=0;ii<6;ii++){fDose_StripX[iPart][ii] = 0;}
  }

  G4PrimaryParticle* primaryParticle = primaryVertex->GetPrimary(0);
  G4String        pname_primary   = primaryParticle->GetParticleDefinition()->GetParticleName();

  //set particle type
  int iPrimaryType = -1;
  if(pname_primary == "proton") {totalProtonEvents_++; iPrimaryType = 0;}
  if(pname_primary == "e-") {totalElectronEvents_++;iPrimaryType = 1;}

  // save hits
  for (int i = 0; i < i_numberOfHits; i++) {
    PANHit* aHit = static_cast<PANHit*>(i_PANHitsCollection->GetHit(i));
    if(!(aHit->EnergyDep>0)) continue;
    // necessary
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripX"), 0 , theEventID                       );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripX"), 1 , aHit->TrackID                    );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripX"), 2 , aHit->CopyID                     );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripX"), 3 , aHit->StripID                    );
    fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_StripX"), 4 , aHit->ParticleName               );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 5 , aHit->KinEnergy / keV            );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 6 , aHit->EnergyDep / keV            );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripX"), 7 , aHit->pdgID                      );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripX"), 8 , aHit->parentID                   );

    if (fPANGlobalConfig->simout_more) {
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_StripX"), 9 , aHit->VolName                    );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 10, aHit->GlobalTime / second        );
      fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripX"), 11, aHit->StepStatus                 );
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_StripX"), 12, aHit->ProcessName                );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 13, aHit->DeltaTime / second            );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 14, aHit->LocalPosition.x() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 15, aHit->LocalPosition.y() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 16, aHit->LocalPosition.z() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 17, aHit->WorldPosition.x() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 18, aHit->WorldPosition.y() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 19, aHit->WorldPosition.z() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 20, aHit->Momentum.x() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 21, aHit->Momentum.y() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripX"), 22, aHit->Momentum.z() / keV         );
    }

    fAnalysisManager->AddNtupleRow(GetTreeID("HitsCol_StripX"));

    //register dose
    const PANDetectorConstruction* detectorConstruction = static_cast<const PANDetectorConstruction*>
            (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

    fDose_StripX[iPrimaryType][aHit->CopyID] += aHit->EnergyDep / aHit->MassVolume / detectorConstruction->GetMiniPAN().GetdetStripXY().GetNoStripXPlacements();
  }
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::WriteData_StripY(G4PrimaryVertex* primaryVertex, PANHitsCollection* i_PANHitsCollection, G4AnalysisManager* fAnalysisManager, G4int i_numberOfHits, G4int theEventID){
  
  for(int iPart=0;iPart<2;iPart++){//particle type
    for(int ii=0;ii<3;ii++){fDose_StripY[iPart][ii] = 0;}
  }

  G4PrimaryParticle* primaryParticle = primaryVertex->GetPrimary(0);
  G4String        pname_primary   = primaryParticle->GetParticleDefinition()->GetParticleName();

  //set particle type
  int iPrimaryType = -1;
  if(pname_primary == "proton") {totalProtonEvents_++; iPrimaryType = 0;}
  if(pname_primary == "e-") {totalElectronEvents_++;iPrimaryType = 1;}

  // save hits
  for (int i = 0; i < i_numberOfHits; i++) {
    PANHit* aHit = static_cast<PANHit*>(i_PANHitsCollection->GetHit(i));
    if(!(aHit->EnergyDep>0)) continue;
    // necessary
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripY"), 0 , theEventID                       );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripY"), 1 , aHit->TrackID                    );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripY"), 2 , aHit->CopyID                     );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripY"), 3 , aHit->StripID                    );
    fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_StripY"), 4 , aHit->ParticleName               );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 5 , aHit->KinEnergy / keV            );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 6 , aHit->EnergyDep / keV            );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripY"), 7 , aHit->pdgID                      );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripY"), 8 , aHit->parentID                   );

    if (fPANGlobalConfig->simout_more) {
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_StripY"), 9 , aHit->VolName                    );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 10, aHit->GlobalTime / second        );
      fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_StripY"), 11, aHit->StepStatus                 );
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_StripY"), 12, aHit->ProcessName                );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 13, aHit->DeltaTime / second            );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 14, aHit->LocalPosition.x() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 15, aHit->LocalPosition.y() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 16, aHit->LocalPosition.z() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 17, aHit->WorldPosition.x() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 18, aHit->WorldPosition.y() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 19, aHit->WorldPosition.z() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 20, aHit->Momentum.x() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 21, aHit->Momentum.y() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_StripY"), 22, aHit->Momentum.z() / keV         );
    }

    fAnalysisManager->AddNtupleRow(GetTreeID("HitsCol_StripY"));

    //register dose
    const PANDetectorConstruction* detectorConstruction = static_cast<const PANDetectorConstruction*>
            (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

    fDose_StripY[iPrimaryType][aHit->CopyID] += aHit->EnergyDep / aHit->MassVolume / detectorConstruction->GetMiniPAN().GetdetStripXY().GetNoStripYPlacements();
  }
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::WriteData_PixSensor(G4PrimaryVertex* primaryVertex, PANHitsCollection* i_PANHitsCollection, G4AnalysisManager* fAnalysisManager, G4int i_numberOfHits, G4int theEventID){

  for(int iPart=0;iPart<2;iPart++){//particle type
    for(int ii=0;ii<2;ii++){fDose_PixSensor[iPart][ii] = 0;}
  }

  G4PrimaryParticle* primaryParticle = primaryVertex->GetPrimary(0);
  G4String        pname_primary   = primaryParticle->GetParticleDefinition()->GetParticleName();

  //set particle type
  int iPrimaryType = -1;
  if(pname_primary == "proton") {totalProtonEvents_++; iPrimaryType = 0;}
  if(pname_primary == "e-") {totalElectronEvents_++;iPrimaryType = 1;}

  // save hits
  for (int i = 0; i < i_numberOfHits; i++) {
    PANHit* aHit = static_cast<PANHit*>(i_PANHitsCollection->GetHit(i));
    if(!(aHit->EnergyDep>0)) continue;
    // necessary
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixSensor"), 0 , theEventID                       );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixSensor"), 1 , aHit->TrackID                    );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixSensor"), 2 , aHit->CopyID                     );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixSensor"), 3 , aHit->PixelID                    );
    fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_PixSensor"), 4 , aHit->ParticleName               );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 5 , aHit->KinEnergy / keV            );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 6 , aHit->EnergyDep / keV            );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixSensor"), 7 , aHit->pdgID                      );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixSensor"), 8 , aHit->parentID                   );

    if (fPANGlobalConfig->simout_more) {
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_PixSensor"), 9 , aHit->VolName                    );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 10, aHit->GlobalTime / second        );
      fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixSensor"), 11, aHit->StepStatus                 );
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_PixSensor"), 12, aHit->ProcessName                );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 13, aHit->DeltaTime / second            );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 14, aHit->LocalPosition.x() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 15, aHit->LocalPosition.y() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 16, aHit->LocalPosition.z() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 17, aHit->WorldPosition.x() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 18, aHit->WorldPosition.y() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 19, aHit->WorldPosition.z() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 20, aHit->Momentum.x() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 21, aHit->Momentum.y() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixSensor"), 22, aHit->Momentum.z() / keV         );
    }

    fAnalysisManager->AddNtupleRow(GetTreeID("HitsCol_PixSensor"));

    //register dose
    const PANDetectorConstruction* detectorConstruction = static_cast<const PANDetectorConstruction*>
            (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

    fDose_PixSensor[iPrimaryType][aHit->CopyID] += aHit->EnergyDep / aHit->MassVolume / detectorConstruction->GetMiniPAN().GetdetTPX3().GetNoPixSensorPlacements();
  }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::WriteData_PixAsic(G4PrimaryVertex* primaryVertex, PANHitsCollection* i_PANHitsCollection, G4AnalysisManager* fAnalysisManager, G4int i_numberOfHits, G4int theEventID){

  for(int iPart=0;iPart<2;iPart++){//particle type
    for(int ii=0;ii<2;ii++){fDose_PixAsic[iPart][ii] = 0;}
  }

  G4PrimaryParticle* primaryParticle = primaryVertex->GetPrimary(0);
  G4String        pname_primary   = primaryParticle->GetParticleDefinition()->GetParticleName();

  //set particle type
  int iPrimaryType = -1;
  if(pname_primary == "proton") {totalProtonEvents_++; iPrimaryType = 0;}
  if(pname_primary == "e-") {totalElectronEvents_++;iPrimaryType = 1;}

  // save hits
  for (int i = 0; i < i_numberOfHits; i++) {
    PANHit* aHit = static_cast<PANHit*>(i_PANHitsCollection->GetHit(i));
    if(!(aHit->EnergyDep>0)) continue;
    // necessary
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixAsic"), 0 , theEventID                       );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixAsic"), 1 , aHit->TrackID                    );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixAsic"), 2 , aHit->CopyID                     );
    fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_PixAsic"), 3 , aHit->ParticleName               );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 4 , aHit->KinEnergy / keV            );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 5 , aHit->EnergyDep / keV            );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixAsic"), 6 , aHit->pdgID                      );
    fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixAsic"), 7 , aHit->parentID                   );

    if (fPANGlobalConfig->simout_more) {
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_PixAsic"), 8 , aHit->VolName                    );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 9 , aHit->GlobalTime / second        );
      fAnalysisManager->FillNtupleIColumn(GetTreeID("HitsCol_PixAsic"), 10, aHit->StepStatus                 );
      fAnalysisManager->FillNtupleSColumn(GetTreeID("HitsCol_PixAsic"), 11, aHit->ProcessName                );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 12, aHit->DeltaTime / second            );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 13, aHit->LocalPosition.x() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 14, aHit->LocalPosition.y() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 15, aHit->LocalPosition.z() / cm     );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 16, aHit->Momentum.x() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 17, aHit->Momentum.y() / keV         );
      fAnalysisManager->FillNtupleDColumn(GetTreeID("HitsCol_PixAsic"), 18, aHit->Momentum.z() / keV         );
      }

    fAnalysisManager->AddNtupleRow(GetTreeID("HitsCol_PixAsic"));

    //register dose
    const PANDetectorConstruction* detectorConstruction = static_cast<const PANDetectorConstruction*>
            (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

    fDose_PixAsic[iPrimaryType][aHit->CopyID] += aHit->EnergyDep / aHit->MassVolume / detectorConstruction->GetMiniPAN().GetdetTPX3().GetNoPixAsicPlacements();
  }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

G4int MiniPANRun_Jun2022::GetTreeID(G4String stree) const {

  return fPANSDConfig->GetTreeID().at(stree);

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

G4int MiniPANRun_Jun2022::GetHistID(G4String sHist) const {

  return fPANSDConfig->GetHistID().at(sHist);

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::CheckHC_Status(PANHitsCollection* fPANHC, G4int collectionID){
  if (fPANHC == NULL) {
    G4ExceptionDescription msg;
    msg << "Cannot access hitsCollection: PANHitsCollection with ID " << collectionID;
    G4Exception("PANEventAction::EndOfEventAction", "MyCode0001", FatalException, msg);
  }
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

G4double MiniPANRun_Jun2022::GetMaxEnergyDeposit(PANHitsCollection* fPANHC, G4int i_numberOfHits, G4int nComponents){

  if (i_numberOfHits < 1) {return 0.;}

  std::vector<G4double> energy_dep(nComponents,0.);

  for (int i = 0; i < i_numberOfHits; i++) {
    PANHit* aHit = static_cast<PANHit*>(fPANHC->GetHit(i));
    energy_dep[aHit->CopyID] += aHit->EnergyDep;
  }

  G4double max_energy_dep = 0;
  for (int iComp = 0; iComp < nComponents; iComp++) {
    if (max_energy_dep < energy_dep[iComp]) {max_energy_dep = energy_dep[iComp];}	
  }

  return max_energy_dep;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::WriteData_FluxInside(PANHitsCollection* i_PANHitsCollection, G4AnalysisManager* fAnalysisManager, G4int i_numberOfHits){

  // save hits
  for (int i = 0; i < i_numberOfHits; i++) {
    PANHit* aHit = static_cast<PANHit*>(i_PANHitsCollection->GetHit(i));
    if(aHit->StepStatus != 1) continue;  //Particle MUST be entering

    if(aHit->ParticleName == "e+" || aHit->ParticleName == "e-"){
      fAnalysisManager->FillH1(GetHistID(Form("Electron_%d",aHit->CopyID)),aHit->KinEnergy);
    } else if (aHit->ParticleName == "proton") {
      fAnalysisManager->FillH1(GetHistID(Form("Proton_%d",aHit->CopyID)),aHit->KinEnergy);
    } else if (aHit->ParticleName == "gamma") {
      fAnalysisManager->FillH1(GetHistID(Form("Gamma_%d",aHit->CopyID)),aHit->KinEnergy);
    }
    
    fAnalysisManager->FillH1(GetHistID(Form("Total_%d",aHit->CopyID)),aHit->KinEnergy);

  }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void MiniPANRun_Jun2022::AddConfig(G4AnalysisManager* fAnalysisManager){

  fAnalysisManager->FillNtupleIColumn(1, 2, G4int(fPANSDConfig->AddSD_SiStripX));
  fAnalysisManager->FillNtupleIColumn(1, 3, G4int(fPANSDConfig->AddSD_SiStripY));
  fAnalysisManager->FillNtupleIColumn(1, 4, G4int(fPANSDConfig->AddSD_PixSensor));
  fAnalysisManager->FillNtupleIColumn(1, 5, G4int(fPANSDConfig->AddSD_PixAsic));

}
