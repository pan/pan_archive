#include "instr_MiniPAN_Jun2022.hh"

//material
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4NistManager.hh"

//geometry
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"

//sensitive detector
#include "G4SDManager.hh"
#include "PANSensitiveDetector.hh"

//magnetic field
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4UniformMagField.hh"
#include "PANTabulatedField3D.hh"

//visual attribute
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//global function
#include "globals.hh"

MiniPAN_Jun2022_Instrument::MiniPAN_Jun2022_Instrument() {

    AluSupportLog_     = NULL;
    OuterAluFrameLog_  = NULL;
    CenterAluFrameLog_ = NULL;


    fPANGlobalConfig = PANGlobalConfig::Instance();
    fPANSDConfig     = PANSDConfig::Instance();

    fMagField.Put(0);

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void MiniPAN_Jun2022_Instrument::ConstructSDandField(std::map<G4LogicalVolume*,G4String>& instrumentSD) {

    //create magnetic field
    if(fMagField.Get() == 0 && fPANGlobalConfig->AddMagneticField){

      //Field grid in table.
      //G4String theFile = MagnetDataPath + G4String("/2-magnets-13p4mm-N48.txt"); //
      G4String theFile = MagnetDataPath + G4String("/2-magnets-12mm-N48.txt");
      G4MagneticField* PANMagField= new PANTabulatedField3D(theFile, 0); 
      fMagField.Put(PANMagField);
      
      //This is thread-local
      G4FieldManager* pFieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
           
      G4cout<< "DeltaStep "<<pFieldMgr->GetDeltaOneStep()/mm <<"mm" <<G4endl;
      //G4ChordFinder *pChordFinder = new G4ChordFinder(PurgMagField);

      pFieldMgr->SetDetectorField(fMagField.Get());
      pFieldMgr->CreateChordFinder(fMagField.Get());

    }

    //create SD for PixPAN
    if (!fPANGlobalConfig->primary_only) {

      if(fPANSDConfig->AddSD_PixSensor){
        PANSensitiveDetector* PAN_SD = TimePix3.AddSD(TimePix3.GetPixelSensorLog(),instrumentSD);
        G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);
        TimePix3.GetPixelSensorLog()->SetSensitiveDetector(PAN_SD);  
      }

      if(fPANSDConfig->AddSD_PixAsic){
        PANSensitiveDetector* PAN_SD = TimePix3.AddSD(TimePix3.GetPixelASICLog(),instrumentSD);
        TimePix3.GetPixelASICLog()->SetSensitiveDetector(PAN_SD);
      }

      if(fPANGlobalConfig->UseSSD){
        if(fPANSDConfig->AddSD_SiStripX || fPANGlobalConfig->WriteFluxInside){
          PANSensitiveDetector* PAN_SD = StripXY.AddSD(StripXY.GetSiStripXVolume(),instrumentSD);
          G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);
          StripXY.GetSiStripXVolume()->SetSensitiveDetector(PAN_SD);
        }

        if(fPANSDConfig->AddSD_SiStripY || fPANGlobalConfig->WriteFluxInside){
          for(size_t iStripY=0;iStripY<StripXY.GetSiStripYVolume().size();iStripY++){
            PANSensitiveDetector* PAN_SD = StripXY.AddSD(StripXY.GetSiStripYVolume()[iStripY],instrumentSD,iStripY);
            G4SDManager::GetSDMpointer()->AddNewDetector(PAN_SD);
            StripXY.GetSiStripYVolume()[iStripY]->SetSensitiveDetector(PAN_SD);
          }
        }
      }

    }
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

MiniPAN_Jun2022_Instrument::~MiniPAN_Jun2022_Instrument() {

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void MiniPAN_Jun2022_Instrument::DefineMaterials_() {

  //-----------------------------------------------------------------------------------------------------------
  //--------Define Material
  //-----------------------------------------------------------------------------------------------------------

  AmbientEnv_   = NULL;
  Polythene_Al_ = NULL;
  G4_Al_        = NULL;
  G4_Si_        = NULL;

  G4double density;                         // matter density
  G4double fractionmass;                    // compound proportion
  G4String name;                            // material name
  G4int ncomponents;                        // component number


  G4NistManager* man = G4NistManager::Instance();

  if(fPANGlobalConfig->IsInSpace) {AmbientEnv_ = man->FindOrBuildMaterial("G4_Galactic");}  // for space
  else {AmbientEnv_ = man->FindOrBuildMaterial("G4_AIR");}     

  // Elements
  G4Element* elAl  = man->FindOrBuildElement("Al");
  G4Element* elC  = man->FindOrBuildElement("C");
  G4Element* elH  = man->FindOrBuildElement("H");


  // MLI Polythene
  Polythene_Al_ = new G4Material(name = "Polythene", density = 0.975*g/cm3, ncomponents = 3);
  Polythene_Al_->AddElement(elC, fractionmass = 85.68*perCent);
  Polythene_Al_->AddElement(elH, fractionmass = 14.28*perCent);
  Polythene_Al_->AddElement(elAl, fractionmass = 0.04*perCent);

  // G4_Al
  G4_Al_ = man->FindOrBuildMaterial("G4_Al");

  //G4_Si
  G4_Si_ = man->FindOrBuildMaterial("G4_Si");

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4VPhysicalVolume* MiniPAN_Jun2022_Instrument::Construct(){

  DefineMaterials_();

  // Construct WorldPhys
  G4double world_hx = (fPANGlobalConfig->spacelab ? 12*m : 500*mm);
  G4double world_hy = (fPANGlobalConfig->spacelab ? 30*m : 500*mm);
  G4double world_hz = (fPANGlobalConfig->spacelab ? 12*m : 500*mm);
  G4Box* WorldBox = new G4Box("WorldBox", world_hx, world_hy, world_hz);
  G4LogicalVolume* WorldLog_ = new G4LogicalVolume(WorldBox, AmbientEnv_, "WorldLog_");

  G4LogicalVolume* DetectorLog_ = ConstructMiniPAN_Jun2022_Detector_();

  new G4PVPlacement(NULL, G4ThreeVector(0,0,0), DetectorLog_, "MiniPAN_Jun2022", WorldLog_, false, 0);

  //set visualization of World
  G4VisAttributes* VisAtt_WorldLog = new G4VisAttributes(true, G4Colour(1.0, 0.0, 0.0, 1.0));
  VisAtt_WorldLog->SetForceWireframe(!fPANGlobalConfig->visual_light);
  VisAtt_WorldLog->SetVisibility(!fPANGlobalConfig->visual_light);
  WorldLog_->SetVisAttributes(VisAtt_WorldLog);

  // Return WorldPhys
  G4VPhysicalVolume* WorldPhys = new G4PVPlacement(NULL, G4ThreeVector(), WorldLog_, "World", 0, false, 0);

  return WorldPhys;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4LogicalVolume* MiniPAN_Jun2022_Instrument::ConstructMiniPAN_Jun2022_Detector_(){
    //....oooOO0OOooo.......
    G4Box* DetectorBox = new G4Box("DetectorBox",MiniPAN_DetectorBox_x/2,MiniPAN_DetectorBox_y/2,MiniPAN_DetectorBox_z/2);
    G4LogicalVolume* DetectorLog_ = new G4LogicalVolume(DetectorBox, AmbientEnv_, "DetectorLog");
    G4VisAttributes* VisAtt_DetectorLog = new G4VisAttributes(true, G4Colour(1.0, 0.0, 0.0, 1.0));
    VisAtt_DetectorLog->SetForceWireframe(!fPANGlobalConfig->visual_light);
    VisAtt_DetectorLog->SetVisibility(!fPANGlobalConfig->visual_light);
    DetectorLog_->SetVisAttributes(VisAtt_DetectorLog);


    ////MLI
    if(fPANGlobalConfig->UseMLI){
      G4Box* MLIBox;
      if(fPANGlobalConfig->visual_light) {MLIBox = new G4Box("MLIBox", MLI_XY/2, MLI_XY/2, (MLIThickness*nMLI + MLIGap*(nMLI-1))/2);}
      else {MLIBox = new G4Box("MLIBox", MLI_XY/2, MLI_XY/2, MLIThickness/2);}
      G4LogicalVolume* MLI_Log_ = new G4LogicalVolume(MLIBox, Polythene_Al_, "MLILog");
      G4VisAttributes* VisAtt_MLILog = new G4VisAttributes(true, G4Color(0.66, 0.0, 1.0, 0.2));
      VisAtt_MLILog->SetForceSolid(true);
      MLI_Log_->SetVisAttributes(VisAtt_MLILog);

      if(fPANGlobalConfig->visual_light){
        new G4PVPlacement(NULL, G4ThreeVector(0,0,-MLI_XY), MLI_Log_, "MLI", DetectorLog_, false, 0);
        new G4PVPlacement(NULL, G4ThreeVector(0,0,MLI_XY), MLI_Log_, "MLI", DetectorLog_, false, 1);
      }else{
        for(G4int iMLI=0; iMLI<nMLI; iMLI++){
          new G4PVPlacement(NULL, G4ThreeVector(0,0,-MLI_XY/2-(MLIThickness+MLIGap)*iMLI), MLI_Log_, "MLI", DetectorLog_, false, iMLI);
          new G4PVPlacement(NULL, G4ThreeVector(0,0,MLI_XY/2+(MLIThickness+MLIGap)*iMLI+moduleOffset_z), MLI_Log_, "MLI", DetectorLog_, false, iMLI);
        }
      }
    }

    ////Magnets & Casing
    std::pair<G4LogicalVolume*,G4LogicalVolume*> volPair = Magnet.ConstructMagnet_();
    G4LogicalVolume* MagnetLog_ = volPair.first;
    G4LogicalVolume* AlCaseLog_ = Magnet.ConstructAluCase_();
    
    for(int iMagnet =0; iMagnet<nMagnets;iMagnet++){
      new G4PVPlacement(NULL, G4ThreeVector(0,0,MiniPAN_MagPos_z[iMagnet]+(iMagnet == 0 ? 0 : moduleOffset_z)), AlCaseLog_, "AluCasing", DetectorLog_, false,iMagnet);
      new G4PVPlacement(NULL, G4ThreeVector(0,0,MiniPAN_MagPos_z[iMagnet]+(iMagnet == 0 ? 0 : moduleOffset_z)), MagnetLog_, "Magnet", DetectorLog_, false, iMagnet);
    }

    //Silicon Strip Detector
    if(fPANGlobalConfig->UseSSD){
      //StripX
      G4LogicalVolume* StripXLog = StripXY.ConstructStripX();
      int iStripX = 0;
      for(int  iLayer=0; iLayer<(SiStripX_nLayers+SiStripY_nLayers);iLayer++){
        if(iLayer == 2 || iLayer == 4 || iLayer == 8) continue; //ignore StipY Layers
        G4double z_pos_1, z_pos_2, z_pos_3;
        if(iLayer >= 0 && iLayer <= 2){
          z_pos_1 = MiniPAN_MagPos_z[0] - Magnet.magnet_length/2 - PCB_Gap2Magnet[0] - 5*StripPCB.PCB_Box_z/2 - PCB_Gap[0] - PCB_Gap[1];
        } else if(iLayer >= 3 && iLayer <= 5){
          z_pos_1 = MiniPAN_MagPos_z[0] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1] + StripPCB.PCB_Box_z/2;
        } else if(iLayer >= 6 && iLayer <= 8){
          z_pos_1 = MiniPAN_MagPos_z[1] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1] + StripPCB.PCB_Box_z/2 + moduleOffset_z;
        }

        z_pos_2 = z_pos_1 + PCB_Gap[0+(iLayer/3)*2] + StripPCB.PCB_Box_z;
        z_pos_3 = z_pos_2 + PCB_Gap[1+(iLayer/3)*2] + StripPCB.PCB_Box_z;

        G4double z_pos;
        if(iLayer == 0) z_pos = z_pos_1 + (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);
        if(iLayer == 1) z_pos = z_pos_2 - (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);
        if(iLayer == 3) z_pos = z_pos_1 + (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);
        if(iLayer == 5) z_pos = z_pos_3 + (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);
        if(iLayer == 6) z_pos = z_pos_1 + (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);
        if(iLayer == 7) z_pos = z_pos_2 - (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);

        new G4PVPlacement(NULL, G4ThreeVector(0,0,z_pos), StripXLog, "SiStripX", DetectorLog_, false, iStripX);
        iStripX++;
      }

      //StripY
      G4LogicalVolume* StripYLog = StripXY.ConstructStripY();
      for (G4int iLayer=0; iLayer<SiStripY_nLayers; iLayer++){

        G4double z_pos_1, z_pos_2, z_pos_3;
        if(iLayer ==0){
          z_pos_1 = MiniPAN_MagPos_z[0] - Magnet.magnet_length/2 - PCB_Gap2Magnet[0] - 5*StripPCB.PCB_Box_z/2 - PCB_Gap[0] - PCB_Gap[1];
        } else if(iLayer ==1){
          z_pos_1 = MiniPAN_MagPos_z[0] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1] + StripPCB.PCB_Box_z/2;
        } else if(iLayer ==2){
          z_pos_1 = MiniPAN_MagPos_z[1] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1] + StripPCB.PCB_Box_z/2 + moduleOffset_z;
        }

        z_pos_2 = z_pos_1 + PCB_Gap[0+iLayer*2] + StripPCB.PCB_Box_z;
        z_pos_3 = z_pos_2 + PCB_Gap[1+iLayer*2] + StripPCB.PCB_Box_z;

        G4double z_pos;
        if(iLayer == 0) z_pos = z_pos_3 + (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);
        if(iLayer == 1) z_pos = z_pos_2 - (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);
        if(iLayer == 2) z_pos = z_pos_3 + (StripPCB.PCB_Box_z/2 + StripXY.SiStrip_thickness/2.);

        new G4PVPlacement(NULL, G4ThreeVector(0, 0, z_pos), StripYLog, "SiStripY", DetectorLog_, false, iLayer);
      }        

    }

    ////PCB
    G4LogicalVolume* PCB_StripLog = StripPCB.ConstructStripPCB();
    G4RotationMatrix * rot_pcb = new G4RotationMatrix();
    rot_pcb->rotateZ(180*deg);
    for (G4int ilayer=0; ilayer<nPCB_segments; ilayer++){

      G4double z_pos_1, z_pos_2, z_pos_3;
      if(ilayer == 0){
        z_pos_1 = MiniPAN_MagPos_z[0] - Magnet.magnet_length/2 - PCB_Gap2Magnet[0] - 5*StripPCB.PCB_Box_z/2 - PCB_Gap[0] - PCB_Gap[1];
      } else if(ilayer ==1){
        z_pos_1 = MiniPAN_MagPos_z[0] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1] + StripPCB.PCB_Box_z/2;
      } else if(ilayer ==2){
        z_pos_1 = MiniPAN_MagPos_z[1] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1] + StripPCB.PCB_Box_z/2 + moduleOffset_z;
      }

      z_pos_2 = z_pos_1 + PCB_Gap[0+ilayer*2] + StripPCB.PCB_Box_z;
      z_pos_3 = z_pos_2 + PCB_Gap[1+ilayer*2] + StripPCB.PCB_Box_z;

      new G4PVPlacement(NULL, G4ThreeVector(0,-(StripPCB.PCB_HoleCenterOffset-StripPCB.PCB_Box_y/2.),z_pos_1), PCB_StripLog, "PCB_Strip", DetectorLog_, false, 0+ ilayer*nPCB_segments);
      new G4PVPlacement(rot_pcb, G4ThreeVector(0,(StripPCB.PCB_HoleCenterOffset-StripPCB.PCB_Box_y/2.),z_pos_2), PCB_StripLog, "PCB_Strip", DetectorLog_, false, 1 + ilayer*nPCB_segments);
      new G4PVPlacement(rot_pcb, G4ThreeVector(0,(StripPCB.PCB_HoleCenterOffset-StripPCB.PCB_Box_y/2.),z_pos_3), PCB_StripLog, "PCB_Strip", DetectorLog_, false, 2 + ilayer*nPCB_segments);

    }

    //Alu Support 
    if(ConstructMiniPAN_AluSupport()){
      G4double z_pos_support1 = MiniPAN_MagPos_z[0] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1]-Support_box_z/2;
      G4double z_pos_support2 = z_pos_support1 + PCB_Gap[2] + PCB_Gap[3] + StripPCB.PCB_Box_z*3 + Support_box_z + moduleOffset_z;
      new G4PVPlacement(NULL, G4ThreeVector(0,0,z_pos_support1),AluSupportLog_,"Alu_Support",DetectorLog_,false,0);
      new G4PVPlacement(NULL, G4ThreeVector(0,0,z_pos_support2),AluSupportLog_,"Alu_Support",DetectorLog_,false,1);
    }

    //Outer Alu Frame
    if(ConstructMiniPAN_OuterAluFrame()){
      G4double z_pos_frame1 = MiniPAN_MagPos_z[0] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1]-Support_box_z -Frame_box_z/2;
      G4double z_pos_frame2 = z_pos_frame1 + PCB_Gap[2] + PCB_Gap[3] + StripPCB.PCB_Box_z*3 + Support_box_z*2+Frame_box_z + moduleOffset_z;
      new G4PVPlacement(NULL, G4ThreeVector(0,0,z_pos_frame1),OuterAluFrameLog_,"OuterAlu_Frame",DetectorLog_,false,0);
      new G4PVPlacement(NULL, G4ThreeVector(0,0,z_pos_frame2),OuterAluFrameLog_,"OuterAlu_Frame",DetectorLog_,false,1);
    }

    //Center Alu Frame
    if(ConstructMiniPAN_CenterAluFrame()){
      G4double z_pos_centerFrame = MiniPAN_MagPos_z[0] + Magnet.magnet_length/2 + PCB_Gap2Magnet[1]+CenterFrame_box_z/2;
      new G4PVPlacement(NULL, G4ThreeVector(0,0,z_pos_centerFrame),CenterAluFrameLog_,"CenterAlu_Frame",DetectorLog_,false,0);
    }

    // TimePix3
    if(fPANGlobalConfig->UseTPx3){ //TimePix3 design
         
      G4LogicalVolume* TPX3Log_ = TimePix3.ConstructTimePix3_();

      G4RotationMatrix * rotax = new G4RotationMatrix();
      rotax->rotateX(180*deg);
      for(int iLayer =0; iLayer<nTpx3Layers; iLayer++){
        new G4PVPlacement((iLayer==0 ? rotax : NULL), G4ThreeVector(0, 0, Tpx3Distance[iLayer]), TPX3Log_, "TimePix3", DetectorLog_, false, iLayer);
      }
    }

    //TOF
    if(fPANGlobalConfig->UseTOF){ //TOF design
      G4LogicalVolume* TOFLog_ = TOF.ConstructTOF();
      for(int iTOF=0;iTOF<nTOF;iTOF++){
        new G4PVPlacement(NULL, G4ThreeVector(0,0, TOF_Center[iTOF] + (iTOF == 0 ? 0. : moduleOffset_z)), TOFLog_, "TOF", DetectorLog_, false, iTOF);
      }
    }

    return DetectorLog_;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool MiniPAN_Jun2022_Instrument::ConstructMiniPAN_AluSupport(){

    //The Support is characterized as a box with subtracted volumes (i.e. "CutOut")
    //CutOut1
    G4Box* CutOut1_Box  = new G4Box("CutOut1_Box",CutOut1_box_x/2,CutOut1_box_y/2,CutOut1_thickness);
    G4Box* CutOut1_Edge = new G4Box("CutOut1_Edge",CutOut1_edgeR/2,CutOut1_edgeR/2,CutOut1_thickness+0.1*mm);
    G4Box* CutOut1_Void = new G4Box("CutOut1_Void",CutOut1_Voidbox_x/2,CutOut1_Voidbox_y/2,CutOut1_thickness+0.1*mm);
    G4Box* CutOut1_BottomVoid = new G4Box("CutOut1_BottomVoid",CutOut1_BottomVoidbox_x/2,CutOut1_BottomVoidbox_y,CutOut1_thickness+0.1*mm);
    G4VSolid* CutOut1_Round   = new G4Tubs("CutOut1_Round",0,CutOut1_edgeR, CutOut1_thickness+0.1*mm, 0, 2.* M_PI * rad);
    G4VSolid* CutOut_Center   = new G4Tubs("CutOut_Center",0,Magnet.Al_case_outR, CutOut1_thickness+0.1*mm, 0, 2.* M_PI * rad);

    //2 * asin(Al_case_outR/(CutOut1_SliceCenterWidth/2))
    G4VSolid* CutOut_SliceCenter = new G4Tubs("CutOut_SliceCenter",0,Magnet.Al_case_outR, CutOut1_thickness, (-31.76+90)*deg, 2*31.76*deg);

    G4VSolid* EdgeClip;
    if(fPANGlobalConfig->visual_light){EdgeClip = CutOut1_Edge;}
    else{EdgeClip = new G4SubtractionSolid("CutOut1_Edge-CutOut1_Round",CutOut1_Edge,CutOut1_Round,NULL,G4ThreeVector(CutOut1_edgeR/2,-CutOut1_edgeR/2,0));}

    G4VSolid* CutOut1_noEdges = CutOut1_Box;
    G4RotationMatrix * rotEdge = new G4RotationMatrix();
    for(int iEdge=0;iEdge<6;iEdge++){
      if(iEdge==0) {
        rotEdge->rotateZ(0*deg);
        CutOut1_noEdges = new G4SubtractionSolid("CutOut1_noEdges-EdgeClip",CutOut1_noEdges,EdgeClip,rotEdge,G4ThreeVector(-CutOut1_box_x/2+CutOut1_edgeR/2,CutOut1_box_y/2-CutOut1_edgeR/2,0));
      } else if(iEdge==1) {
        rotEdge->rotateZ(-90*deg);
        CutOut1_noEdges = new G4SubtractionSolid("CutOut1_noEdges-EdgeClip",CutOut1_noEdges,EdgeClip,rotEdge,G4ThreeVector(-CutOut1_box_x/2+CutOut1_edgeR/2,-CutOut1_box_y/2+CutOut1_edgeR/2,0));
      } else if(iEdge==2) {
        rotEdge->rotateZ(-90*deg);
        CutOut1_noEdges = new G4SubtractionSolid("CutOut1_noEdges-EdgeClip",CutOut1_noEdges,EdgeClip,rotEdge,G4ThreeVector(CutOut1_box_x/2-CutOut1_edgeR/2,-CutOut1_box_y/2+CutOut1_edgeR/2,0));      
      } else if(iEdge==3) {
        rotEdge->rotateZ(-90*deg);
        CutOut1_noEdges = new G4SubtractionSolid("CutOut1_noEdges-EdgeClip",CutOut1_noEdges,EdgeClip,rotEdge,G4ThreeVector(CutOut1_box_x/2-CutOut1_edgeR/2,CutOut1_box_y/2-CutOut1_edgeR/2,0));      
      } else if(iEdge==4) {
        rotEdge->rotateZ(90*deg);
        CutOut1_noEdges = new G4SubtractionSolid("CutOut1_noEdges-EdgeClip",CutOut1_noEdges,EdgeClip,rotEdge,G4ThreeVector(-CutOut1_box_x/2-CutOut1_edgeR/2+CutOut1_VoidOffset_x,-CutOut1_box_y/2+CutOut1_edgeR/2,0));
      } else if(iEdge==5) {
        rotEdge->rotateZ(90*deg);
        CutOut1_noEdges = new G4SubtractionSolid("CutOut1_noEdges-EdgeClip",CutOut1_noEdges,EdgeClip,rotEdge,G4ThreeVector(CutOut1_box_x/2+CutOut1_edgeR/2-CutOut1_VoidOffset_x,-CutOut1_box_y/2+CutOut1_edgeR/2,0));  
      }
    }

    G4VSolid* CutOut1_noVoid = CutOut1_noEdges;
    CutOut1_noVoid = new G4SubtractionSolid("CutOut1_noVoid-CutOut1_Void",CutOut1_noVoid,CutOut1_Void,NULL,G4ThreeVector((-CutOut1_Voidbox_x/2+CutOut1_VoidOffset_x+CutOut1_Voidbox_x/2),-CutOut1_box_y/2-CutOut1_Voidbox_y/2,0));
    CutOut1_noVoid = new G4SubtractionSolid("CutOut1_noVoid-CutOut1_Void",CutOut1_noVoid,CutOut1_Void,NULL,G4ThreeVector(-(-CutOut1_Voidbox_x/2+CutOut1_VoidOffset_x+CutOut1_Voidbox_x/2),-CutOut1_box_y/2-CutOut1_Voidbox_y/2,0));

    G4VSolid* CutOut1 = new G4SubtractionSolid("CutOut1_noVoid-CutOut_Center",CutOut1_noVoid,CutOut1_BottomVoid,NULL,G4ThreeVector(0,-CutOut1_box_y/2,0));
    CutOut1 = new G4SubtractionSolid("CutOut1-CutOut_Center",CutOut1,CutOut_SliceCenter,NULL,G4ThreeVector(0,CutOut1_box_y/2-CutOut1_CenterOffset_y,0));

    //CutOut2
    G4Box* CutOut2 = new G4Box("CutOut2",CutOut2_box_x,CutOut2_box_y/2,CutOut2_box_z/2);

    //CutOut3
    G4Box* CutOut3_Box  = new G4Box("CutOut1_Box",CutOut3_box_x/2,CutOut3_box_y,CutOut3_box_z/2);
    G4Box* CutOut3_Void = new G4Box("CutOut1_Void",CutOut3_Voidbox_x/2,CutOut3_Voidbox_y,CutOut3_Voidbox_z/2);

    G4VSolid* CutOut3 = new G4SubtractionSolid("CutOut3_Box-CutOut3_Void",CutOut3_Box,CutOut3_Void,NULL,G4ThreeVector(0,0,CutOut3_box_z/2));
    CutOut3 = new G4SubtractionSolid("CutOut3_Box-CutOut3_Void",CutOut3,CutOut3_Void,NULL,G4ThreeVector(0,0,-CutOut3_box_z/2));

    //Support
    G4RotationMatrix * rot_cutout1 = new G4RotationMatrix();
    rot_cutout1->rotateZ(180*deg);
    G4Box* Support_Box  = new G4Box("Support_Box",Support_box_x/2,Support_box_y/2,Support_box_z/2);

    //remove CutOut1
    G4VSolid* AluSupportBox = Support_Box;
    if(!fPANGlobalConfig->visual_light) {
      AluSupportBox = new G4SubtractionSolid("AluSupportBox-CutOut1",AluSupportBox,CutOut1,NULL,G4ThreeVector(0,Support_box_y/2-CutOut1_position_y-CutOut1_box_y/2,0));
      AluSupportBox = new G4SubtractionSolid("AluSupportBox-CutOut1",AluSupportBox,CutOut1,rot_cutout1,G4ThreeVector(0,-(Support_box_y/2-CutOut1_position_y-CutOut1_box_y/2),0));
    }

    //remove CutOut2
    AluSupportBox = new G4SubtractionSolid("AluSupportBox-CutOut2",AluSupportBox,CutOut2,NULL,G4ThreeVector(Support_box_x/2,0,0));
    AluSupportBox = new G4SubtractionSolid("AluSupportBox-CutOut2",AluSupportBox,CutOut2,NULL,G4ThreeVector(-Support_box_x/2,0,0));

    //remove CutOut3
    AluSupportBox = new G4SubtractionSolid("AluSupportBox-CutOut2",AluSupportBox,CutOut3,NULL,G4ThreeVector(0,Support_box_y/2,0));
    AluSupportBox = new G4SubtractionSolid("AluSupportBox-CutOut2",AluSupportBox,CutOut3,NULL,G4ThreeVector(0,-Support_box_y/2,0));
  
    //remove Center
    AluSupportBox = new G4SubtractionSolid("AluSupportBox-CutOut_Center",AluSupportBox,CutOut_Center,NULL,G4ThreeVector(0,0,0));
    AluSupportLog_ = new G4LogicalVolume(AluSupportBox, G4_Al_, "AluSupportLog_");

    G4VisAttributes* VisAtt_Alu_Support = new G4VisAttributes(true, G4Color(0.5, 0.5, 0.5, 1));
    VisAtt_Alu_Support->SetForceSolid(true);
    AluSupportLog_->SetVisAttributes(VisAtt_Alu_Support);

    return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool MiniPAN_Jun2022_Instrument::ConstructMiniPAN_OuterAluFrame(){

  //The Support is characterized as a box with subtracted volumes (i.e. "CutOut")
  //CutOut1
  G4Box* CutOut1_Box  = new G4Box("CutOut1_Box",Frame_CutOut1_box_x/2,Frame_CutOut1_box_y/2,Frame_CutOut1_box_z);

  //CutOut2
  G4Box* CutOut2_Box  = new G4Box("CutOut2_Box",Frame_CutOut2_box_x/2,Frame_CutOut2_box_y/2,Frame_CutOut2_box_z);

  //CutOut3
  G4Box* CutOut3_Box  = new G4Box("CutOut3_Box",Frame_CutOut3_box_x/2,Frame_CutOut3_box_y/2,Frame_CutOut3_box_z);

  //CutOut4
  G4Box* CutOut4_Box  = new G4Box("CutOut4_Box",Frame_CutOut4_box_x/2,Frame_CutOut4_box_y/2,Frame_CutOut4_box_z);

  //CutOut5
  G4Box* CutOut5_Box  = new G4Box("CutOut5_Box",Frame_CutOut5_box_x/2,Frame_CutOut5_box_y/2,Frame_CutOut5_box_z);
  
  //CutOut6
  G4Box* CutOut6_Box  = new G4Box("CutOut6_Box",Frame_CutOut6_box_x/2,Frame_CutOut6_box_y,Frame_CutOut6_box_z);

  //CutOut7
  G4Box* CutOut7_Box  = new G4Box("CutOut7_Box",Frame_CutOut7_box_x/2,Frame_CutOut7_box_y,Frame_CutOut7_box_z);

  //FrameBox
  G4Box* FrameBox  = new G4Box("OuterAluFrameBox",Frame_box_x/2,Frame_box_y/2,Frame_box_z/2); 

  //OuterAluFrameBox
  G4VSolid* OuterAluFrameBox = new G4SubtractionSolid("FrameBox-CutOut3_Box",FrameBox,CutOut3_Box,NULL,G4ThreeVector(0,0,0));

  for(int iSide=0;iSide<2;iSide++){
    if(fPANGlobalConfig->visual_light) break;
    G4double y_pos_CutOut = Frame_box_y/2-Frame_CutOut2_box_y/2-Frame_CoutOut12_offset_y[0];
    OuterAluFrameBox  = new G4SubtractionSolid("OuterAluFrameBox-CutOut1_Box",OuterAluFrameBox,CutOut2_Box,NULL,G4ThreeVector((iSide == 0 ? 1 : -1)*(Frame_box_x/2-Frame_CutOut2_box_x/2-Frame_CoutOut01_offset_x),(iSide == 0 ? 1 : -1)*(y_pos_CutOut),0));
    y_pos_CutOut = y_pos_CutOut - Frame_CutOut2_box_y - Frame_CoutOut12_offset_y[1];
    OuterAluFrameBox  = new G4SubtractionSolid("OuterAluFrameBox-CutOut1_Box",OuterAluFrameBox,CutOut2_Box,NULL,G4ThreeVector((iSide == 0 ? 1 : -1)*(Frame_box_x/2-Frame_CutOut2_box_x/2-Frame_CoutOut01_offset_x),(iSide == 0 ? 1 : -1)*(y_pos_CutOut),0));
    y_pos_CutOut = y_pos_CutOut - Frame_CutOut2_box_y/2 - Frame_CutOut1_box_y/2 - Frame_CoutOut12_offset_y[1];
    OuterAluFrameBox  = new G4SubtractionSolid("OuterAluFrameBox-CutOut1_Box",OuterAluFrameBox,CutOut1_Box,NULL,G4ThreeVector((iSide == 0 ? 1 : -1)*(Frame_box_x/2-Frame_CutOut1_box_x/2-Frame_CoutOut01_offset_x),(iSide == 0 ? 1 : -1)*(y_pos_CutOut),0));
  }

  //remove CutOut4
  OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut4_Box",OuterAluFrameBox,CutOut4_Box,NULL,G4ThreeVector(0,0,Frame_box_z/2));
  OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut4_Box",OuterAluFrameBox,CutOut4_Box,NULL,G4ThreeVector(0,0,-Frame_box_z/2-(Frame_CutOut4_box_z-StripPCB.PCB_Box_z)));

  //remove CutOut5
  G4RotationMatrix * rot_co5 = new G4RotationMatrix();
  rot_co5->rotateZ(90*deg);
  if(!fPANGlobalConfig->visual_light){
    OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut5_Box",OuterAluFrameBox,CutOut5_Box,NULL,G4ThreeVector(0,0,(Frame_box_z/2-Frame_CutOut4_box_z)));
    OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut5_Box",OuterAluFrameBox,CutOut5_Box,NULL,G4ThreeVector(0,0,-(Frame_box_z/2-StripPCB.PCB_Box_z)));
    OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut5_Box",OuterAluFrameBox,CutOut5_Box,rot_co5,G4ThreeVector(0,0,(Frame_box_z/2-Frame_CutOut4_box_z)));
    OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut5_Box",OuterAluFrameBox,CutOut5_Box,rot_co5,G4ThreeVector(0,0,-(Frame_box_z/2-StripPCB.PCB_Box_z)));
  }

  //remove CutOut6
  OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut6_Box",OuterAluFrameBox,CutOut6_Box,NULL,G4ThreeVector(0,Frame_box_y/2,0));
  OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut6_Box",OuterAluFrameBox,CutOut6_Box,NULL,G4ThreeVector(0,-Frame_box_y/2,0));

  //remove CutOut7
  OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut7_Box",OuterAluFrameBox,CutOut7_Box,NULL,G4ThreeVector(0,-Frame_box_y/2,Frame_box_z/2));
  OuterAluFrameBox = new G4SubtractionSolid("OuterAluFrameBox-CutOut7_Box",OuterAluFrameBox,CutOut7_Box,NULL,G4ThreeVector(0,Frame_box_y/2,-Frame_box_z/2));

  //remove 
  OuterAluFrameLog_ = new G4LogicalVolume(OuterAluFrameBox, G4_Al_, "OuterAluFrameLog_");

  G4VisAttributes* VisAtt_Alu_Frame = new G4VisAttributes(true, G4Color(0.6, 0.6, 0.6, 1));
  VisAtt_Alu_Frame->SetForceSolid(true);
  OuterAluFrameLog_->SetVisAttributes(VisAtt_Alu_Frame);

  return true;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

G4bool MiniPAN_Jun2022_Instrument::ConstructMiniPAN_CenterAluFrame(){

  //FrameBox
  G4Box* FrameBox  = new G4Box("CenterAluFrameBox",Frame_box_x/2,Frame_box_y/2,CenterFrame_box_z/2); 

  //CutOut1
  G4Box* CutOut1_Box  = new G4Box("CutOut1_Box",CenterFrame_CutOut1_box_x/2,CenterFrame_CutOut1_box_y/2,CenterFrame_CutOut1_box_z);

  //CutOut2
  G4Box* CutOut2_Box  = new G4Box("CutOut2_Box",CenterFrame_CutOut2_box_x/2,CenterFrame_CutOut2_box_y,CenterFrame_CutOut2_box_z/2);

  //CutOut3 (duplicate geometry from OuterAluFrame())
  G4Box* CutOut3_Box  = new G4Box("CutOut3_Box",Frame_CutOut3_box_x/2,Frame_CutOut3_box_y/2,Frame_CutOut3_box_z);

  //CutOut4 (duplicate geometry from OuterAluFrame())
  G4Box* CutOut4_Box1  = new G4Box("CutOut4_Box1",Frame_CutOut4_box_x/2,Frame_CutOut4_box_y/2,Frame_CutOut4_box_z);
  G4Box* CutOut4_Box2  = new G4Box("CutOut4_Box2",Frame_CutOut4_box_x/2,Frame_CutOut4_box_y/2,StripPCB.PCB_Box_z);

  //CutOut5 (duplicate geometry from OuterAluFrame())
  G4Box* CutOut5_Box  = new G4Box("CutOut5_Box",Frame_CutOut5_box_x/2,Frame_CutOut5_box_y/2,Frame_CutOut5_box_z);

  //CutOut6 (duplicate geometry from OuterAluFrame())
  G4Box* CutOut6_Box  = new G4Box("CutOut6_Box",Frame_CutOut6_box_x/2,Frame_CutOut6_box_y,Frame_CutOut6_box_z);

  //CutOut7 (duplicate geometry from OuterAluFrame())
  G4Box* CutOut7_Box1  = new G4Box("CutOut7_Box1",CenterFrame_CutOut7_box_x/2,CenterFrame_CutOut7_box_y,CenterFrame_CutOut7_box_z);
  G4Box* CutOut7_Box2  = new G4Box("CutOut7_Box2",CenterFrame_CutOut7_box_x/2,CenterFrame_CutOut7_box_y,Frame_CutOut4_box_z);

  //CutOut8
  G4Box* CutOut8_Box  = new G4Box("CutOut8_Box",CenterFrame_CutOut8_box_x/2,CenterFrame_CutOut8_box_y/2,CenterFrame_CutOut8_box_z);

  //CutOut9
  G4Box* CutOut9_Box  = new G4Box("CutOut9_Box",CenterFrame_CutOut9_box_x/2,CenterFrame_CutOut9_box_y/2,CenterFrame_CutOut9_box_z);

  //CenterAluFrameBox
  G4VSolid* CenterAluFrameBox = FrameBox;

  //remove CutOut1
  for(int iCutOut =0; iCutOut < CenterFrame_nCutOut1; iCutOut++){
    if(fPANGlobalConfig->visual_light) continue;
    G4double xpos = (iCutOut / 2 == 0 ? 1 : -1 )*(-Frame_box_x/2 + CenterFrame_CutOut1_OffsetX + CenterFrame_CutOut1_box_x/2);
    G4double ypos = (iCutOut % 2 == 0 ? 1 : -1 )*(-Frame_box_y/2 + CenterFrame_CutOut1_OffsetY[0] + CenterFrame_CutOut1_box_y/2);
    CenterAluFrameBox = new G4SubtractionSolid("FrameBox-CutOut1_Box",CenterAluFrameBox,CutOut1_Box,NULL,G4ThreeVector(xpos,ypos,0));
  }

  //remove CutOut2
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut2_Box",CenterAluFrameBox,CutOut2_Box,NULL,G4ThreeVector(0,-Frame_CutOut3_box_y/2,0));

  //remove CutOut3
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut3_Box",CenterAluFrameBox,CutOut3_Box,NULL,G4ThreeVector(0,0,0));

  //remove CutOut4
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut4_Box1",CenterAluFrameBox,CutOut4_Box1,NULL,G4ThreeVector(0,0,+CenterFrame_box_z/2));
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut4_Box2",CenterAluFrameBox,CutOut4_Box2,NULL,G4ThreeVector(0,0,-CenterFrame_box_z/2));

  //remove CutOut5
  G4RotationMatrix * rot_co5 = new G4RotationMatrix();
  rot_co5->rotateZ(90*deg);
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut5_Box",CenterAluFrameBox,CutOut5_Box,NULL,G4ThreeVector(0,0,-CenterFrame_box_z/2+Frame_CutOut4_box_z/2));
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut5_Box",CenterAluFrameBox,CutOut5_Box,rot_co5,G4ThreeVector(0,0,-CenterFrame_box_z/2+Frame_CutOut4_box_z/2));

  //remove CutOut6
  CenterAluFrameBox = new G4SubtractionSolid("CenterluFrameBox-CutOut6_Box",CenterAluFrameBox,CutOut6_Box,NULL,G4ThreeVector(0,Frame_box_y/2,0));
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut6_Box",CenterAluFrameBox,CutOut6_Box,NULL,G4ThreeVector(0,-Frame_box_y/2,0));

  //remove CutOut7
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut7_Box1",CenterAluFrameBox,CutOut7_Box1,NULL,G4ThreeVector(0,+Frame_box_y/2,-CenterFrame_box_z/2));
  CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut7_Box2",CenterAluFrameBox,CutOut7_Box2,NULL,G4ThreeVector(0,-Frame_box_y/2,+CenterFrame_box_z/2));
  
  if(!fPANGlobalConfig->visual_light){
    //remove CutOut8
    CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut8_Box",CenterAluFrameBox,CutOut8_Box,NULL,G4ThreeVector(CenterFrame_CutOut8_OffsetX,-Frame_CutOut3_box_y/2-CenterFrame_CutOut2_box_y-CenterFrame_CutOut8_box_y/2,CenterFrame_box_z/2-Frame_CutOut4_box_z));
  
    //remove CutOut9
    CenterAluFrameBox = new G4SubtractionSolid("CenterAluFrameBox-CutOut9_Box",CenterAluFrameBox,CutOut9_Box,NULL,G4ThreeVector(Frame_box_x/2-CenterFrame_CutOut9_OffsetX-CenterFrame_CutOut9_box_x/2,-Frame_box_y/2+CenterFrame_CutOut9_OffsetY+CenterFrame_CutOut9_box_y/2,0));
  }
  
  CenterAluFrameLog_ = new G4LogicalVolume(CenterAluFrameBox, G4_Al_, "CenterAluFrameLog_");

  G4VisAttributes* VisAtt_Alu_Frame = new G4VisAttributes(true, G4Color(0.6, 0.6, 0.6, 1));
  VisAtt_Alu_Frame->SetForceSolid(true);
  CenterAluFrameLog_->SetVisAttributes(VisAtt_Alu_Frame);

  return true;
}