#include "PANDetectorConstruction.hh"
#include "PANActionInitialization.hh"

#include "TString.h"

PANActionInitialization::PANActionInitialization(G4String the_output_file, G4bool fixed_name) {
    fPrimaryGeneratorAction_ = new PANPrimaryGeneratorAction();

    output_file_       = the_output_file;
    fixed_filename_    = fixed_name;

    fPANSDConfig     = PANSDConfig::Instance();
    fPANGlobalConfig = PANGlobalConfig::Instance();

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

PANActionInitialization::~PANActionInitialization() {
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

PANPrimaryGeneratorAction* PANActionInitialization::GetPrimaryGeneratorAction() {
    return fPrimaryGeneratorAction_;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANActionInitialization::Build() const {
    //primary generator action
    SetUserAction(fPrimaryGeneratorAction_);

    //run action
    PANRunAction* runAction = new PANRunAction(output_file_, fixed_filename_);
    SetUserAction(runAction);

    //event action
    PANEventAction* eventAction = new PANEventAction();
    SetUserAction(eventAction);


    // prepare file
    G4AnalysisManager* fAnalysisManager = G4AnalysisManager::Instance();
    if (G4RunManager::GetRunManager()->GetRunManagerType() == G4RunManager::RMType::sequentialRM) {

      fAnalysisManager->CreateNtuple("Keyword", "Keyword");
      fAnalysisManager->CreateNtupleIColumn("RunID");                      // 0
      fAnalysisManager->CreateNtupleIColumn("NumberOfEvents");             // 1

      fAnalysisManager->FinishNtuple();

      fAnalysisManager->CreateNtuple("Configuration", "Configuration");
      // necessary
      fAnalysisManager->CreateNtupleIColumn("IsSim");                         // 0
      fAnalysisManager->CreateNtupleIColumn("IsSimMore");                     // 1
      if(fPANGlobalConfig->IsMiniPAN){Add_MiniPAN_Config(fAnalysisManager);}
      
      fAnalysisManager->FinishNtuple();

    }

    fAnalysisManager->CreateNtuple("Primary", "Primary");
    // necessary
    fAnalysisManager->CreateNtupleIColumn("EventID");                       // 0
    fAnalysisManager->CreateNtupleSColumn("PName_Primary");                 // 1
    fAnalysisManager->CreateNtupleDColumn("X_Primary");                     // 2
    fAnalysisManager->CreateNtupleDColumn("Y_Primary");                     // 3
    fAnalysisManager->CreateNtupleDColumn("Z_Primary");                     // 4
    fAnalysisManager->CreateNtupleDColumn("Energy_Primary");                // 5
    fAnalysisManager->CreateNtupleDColumn("Theta_Primary");                 // 6
    fAnalysisManager->CreateNtupleDColumn("Phi_Primary");                   // 7
    // optional
    if (fPANGlobalConfig->simout_more) {
        fAnalysisManager->CreateNtupleDColumn("Px_Primary");                // 1.0
        fAnalysisManager->CreateNtupleDColumn("Py_Primary");                // 2.0
        fAnalysisManager->CreateNtupleDColumn("Pz_Primary");                // 3.0
    }

    if(fPANGlobalConfig->IsMiniPAN){Add_MiniPAN_PrimaryTree(fAnalysisManager);}
    
    fAnalysisManager->FinishNtuple();

    if(fPANGlobalConfig->IsMiniPAN){Create_MiniPAN_Tree(fAnalysisManager);}
    
    if(fPANGlobalConfig->WriteFluxInside){
      G4int nLayers = 0;
      const PANDetectorConstruction* detectorConstruction = static_cast<const PANDetectorConstruction*> (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
      if(fPANGlobalConfig->IsMiniPAN) {nLayers = detectorConstruction->GetMiniPAN().GetnStripX();}
      for(int iLayer=0;iLayer<nLayers;iLayer++){
        fAnalysisManager->CreateH1(Form("hSpectrum_Electron_%d",iLayer),Form("hSpectrum_Electron_%d",iLayer),1000,0,250*MeV,"MeV");
        fAnalysisManager->CreateH1(Form("hSpectrum_Proton_%d",iLayer),Form("hSpectrum_Proton_%d",iLayer),1000,0,250*MeV,"MeV");
        fAnalysisManager->CreateH1(Form("hSpectrum_Photon_%d",iLayer),Form("hSpectrum_Photon_%d",iLayer),1000,0,250*MeV,"MeV");
        fAnalysisManager->CreateH1(Form("hSpectrum_Total_%d",iLayer),Form("hSpectrum_Rest_%d",iLayer),1000,0,250*MeV,"MeV");
      }
    }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANActionInitialization::BuildForMaster() const {

    SetUserAction(new PANRunAction(output_file_, fixed_filename_));

    G4AnalysisManager* fAnalysisManager = G4AnalysisManager::Instance();
    fAnalysisManager->SetNtupleMerging(true);

    fAnalysisManager->CreateNtuple("Keyword", "Keyword");
    fAnalysisManager->CreateNtupleIColumn("RunID");                      // 0
    fAnalysisManager->CreateNtupleIColumn("NumberOfEvents");             // 1
    
    fAnalysisManager->FinishNtuple();

    fAnalysisManager->CreateNtuple("Configuration", "Configuration");
    // necessary
    fAnalysisManager->CreateNtupleIColumn("IsSim");                     // 0
    fAnalysisManager->CreateNtupleIColumn("IsSimMore");                 // 1
    if(fPANGlobalConfig->IsMiniPAN){Add_MiniPAN_Config(fAnalysisManager);}
    
    fAnalysisManager->FinishNtuple();
    
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANActionInitialization::Add_MiniPAN_Config(G4AnalysisManager* fAnalysisManager) const {
    fAnalysisManager->CreateNtupleIColumn("Add_SiStripX");                 // 2
    fAnalysisManager->CreateNtupleIColumn("Add_SiStripY");                 // 3
    fAnalysisManager->CreateNtupleIColumn("Add_PixSensor");                // 4
    fAnalysisManager->CreateNtupleIColumn("Add_PixAsic");                  // 5
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANActionInitialization::Add_MiniPAN_PrimaryTree(G4AnalysisManager* fAnalysisManager) const {

    if(fPANSDConfig->AddSD_SiStripX){
      fAnalysisManager->CreateNtupleIColumn("HitsStripXCount");         // 1.2
      fAnalysisManager->CreateNtupleDColumn("pDose_StripX_0");           // 2.2
      fAnalysisManager->CreateNtupleDColumn("pDose_StripX_1");           // 3.2
      fAnalysisManager->CreateNtupleDColumn("pDose_StripX_2");           // 4.2
      fAnalysisManager->CreateNtupleDColumn("pDose_StripX_3");           // 5.2
      fAnalysisManager->CreateNtupleDColumn("pDose_StripX_4");           // 6.2
      fAnalysisManager->CreateNtupleDColumn("pDose_StripX_5");           // 7.2
      fAnalysisManager->CreateNtupleDColumn("eDose_StripX_0");           // 8.2
      fAnalysisManager->CreateNtupleDColumn("eDose_StripX_1");           // 9.2
      fAnalysisManager->CreateNtupleDColumn("eDose_StripX_2");           // 10.2
      fAnalysisManager->CreateNtupleDColumn("eDose_StripX_3");           // 11.2
      fAnalysisManager->CreateNtupleDColumn("eDose_StripX_4");           // 12.2
      fAnalysisManager->CreateNtupleDColumn("eDose_StripX_5");           // 13.2
    }

    if(fPANSDConfig->AddSD_SiStripY){
      fAnalysisManager->CreateNtupleIColumn("HitsStripYCount");          // 1.3
      fAnalysisManager->CreateNtupleDColumn("pDose_StripY_0");           // 2.3
      fAnalysisManager->CreateNtupleDColumn("pDose_StripY_1");           // 3.3
      fAnalysisManager->CreateNtupleDColumn("pDose_StripY_2");           // 4.3
      fAnalysisManager->CreateNtupleDColumn("eDose_StripY_0");           // 5.3
      fAnalysisManager->CreateNtupleDColumn("eDose_StripY_1");           // 6.3
      fAnalysisManager->CreateNtupleDColumn("eDose_StripY_2");           // 7.3
    }

    if(fPANSDConfig->AddSD_PixSensor){
      fAnalysisManager->CreateNtupleIColumn("HitsPixSensorCount");       // 1.4
      fAnalysisManager->CreateNtupleDColumn("pDose_PixSensor_0");        // 2.4
      fAnalysisManager->CreateNtupleDColumn("pDose_PixSensor_1");        // 3.4
      fAnalysisManager->CreateNtupleDColumn("eDose_PixSensor_0");        // 4.4
      fAnalysisManager->CreateNtupleDColumn("eDose_PixSensor_1");        // 5.4
    }
    if(fPANSDConfig->AddSD_PixAsic){
      fAnalysisManager->CreateNtupleIColumn("HitsPixAsicCount");         // 1.5
      fAnalysisManager->CreateNtupleDColumn("pDose_PixAsic_0");          // 2.5
      fAnalysisManager->CreateNtupleDColumn("pDose_PixAsic_1");          // 3.5
      fAnalysisManager->CreateNtupleDColumn("eDose_PixAsic_0");          // 4.5
      fAnalysisManager->CreateNtupleDColumn("eDose_PixAsic_1");          // 5.5
    }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANActionInitialization::Create_MiniPAN_Tree(G4AnalysisManager* fAnalysisManager) const {


    if(fPANSDConfig->AddSD_SiStripX){
      fAnalysisManager->CreateNtuple("HitsCol_StripX", "HitsCollection_StripX");
      // necessary
      fAnalysisManager->CreateNtupleIColumn("EventID");                       // 0
      fAnalysisManager->CreateNtupleIColumn("TrackID");                       // 1
      fAnalysisManager->CreateNtupleIColumn("CopyID");                        // 2
      fAnalysisManager->CreateNtupleIColumn("StripID");                       // 3
      fAnalysisManager->CreateNtupleSColumn("ParticleName");                  // 4
      fAnalysisManager->CreateNtupleDColumn("KinEnergy");                     // 5
      fAnalysisManager->CreateNtupleDColumn("EnergyDep");                     // 6
      fAnalysisManager->CreateNtupleIColumn("pdgID");                         // 7
      fAnalysisManager->CreateNtupleIColumn("parentID");                      // 8
      if(fPANGlobalConfig->simout_more){
        fAnalysisManager->CreateNtupleSColumn("VolName");                     // 9
        fAnalysisManager->CreateNtupleDColumn("GlobalTime");                  // 10
        fAnalysisManager->CreateNtupleIColumn("StepStatus");                  // 11
        fAnalysisManager->CreateNtupleSColumn("ProcessName");                 // 12
        fAnalysisManager->CreateNtupleDColumn("DeltaTime");                   // 13
        fAnalysisManager->CreateNtupleDColumn("LocPosX");                     // 14
        fAnalysisManager->CreateNtupleDColumn("LocPosY");                     // 15
        fAnalysisManager->CreateNtupleDColumn("LocPosZ");                     // 16
        fAnalysisManager->CreateNtupleDColumn("WorldPosX");                   // 17
        fAnalysisManager->CreateNtupleDColumn("WorldPosY");                   // 18
        fAnalysisManager->CreateNtupleDColumn("WorldPosZ");                   // 19
        fAnalysisManager->CreateNtupleDColumn("Px");                          // 20
        fAnalysisManager->CreateNtupleDColumn("Py");                          // 21
        fAnalysisManager->CreateNtupleDColumn("Pz");                          // 22
      }
      fAnalysisManager->FinishNtuple();
    }

    if(fPANSDConfig->AddSD_SiStripY){
      fAnalysisManager->CreateNtuple("HitsCol_StripY", "HitsCollection_StripY");
      // necessary
      fAnalysisManager->CreateNtupleIColumn("EventID");                       // 0
      fAnalysisManager->CreateNtupleIColumn("TrackID");                       // 1
      fAnalysisManager->CreateNtupleIColumn("CopyID");                        // 2
      fAnalysisManager->CreateNtupleIColumn("StripID");                       // 3
      fAnalysisManager->CreateNtupleSColumn("ParticleName");                  // 4
      fAnalysisManager->CreateNtupleDColumn("KinEnergy");                     // 5
      fAnalysisManager->CreateNtupleDColumn("EnergyDep");                     // 6
      fAnalysisManager->CreateNtupleIColumn("pdgID");                         // 7
      fAnalysisManager->CreateNtupleIColumn("parentID");                      // 8
      if(fPANGlobalConfig->simout_more){
        fAnalysisManager->CreateNtupleSColumn("VolName");                     // 9
        fAnalysisManager->CreateNtupleDColumn("GlobalTime");                  // 10
        fAnalysisManager->CreateNtupleIColumn("StepStatus");                  // 11
        fAnalysisManager->CreateNtupleSColumn("ProcessName");                 // 12
        fAnalysisManager->CreateNtupleDColumn("DeltaTime");                   // 13
        fAnalysisManager->CreateNtupleDColumn("LocPosX");                     // 14
        fAnalysisManager->CreateNtupleDColumn("LocPosY");                     // 15
        fAnalysisManager->CreateNtupleDColumn("LocPosZ");                     // 16
        fAnalysisManager->CreateNtupleDColumn("WorldPosX");                   // 17
        fAnalysisManager->CreateNtupleDColumn("WorldPosY");                   // 18
        fAnalysisManager->CreateNtupleDColumn("WorldPosZ");                   // 19
        fAnalysisManager->CreateNtupleDColumn("Px");                          // 20
        fAnalysisManager->CreateNtupleDColumn("Py");                          // 21
        fAnalysisManager->CreateNtupleDColumn("Pz");                          // 22
      }
      fAnalysisManager->FinishNtuple();
    }

    if(fPANSDConfig->AddSD_PixSensor){
      fAnalysisManager->CreateNtuple("HitsCol_PixSensor", "HitsCollection_PixSensor");
      // necessary
      fAnalysisManager->CreateNtupleIColumn("EventID");                       // 0
      fAnalysisManager->CreateNtupleIColumn("TrackID");                       // 1
      fAnalysisManager->CreateNtupleIColumn("CopyID");                        // 2
      fAnalysisManager->CreateNtupleIColumn("PixelID");                       // 3
      fAnalysisManager->CreateNtupleSColumn("ParticleName");                  // 4
      fAnalysisManager->CreateNtupleDColumn("KinEnergy");                     // 5
      fAnalysisManager->CreateNtupleDColumn("EnergyDep");                     // 6
      fAnalysisManager->CreateNtupleIColumn("pdgID");                         // 7
      fAnalysisManager->CreateNtupleIColumn("parentID");                      // 8
      if(fPANGlobalConfig->simout_more){
        fAnalysisManager->CreateNtupleSColumn("VolName");                     // 9
        fAnalysisManager->CreateNtupleDColumn("GlobalTime");                  // 10
        fAnalysisManager->CreateNtupleIColumn("StepStatus");                  // 11
        fAnalysisManager->CreateNtupleSColumn("ProcessName");                 // 12
        fAnalysisManager->CreateNtupleDColumn("DeltaTime");                   // 13
        fAnalysisManager->CreateNtupleDColumn("LocPosX");                     // 14
        fAnalysisManager->CreateNtupleDColumn("LocPosY");                     // 15
        fAnalysisManager->CreateNtupleDColumn("LocPosZ");                     // 16
        fAnalysisManager->CreateNtupleDColumn("WorldPosX");                   // 17
        fAnalysisManager->CreateNtupleDColumn("WorldPosY");                   // 18
        fAnalysisManager->CreateNtupleDColumn("WorldPosZ");                   // 19
        fAnalysisManager->CreateNtupleDColumn("Px");                          // 20
        fAnalysisManager->CreateNtupleDColumn("Py");                          // 21
        fAnalysisManager->CreateNtupleDColumn("Pz");                          // 22
      }
      fAnalysisManager->FinishNtuple();

    }

    if(fPANSDConfig->AddSD_PixAsic){
      fAnalysisManager->CreateNtuple("HitsCol_PixAsic", "HitsCollection_PixAsic");
      // necessary
      fAnalysisManager->CreateNtupleIColumn("EventID");                       // 0
      fAnalysisManager->CreateNtupleIColumn("TrackID");                       // 1
      fAnalysisManager->CreateNtupleIColumn("CopyID");                        // 2
      fAnalysisManager->CreateNtupleSColumn("ParticleName");                  // 3
      fAnalysisManager->CreateNtupleDColumn("KinEnergy");                     // 4
      fAnalysisManager->CreateNtupleDColumn("EnergyDep");                     // 5
      fAnalysisManager->CreateNtupleIColumn("pdgID");                         // 6
      fAnalysisManager->CreateNtupleIColumn("parentID");                      // 7
      if(fPANGlobalConfig->simout_more){
        fAnalysisManager->CreateNtupleSColumn("VolName");                       // 8
        fAnalysisManager->CreateNtupleDColumn("GlobalTime");                    // 9
        fAnalysisManager->CreateNtupleIColumn("StepStatus");                    // 10
        fAnalysisManager->CreateNtupleSColumn("ProcessName");                   // 11
        fAnalysisManager->CreateNtupleDColumn("DeltaTime");                     // 12
        fAnalysisManager->CreateNtupleDColumn("LocPosX");                       // 13
        fAnalysisManager->CreateNtupleDColumn("LocPosY");                       // 14
        fAnalysisManager->CreateNtupleDColumn("LocPosZ");                       // 15
        fAnalysisManager->CreateNtupleDColumn("Px");                            // 16
        fAnalysisManager->CreateNtupleDColumn("Py");                            // 17
        fAnalysisManager->CreateNtupleDColumn("Pz");                            // 18
      }
      fAnalysisManager->FinishNtuple();

    }

}