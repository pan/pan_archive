#include "PANRunAction.hh"
#include "PANRun.hh"
#include "PANDetectorConstruction.hh"

#include "G4RunManager.hh"
#include "Randomize.hh"
#include "G4AnalysisManager.hh"
#include "Randomize.hh"
#include <sstream>

PANRunAction::PANRunAction(G4String the_output_file, G4bool fixed_name) {
    output_file_  = the_output_file;
    fixed_output_ = fixed_name;

    fPANSDConfig     = PANSDConfig::Instance();
    fPANGlobalConfig = PANGlobalConfig::Instance();

    fRun = NULL;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PANRunAction::~PANRunAction() {
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Run* PANRunAction::GenerateRun() {
    fRun = new PANRun();
    return fRun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int PANRunAction::randomSeed_ = 0;
G4int PANRunAction::runId_ = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PANRunAction::BeginOfRunAction(const G4Run*) {
    G4AnalysisManager* fAnalysisManager = G4AnalysisManager::Instance();

    if (IsMaster()) {
        randomSeed_ = G4Random::getTheSeed();
        runId_      = fRun->GetRunID();
    }
    G4String filename(output_file_);
    // append extra info to filename
    if (!fixed_output_) {
        G4String extension;
        if (filename.find(".") != G4String::npos) {
            extension = filename.substr(filename.find("."));
            filename = filename.substr(0, filename.find("."));
        } else {
            extension = "";
        }
        std::ostringstream os;
        os << "_s" << randomSeed_ << "_r" << runId_;
        filename.append(os.str());
        filename.append(extension);
    }
    // open file
    if (fPANGlobalConfig->output_directory.size() > 0) {
        filename.insert(0,"/");
        filename.insert(0,fPANGlobalConfig->output_directory.c_str());
    }
    fAnalysisManager->OpenFile(filename);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PANRunAction::EndOfRunAction(const G4Run*) {
    

    if (IsMaster()) {fRun->EndOfRun();}
 
    G4AnalysisManager* fAnalysisManager = G4AnalysisManager::Instance();
    fAnalysisManager->Write();
    fAnalysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int PANRunAction::GetTreeID(G4String stree) const {

  return fPANSDConfig->GetTreeID().at(stree);

}

