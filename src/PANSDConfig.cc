#include "PANSDConfig.hh"
#include "PANSDMessenger.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4RunManager.hh"

#include "TString.h"

PANSDConfig* PANSDConfig::fSDinstance_ = NULL;

PANSDConfig* PANSDConfig::Instance()
{
  // A new instance of AnalysisManager is created if it does not exist:
  if (fSDinstance_ == NULL) {fSDinstance_ = new PANSDConfig();}
  
  return fSDinstance_;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PANSDConfig::Destroy()
{
  // The AnalysisManager instance is deleted if it exists:
  if (fSDinstance_ != NULL) 
  { 
    delete fSDinstance_;
    fSDinstance_ = NULL;
  }    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PANSDConfig::PANSDConfig() 
{
    fPANSDMessenger_ = new PANSDMessenger(this);
    fSDinstance_ = this;
   
    fPANGlobalConfig = PANGlobalConfig::Instance();

    //defaults for Strip
    AddSD_SiStripX  = false;
    AddSD_SiStripY  = false;

    //defaults for Pixel
    AddSD_PixSensor   = false;
    AddSD_PixAsic     = false;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PANSDConfig::~PANSDConfig() {
    delete fPANSDMessenger_;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PANSDConfig::GenerateTreeKeys(G4bool isMT) {

  G4int key_id  = 0;

  if(!isMT){
    treeID_.emplace("Keyword",key_id);key_id++;
    treeID_.emplace("Configuration",key_id);key_id++;
  }
  treeID_.emplace("Primary",key_id);key_id++;

  if(fPANGlobalConfig->IsMiniPAN){
    //Strip
    if(AddSD_SiStripX || fPANGlobalConfig->WriteFluxInside) {treeID_.emplace("HitsCol_StripX",key_id);key_id++;}
    if(AddSD_SiStripY || fPANGlobalConfig->WriteFluxInside) {treeID_.emplace("HitsCol_StripY",key_id);key_id++;}

    //Pixel
    if(AddSD_PixSensor) {treeID_.emplace("HitsCol_PixSensor",key_id);key_id++;}
    if(AddSD_PixAsic) {treeID_.emplace("HitsCol_PixAsic",key_id);key_id++;}
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PANSDConfig::print_config() {
    G4cout << "=============================================================" << G4endl;
    G4cout << "============= Sensitive Detectors Configuration =============" << G4endl;
    G4cout << "=============================================================" << G4endl;
    G4cout << " - Include StripX as SD                                     = " << IsSD(AddSD_SiStripX)               << G4endl;
    G4cout << " - Include StripY as SD                                     = " << IsSD(AddSD_SiStripY)               << G4endl;
    G4cout << " - Include TimePix Sensor as SD                             = " << IsSD(AddSD_PixSensor)              << G4endl;
    G4cout << " - Include TimePix Asic as SD                               = " << IsSD(AddSD_PixAsic)                << G4endl;
    G4cout << "=============================================================" << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4String PANSDConfig::IsSD(G4bool status){
  if(status){return "true";}
  else{return "false";}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
