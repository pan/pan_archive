#include "G4SDManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4RunManager.hh"

#include "PANDetectorConstruction.hh"
#include "PANRun.hh"

#include "TString.h"


PANRun::PANRun() {
    totalEvents_         = 0;
    totalElectronEvents_ = 0;
    totalProtonEvents_   = 0;

    fPANGlobalConfig = PANGlobalConfig::Instance();
    fPANSDConfig     = PANSDConfig::Instance();

    fPrimaryGeneratorAction_ = new PANPrimaryGeneratorAction();

    if(fPANGlobalConfig->IsMiniPAN){MiniPAN_Jun2022 = new MiniPANRun_Jun2022();}

    //default writes
    write_MiniPAN_stripX       = false;
    write_MiniPAN_stripY       = false;

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

PANRun::~PANRun() {
  if(fPANGlobalConfig->IsMiniPAN){delete MiniPAN_Jun2022;}
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANRun::RecordEvent(const G4Event* anEvent) {
    G4PrimaryVertex* primaryVertex = anEvent->GetPrimaryVertex(0);
    if (primaryVertex == NULL){return;}

    G4Run::RecordEvent(anEvent);

    // calculate and record value
    G4int theEventID = anEvent->GetEventID();
    std::vector<G4String> sPANSDHitCollection;
    std::vector<PANHitsCollection*> fPANHitsCollection;
    std::vector<G4int> numberOfHits;

    std::vector<G4String> sTotalNumberOfHits;
    std::vector<G4int>    TotalNumberOfHits;

    const PANDetectorConstruction* detectorConstruction = static_cast<const PANDetectorConstruction*>
                (G4RunManager::GetRunManager()->GetUserDetectorConstruction());

    G4AnalysisManager* fAnalysisManager = G4AnalysisManager::Instance();

    //compute the dose to write
    
    if (!fPANGlobalConfig->primary_only){
      if (fPANGlobalConfig->event_verbose > 1)
        {G4cout << "---> (Record Event) Begin of event: " << theEventID << G4endl;}

      ///// MiniPAN /////
      if(fPANGlobalConfig->IsMiniPAN){
        MiniPAN_Jun2022->RecordEvent(anEvent, detectorConstruction);
        MiniPAN_Jun2022->WriteData(primaryVertex,fAnalysisManager, detectorConstruction, theEventID);
      }

      if (fPANGlobalConfig->event_verbose > 1)
          G4cout << "---> (Record Event) End of event: " << theEventID << G4endl;
    }

    //write primary
    WriteData_Primary(primaryVertex, detectorConstruction, anEvent, fAnalysisManager, sTotalNumberOfHits, TotalNumberOfHits);
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANRun::Merge(const G4Run* aRun) {
    G4Run::Merge(aRun);

    if(fPANGlobalConfig->IsMiniPAN){
      MiniPAN_Jun2022->Merge(aRun);

      totalEvents_         = MiniPAN_Jun2022->GetTotalEvents();
      totalElectronEvents_ = MiniPAN_Jun2022->GetTotalElectronEvents();
      totalProtonEvents_   = MiniPAN_Jun2022->GetTotalProtonEvents();
    } 
 
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANRun::WriteData_Primary(G4PrimaryVertex* primaryVertex, const PANDetectorConstruction* detectorConstruction, const G4Event* anEvent, G4AnalysisManager* fAnalysisManager, std::vector<G4String> sTotalNumberOfHits, std::vector<G4int> TotalNumberOfHits){

  // read primary
  if (anEvent->GetNumberOfPrimaryVertex() > 1) {
    G4ExceptionDescription msg;
    msg << "More than one primary particle for one event.";
    G4Exception("PANEventAction::EndOfEventAction", "MyCode0002", FatalException, msg);
  }

  G4PrimaryParticle* primaryParticle = primaryVertex->GetPrimary(0);
  G4String        pname_primary   = primaryParticle->GetParticleDefinition()->GetParticleName();
  G4ThreeVector   xyz_primary     = primaryVertex->GetPosition();
  G4double        ene_primary     = primaryParticle->GetKineticEnergy();
  G4ThreeVector   direction       = primaryParticle->GetMomentumDirection();
  G4double        theta_primary   = (-direction).theta();
  G4double        phi_primary     = (-direction).phi();
  G4ThreeVector   momentum        = primaryParticle->GetMomentum();

  //count particles
  int iPart = -1;
  if(pname_primary == "proton") {totalProtonEvents_++; iPart = 0;}
  if(pname_primary == "e-") {totalElectronEvents_++;iPart = 1;}
  totalEvents_++;

  ////////////////////////
  //only write primaries with useful information
  ////////////////////////
  bool write_primary = false;
  if(fPANGlobalConfig->IsMiniPAN){write_primary = MiniPAN_Jun2022->WritePrimary(iPart);}
  if(!write_primary && !fPANGlobalConfig->simout_more) {return;} //don't write primary if no dose was written

  ////////////////////////

  if (phi_primary < 0) {phi_primary += twopi;}

  // fill ntuple
  // necessary
  G4int addTupleCol = 0;
  G4int theEventID = anEvent->GetEventID();
  fAnalysisManager->FillNtupleIColumn(GetTreeID("Primary"), 0,  theEventID              );
  fAnalysisManager->FillNtupleSColumn(GetTreeID("Primary"), 1,  pname_primary           );  
  fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 2,  xyz_primary.x() / cm    );
  fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 3,  xyz_primary.y() / cm    );                                
  fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 4,  xyz_primary.z() / cm    );
  fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 5,  ene_primary / keV       );
  fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 6,  theta_primary / deg     );
  fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 7,  phi_primary / deg       );

  // optional
  if (fPANGlobalConfig->simout_more) {
    fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 8 ,  momentum.x() / keV );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 9 ,  momentum.y() / keV );
    fAnalysisManager->FillNtupleDColumn(GetTreeID("Primary"), 10,  momentum.z() / keV );
    addTupleCol += 3;
  }

  if(fPANGlobalConfig->IsMiniPAN){
    MiniPAN_Jun2022->WritePrimaryDose(fAnalysisManager,detectorConstruction,addTupleCol);
  }

  fAnalysisManager->AddNtupleRow(GetTreeID("Primary"));

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANRun::WriteData_FluxInside(PANHitsCollection* i_PANHitsCollection, G4AnalysisManager* fAnalysisManager, G4int i_numberOfHits){

  // save hits
  for (int i = 0; i < i_numberOfHits; i++) {
    PANHit* aHit = static_cast<PANHit*>(i_PANHitsCollection->GetHit(i));
    if(aHit->StepStatus != 1) continue;  //Particle MUST be entering

    if(aHit->ParticleName == "e+" || aHit->ParticleName == "e-"){
      fAnalysisManager->FillH1(GetHistID(Form("Electron_%d",aHit->CopyID)),aHit->KinEnergy);
    } else if (aHit->ParticleName == "proton") {
      fAnalysisManager->FillH1(GetHistID(Form("Proton_%d",aHit->CopyID)),aHit->KinEnergy);
    } else if (aHit->ParticleName == "gamma") {
      fAnalysisManager->FillH1(GetHistID(Form("Gamma_%d",aHit->CopyID)),aHit->KinEnergy);
    }
    
    fAnalysisManager->FillH1(GetHistID(Form("Total_%d",aHit->CopyID)),aHit->KinEnergy);

  }

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void PANRun::EndOfRun() const{

  G4AnalysisManager* fAnalysisManager = G4AnalysisManager::Instance();

  const PANPrimaryGeneratorAction* fPANPrimaryGeneratorAction = static_cast<const PANPrimaryGeneratorAction*>
                (G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction());

  // fill EndOfRun information
  fAnalysisManager->FillNtupleIColumn(0, 0, GetRunID());
  fAnalysisManager->FillNtupleIColumn(0, 1, totalEvents_);

  fAnalysisManager->AddNtupleRow(0);

  //write config
  fAnalysisManager->FillNtupleIColumn(1, 0, G4int(true));
  fAnalysisManager->FillNtupleIColumn(1, 1, G4int(fPANGlobalConfig->simout_more));
  if(fPANGlobalConfig->IsMiniPAN){MiniPAN_Jun2022->AddConfig(fAnalysisManager);}
  fAnalysisManager->AddNtupleRow(1);

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

G4int PANRun::GetTreeID(G4String stree) const {

  return fPANSDConfig->GetTreeID().at(stree);

}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

G4int PANRun::GetHistID(G4String sHist) const {

  return fPANSDConfig->GetHistID().at(sHist);

}