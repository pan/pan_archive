#include "OptionsManager.hh"

OptionsManager::OptionsManager() {
    init();
}

OptionsManager::~OptionsManager(){}


G4bool OptionsManager::parse(G4int argc_par, char** argv_par) {
    if (argc_par < 2){return false;}

    G4String cur_par_str;
    G4int idx = 0;
    while(idx < argc_par - 1){
        cur_par_str = argv_par[++idx];

	//version
	if (cur_par_str == "--version") {
            version_flag_ = true;
            return false;
        }

	//options
        if (cur_par_str[0] == '-') {
            if (cur_par_str.length() != 2){return false;}
            char cur_option = cur_par_str[1];
            switch (cur_option) {

              case 'g':
		if(!case_g(++idx,argv_par)) {return false;}
		break;

              case 'i':
		if(!case_i(++idx,argv_par)) {return false;}
                break;

              case 'o':
		if(!case_o(++idx,argv_par)) {return false;}
		break;
	
              case 'f':
                fixed_name = true;
                break;

              case 'c':
		if(!case_c(++idx,argv_par)) {return false;}
		break;

              default:
                return false;
            }
        } else {
            if (primary_file == "") {
                primary_file = cur_par_str;
                if (primary_file.size() < 5) return false;  // <1.mac> would be legal but <.mac>, <mac> and shorter not 
                if (primary_file.substr(primary_file.size() - 3) == G4String("mac")) {
                    gps_flag = true;
                } else if (primary_file.substr(primary_file.size() - 4) == G4String("root")) {
                    gps_flag = false;
		    rpd_flag = true;
                } else {
                    G4cout << "wrong primary file extention." << G4endl;
                    return false;
                }
            } else if (run_mac_file == "") {
                run_mac_file = cur_par_str;
                if (run_mac_file.size() < 5) return false; // <1.mac> would be legal but <.mac>, <mac> and shorter not
                if (run_mac_file.substr(run_mac_file.size() - 3) != G4String("mac")) {
                    G4cout << "wrong run file extention." << G4endl;
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    if (vis_mac_file == "") {
        if ((primary_file == "" || run_mac_file == "")) {return false;}
    } else {
        if (vis_mac_file.substr(vis_mac_file.size() - 3) != G4String("mac")) {
            G4cout << "wrong vis file extention." << G4endl;
            return false;
        }
    }

    return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::case_g(G4int idx, char** argv_par){
  if (interactive_flag) {return false;}
  gui_flag = true;

  vis_mac_file = argv_par[idx]; 
  if (vis_mac_file[0] == '-') {return false;}
  
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::case_i(G4int idx, char** argv_par){
  if (gui_flag) {return false;}
  interactive_flag = true;

  vis_mac_file = argv_par[idx];
  if (vis_mac_file[0] == '-') {return false;}

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::case_o(G4int idx, char** argv_par){
  output_file = argv_par[idx];
  if (output_file[0] == '-') {return false;}
					
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::case_c(G4int idx, char** argv_par){
  config_file = argv_par[idx];
  if (config_file[0] == '-') {return false;}

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void OptionsManager::print_help() {
    G4cout << "Usage:"                                                                           << G4endl;
    G4cout << "  " << SW_NAME << " -g <vis.mac>"                                                 << G4endl;
    G4cout << "  " << SW_NAME << " -i <init.mac>"                                                << G4endl;
    G4cout << "  " << SW_NAME << " <gps.mac>  <run.mac> [-o <output.root>]" << G4endl;
    G4cout << "  " << SW_NAME << " <gen.root> <run.mac> [-o <output.root>] [-f]"                 << G4endl;
    G4cout << "  " << SW_NAME << " ... [-c <config_file.mac>]"                                   << G4endl;
    G4cout << G4endl;

    G4cout << "Options:"                                                                         				<< G4endl;
    G4cout << "  -g <vis.mac>                    macro file for gui vis"                         				<< G4endl;
    G4cout << "  -i <init.mac>                   macro file for terminal vis"                    				<< G4endl;
    G4cout << "  -o <output.root>                output file name"                               				<< G4endl;
    G4cout << "  -f                              fixed output filename for only sequential mode" 				<< G4endl;
    G4cout << "  -c <config.mac>                 configure file executed before init"            				<< G4endl;
    G4cout << G4endl;

    G4cout << "  --version                        print version and author information" << G4endl;
    G4cout << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void OptionsManager::print_version() {
    G4cout << G4endl;
    G4cout << "    " << SW_NAME    << " - PAN Simulation Framework"                                               << G4endl;
    G4cout << "    " << SW_VERSION << " (" << RELEASE_DATE << ", compiled " << __DATE__ << " " << __TIME__ << ")" << G4endl;
    G4cout << G4endl;

    G4cout << " Copyright (C) 2024 PAN Simulation Group" << G4endl;
    G4cout << G4endl;

    G4cout << " Main Contributors: "                     << G4endl;
    G4cout << " - Johannes Hulsman <johannes.hulsman@unige.ch>" << G4endl;
    G4cout << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void OptionsManager::print_options() {

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void OptionsManager::init() {
    vis_mac_file      = "";
    primary_file      = "";
    run_mac_file      = "";
    config_file       = "";
    output_file       = "output.root";

    gui_flag           = false;
    interactive_flag   = false;
    gps_flag           = true;
    rpd_flag           = false;
    fixed_name         = false;

    version_flag_ = false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::GetVersion() {
    return version_flag_;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::IsGPS(){
    return gps_flag;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::IsGuiVis(){
    return gui_flag;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::IsTerminalVis(){
    return interactive_flag;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool OptionsManager::IsNameFixed(){
    return fixed_name;
}
